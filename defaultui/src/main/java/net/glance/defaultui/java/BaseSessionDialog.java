package net.glance.defaultui.java;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.core.os.BundleCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;

import net.glance.android.DialogSessionMode;
import net.glance.android.Glance;
import net.glance.android.SessionDialogListener;
import net.glance.android.SessionUI;
import net.glance.android.StartParams;
import net.glance.android.Util;
import net.glance.defaultui.R;
import net.glance.defaultui.java.blurdialogfragment.BlurDialogFragment;
import net.glance.defaultui.java.util.CommonUtils;
import net.glance.glancevideo.AutoFitTextureView;
import net.glance.glancevideo.CameraManager;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseSessionDialog extends BlurDialogFragment
        implements SessionDialogModeListener, VisitorVideoControlListener {

    protected Activity activity;

    private ActivityResultLauncher<String[]> requestPermissionLauncher;

    private SessionState state;

    protected DialogSessionMode previousMode;

    protected DialogSessionMode mode;

    @Nullable
    protected SessionDialogListener sessionDialogListener;

    protected Handler mHandler;

    // SIZES PERCENTAGES
    protected static final float DEFAULT_DIALOG_TOP = 0.15f;

    protected static final float TERMS_DIALOG_TOP = 0.05f;

    protected static final float TERMS_DIALOG_HEIGHT = 0.85f;

    protected static final float TERMS_DIALOG_WIDTH = 0.85f;

    protected static final float SESSION_CODE_DIALOG_WIDTH = 0.80f;

    protected static final float VISITOR_VIDEO_WITHSESSION_CODE_DIALOG_WIDTH = 0.90f;

    protected static final float END_SESSION_DIALOG_WIDTH = 0.80f;

    protected static final float WAITING_SESSION_MAX_WEBVIEW_HEIGHT = 0.05f;

    protected static final float TERMS_DIALOG_MAX_WEBVIEW_HEIGHT = 0.685f;

    protected static final float VISITOR_VIDEO_VIEW_SIZE_PORTRAIT = 0.4f;

    protected static final float VISITOR_VIDEO_VIEW_SIZE_LANDSCAPE = 0.3f;

    protected static final String DIALOG_MODE = "DIALOG_MODE";

    protected static final String DIALOG_DISMISS_KEY = "DIALOG_DISMISS_KEY";

    protected static final String DIALOG_MESSAGE_RES_ID_KEY = "DIALOG_MESSAGE_RES_ID_KEY";

    protected static final String DIALOG_SESSION_KEY = "DIALOG_SESSION_KEY";

    protected static final String DIALOG_TERMS_URL_KEY = "DIALOG_TERMS_URL_KEY";

    protected static final String WAITING_FOR_AGENT_ANIMATION_PATH = "file:///android_asset/waiting_session.html";

    // Views
    protected View rootView;

    protected ConstraintLayout clDialogSessionContainer;

    protected ImageButton ibCloseDialog;

    // Video views
    protected CardView clVisitorVideoContainer;

    protected FrameLayout flVisitorVideoContainer;

    protected AutoFitTextureView aftvVisitorVideo;

    protected ConstraintLayout clVisitorVideoOff;

    protected ConstraintLayout clVisitorVideoControlButtonsContainer;

    protected ImageButton ibVideoState;

    // Waiting animation views
    protected ConstraintLayout clWaitingSessionAnimContainer;

    protected WebView wvWaitingSessionAnim;

    // Reusable labels
    protected GlanceTextView gtvHeader;

    protected GlanceTextView gtvDescription;

    // Session code views
    protected ConstraintLayout clSessionCodeContainer;

    protected GlanceTextView gtvSessionCode;

    // Terms and conditions views
    protected GlanceTextView gtvTermsAndConditions;

    protected ConstraintLayout clTermsAndConditionsContainer;

    protected WebView wvTermsAndConditions;

    // Accept and Decline buttons views
    protected ConstraintLayout clAcceptDeclineBtsContainer;

    protected Button btAcceptTerms;

    protected Button btDeclineTerms;

    // Brand label view
    protected GlanceTextView gtvPoweredByGlance;

    protected CameraManager cameraManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestPermissionLauncher = registerForActivityResult(
                new ActivityResultContracts.RequestMultiplePermissions(),
                result -> {
                    Boolean isGranted = result.get(Manifest.permission.CAMERA);
                    if (isGranted != null) {
                        final DefaultSessionUI sessionUI = getDefaultSessionUI();
                        if (sessionUI != null) {
                            sessionUI.onPermissionResult(Manifest.permission.CAMERA);
                        } else {
                            Log.i("Glance", String.format("Permission result received for %s , but there's " +
                                    "no SessionUI implementation available", Manifest.permission.CAMERA));
                        }
                    }
                }
        );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = getActivity();

        state = SessionState.loadFromPreferences(activity);

        changeDialogTop(DEFAULT_DIALOG_TOP);

        rootView = inflater.inflate(R.layout.dialog_session, container, false);
        rootView.setVisibility(View.INVISIBLE);

        loadViews();
        cameraManager = new CameraManager(activity, rootView, null, R.id.aftvVisitorVideo, false);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle;
        if (savedInstanceState != null) {
            bundle = savedInstanceState;
        } else {
            bundle = getArguments();
        }
        if (bundle != null) {
            setMode(BundleCompat.getSerializable(bundle, DIALOG_MODE, DialogSessionMode.class));
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(DIALOG_MODE, mode);
        super.onSaveInstanceState(outState);
    }

    public void setSessionParams(@NonNull StartParams sessionParams) {
        Bundle arguments = getArguments();
        if (arguments != null) {
            arguments.putString(DIALOG_SESSION_KEY, sessionParams.getKeyAsString());
            arguments.putString(DIALOG_TERMS_URL_KEY, sessionParams.getTermsUrl());
        }
    }

    public void setSessionDialogListener(@Nullable SessionDialogListener listener) {
        this.sessionDialogListener = listener;
    }

    @SuppressWarnings("SameParameterValue")
    protected void requestPermission(String permission) {
        requestPermissionLauncher.launch(new String[]{permission});
    }

    protected SessionUIPermissionListener getDialogCameraPermissionResultListener() {
        return new SessionUIPermissionListener() {
            @Override
            public void onPermissionRequestGranted(String permission) {
                changeVisitorVideoViewState();
            }

            @Override
            public void onPermissionRequestDenied(String permission) {
                mHandler.post(() -> {
                    onWarningMode(getString(R.string.glance_denied_camera_permission_post_warning, Util.getApplicationName(activity)));
                    SessionUI sessionUI = getSessionUI();
                    if (sessionUI != null && !Glance.isPresenceConnected()) {
                        Glance.addVisitorVideo(sessionUI.getStartVideoMode());

                        // make sure that the agent won't be able to invite the visitor's video

                        CommonUtils.getInstance().updateVisitorVideoState(true, getDefaultSessionUI());
                    }
                });
            }
        };
    }

    @Override
    public int getTheme() {
        return R.style.GlanceDialogStyle;
    }

    @Override
    protected float getDownScaleFactor() {
        return 3.0f; // how blurred it should be
    }

    @Override
    protected int getBlurRadius() {
        return 3;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        // remove the opaque view used to darken the blurred background
        View myView = activity.findViewById(R.id.cl_session_dialog_underneath_view);
        if (myView != null) {
            ViewGroup parent = (ViewGroup) myView.getParent();
            if (parent != null) {
                parent.removeView(myView);
            }
        }
    }

    protected void setMode(DialogSessionMode newMode) {
        SessionUI sessionUI = getSessionUI();
        if (sessionUI == null) {
            return;
        }

        if (mode == null && Glance.isPresenceConnected()
                && newMode != DialogSessionMode.WAITING_FOR_AGENT
                && newMode != DialogSessionMode.END_SESSION) {
            sendPresenceTermsDisplayed();
        }

        if (this.mode == newMode) {
            rootView.setVisibility(View.VISIBLE);
            return;
        }
        this.previousMode = this.mode;
        this.mode = newMode;

        Bundle bundle = getArguments();
        String msg = bundle != null ? bundle.getString(DIALOG_MESSAGE_RES_ID_KEY) : null;

        switch (newMode) {
            case PROMPT:
                sessionUI.setTermsAccepted(false);
                onPromptMode(msg);
                break;
            case WARNING:
                onWarningMode(msg);
                break;
            case TERMS_CONDITIONS_FULL:
                sessionUI.setTermsAccepted(false);
                onTermsAndConditionsMode();
                break;
            case SESSION_CODE:
                Bundle arguments = getArguments();
                if (arguments != null) {
                    String sessionKey = getArguments().getString(DIALOG_SESSION_KEY);
                    if (!TextUtils.isEmpty(sessionKey)) {
                        onSessionKeyMode(sessionKey);
                    } else {
                        throw new RuntimeException("SessionKey is null or empty. Did you call setSessionParams(StartParams)?");
                    }
                }
                break;
            case TWO_WAY_VIDEO_AND_APP_SHARING:
                sessionUI.setTermsAccepted(false);
                onVisitorVideoWithControlsMode();
                break;
            case TWO_WAY_VIDEO_AND_APP_SHARING_CODE:
                sessionUI.setTermsAccepted(false);
                onVisitorVideoWithControlsAndCodeMode();
                break;
            case WAITING_FOR_AGENT:
                onWaitingSessionMode();
                break;
            case END_SESSION:
                onEndSessionMode();
                break;
            default:
                break;
        }

        rootView.setVisibility(View.VISIBLE);
    }

    /**
     * Changes dialog top position
     *
     * @param newTopInPerc the value in percentage (0.0 - 1.0) that represents the desired top
     *                     increment (for decrement, just pass a negative value)
     */
    protected void changeDialogTop(float newTopInPerc) {
        Dialog dialog = getDialog();
        if (dialog != null) {
            Window window = dialog.getWindow();
            if (window != null) {
                window.setGravity(Gravity.TOP);

                WindowManager.LayoutParams dialogParams = window.getAttributes();
                dialogParams.y = (int) (Util.getScreenHeight() * newTopInPerc);
                window.setAttributes(dialogParams);
            }
        }
    }

    /**
     * Changes dialog height
     *
     * @param newHeightInPerc the value in percentage (0.0 - 1.0) that represents the desired screen
     *                        size height portion to be used as the dialog height
     */
    @SuppressWarnings("SameParameterValue")
    protected void changeDialogHeight(float newHeightInPerc) {
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) rootView.getLayoutParams();
        params.height = (int) (Util.getScreenHeight() * newHeightInPerc);
        rootView.setLayoutParams(params);
    }

    /**
     * Changes dialog width
     *
     * @param newWidthInPerc the value in percentage (0.0 - 1.0) that represents the desired screen
     *                       size width portion to be used as the dialog width
     */
    protected void changeDialogWidth(float newWidthInPerc) {
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) rootView.getLayoutParams();
        params.width = (int) (Util.getScreenWidth() * newWidthInPerc);
        rootView.setLayoutParams(params);
    }

    protected void loadViews() {
        clVisitorVideoContainer = rootView.findViewById(R.id.clVisitorVideoContainer);
        flVisitorVideoContainer = rootView.findViewById(R.id.flVisitorVideoContainer);
        aftvVisitorVideo = clVisitorVideoContainer.findViewById(R.id.aftvVisitorVideo);
        clVisitorVideoOff = clVisitorVideoContainer.findViewById(R.id.clVisitorVideoOff);
        configureVisitorVideoView();

        clVisitorVideoControlButtonsContainer = rootView.findViewById(R.id.clVisitorVideoControlButtonsContainer);
        ibVideoState = clVisitorVideoControlButtonsContainer.findViewById(R.id.ibVideoState);
        configureVisitorVideoControlsState();

        clDialogSessionContainer = rootView.findViewById(R.id.clDialogSessionContainer);

        ibCloseDialog = rootView.findViewById(R.id.ibCloseDialog);
        ViewCompat.replaceAccessibilityAction(ibCloseDialog, AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLICK,
                getString(R.string.glance_accessibility_close_button_action_description), null);

        clWaitingSessionAnimContainer = rootView.findViewById(R.id.clWaitingSessionAnimContainer);
        wvWaitingSessionAnim = clWaitingSessionAnimContainer.findViewById(R.id.wvWaitingSessionAnim);
        configureWaitingSessionView();

        gtvHeader = rootView.findViewById(R.id.tvGlanceAppSharingDialogTitle);
        gtvDescription = rootView.findViewById(R.id.tvAppSharingDialogDescription);

        clSessionCodeContainer = rootView.findViewById(R.id.clSessionCodeContainer);
        gtvSessionCode = clSessionCodeContainer.findViewById(R.id.gtvSessionCode);

        gtvTermsAndConditions = rootView.findViewById(R.id.tvGlanceTermsLink);
        gtvPoweredByGlance = rootView.findViewById(R.id.tvGlancePoweredBy);
        if (!activity.getResources().getBoolean(R.bool.GLANCE_DIALOG_SHOW_GLANCE_TEXT)) {
            gtvPoweredByGlance.setVisibility(View.INVISIBLE);
        }

        clTermsAndConditionsContainer = rootView.findViewById(R.id.clTermsAndConditionsContainer);
        wvTermsAndConditions = clTermsAndConditionsContainer.findViewById(R.id.wvTermsAndConditions);
        configureTermsView();

        clAcceptDeclineBtsContainer = rootView.findViewById(R.id.cl_accept_decline_layout);
        btAcceptTerms = clAcceptDeclineBtsContainer.findViewById(R.id.btAcceptSharingApp);
        btDeclineTerms = clAcceptDeclineBtsContainer.findViewById(R.id.btDeclineSharingApp);
    }

    protected void changeVisitorVideoViewState() {
        cameraManager.initCameraWhenAvailable(state.videoEnabled);

        flVisitorVideoContainer.setVisibility(state.videoEnabled ? View.VISIBLE : View.GONE);
        clVisitorVideoOff.setVisibility(state.videoEnabled ? View.GONE : View.VISIBLE);

        if (state.videoEnabled && aftvVisitorVideo.isAvailable()) {
            cameraManager.onOpenCamera(clVisitorVideoContainer.getWidth(), clVisitorVideoContainer.getHeight());
        } else if (cameraManager.isCameraOpened()) { // on the first time, the aftvVisitorVideo might not be available
            cameraManager.onCloseCamera();
        }

        clVisitorVideoContainer.setContentDescription(clVisitorVideoContainer.getContext().getString(
                state.videoEnabled ? R.string.glance_accessibility_visitor_video_on : R.string.glance_accessibility_visitor_video_off)
        );
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void configureTermsView() {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) wvTermsAndConditions.getLayoutParams();
        params.height = (int) (Util.getScreenHeight() * TERMS_DIALOG_MAX_WEBVIEW_HEIGHT);
        wvTermsAndConditions.setLayoutParams(params);

        // maybe add some loading view while the website gets completed rendered
        wvTermsAndConditions.setWebViewClient(new MyWebViewClient());
        wvTermsAndConditions.getSettings().setJavaScriptEnabled(true);
    }

    private void configureVisitorVideoView() {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) clVisitorVideoContainer.getLayoutParams();
        int size;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            size = (int) (Util.getScreenWidth() * VISITOR_VIDEO_VIEW_SIZE_PORTRAIT);
        } else {
            size = (int) (Util.getScreenHeight() * VISITOR_VIDEO_VIEW_SIZE_LANDSCAPE);
        }
        params.height = size;
        params.width = size;
        clVisitorVideoContainer.setLayoutParams(params);
    }

    private void configureVisitorVideoControlsState() {
        ibVideoState.setActivated(state.videoEnabled);

        ibVideoState.setOnClickListener(v -> {
            state.videoEnabled = !state.videoEnabled;
            SessionState.setVideoEnabled(activity, state.videoEnabled);
            ibVideoState.setActivated(state.videoEnabled);

            if (!state.videoEnabled) {
                final DefaultSessionUI sessionUI = getDefaultSessionUI();
                if (sessionUI != null) {
                    sessionUI.setVisitorVideoAdded(false);
                }
            }

            onVideoStateChanged();

            configureIbVideoStateA11y();

            if (clVisitorVideoContainer != null) {
                clVisitorVideoContainer.announceForAccessibility(getString(state.videoEnabled ? R.string.glance_accessibility_visitor_video_on : R.string.glance_accessibility_visitor_video_off));
            }
        });
        configureIbVideoStateA11y();
    }

    private void configureIbVideoStateA11y() {
        setAccessibilityLabelAndAction(ibVideoState, state.videoEnabled ? R.string.glance_accessibility_disable_action_description : R.string.glance_accessibility_enable_action_description, state.videoEnabled ? R.string.glance_accessibility_video_on_button_label : R.string.glance_accessibility_video_off_button_label);
    }

    protected void setAccessibilityLabelAndAction(View view, int actionResId, int labelResId) {
        ViewCompat.replaceAccessibilityAction(view, AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLICK, getString(actionResId), null);
        view.setContentDescription(getString(labelResId));
    }

    @SuppressLint("SetJavaScriptEnabled")
    protected void configureWaitingSessionView() {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) wvWaitingSessionAnim.getLayoutParams();
        params.height = (int) (Util.getScreenHeight() * WAITING_SESSION_MAX_WEBVIEW_HEIGHT);
        wvWaitingSessionAnim.setLayoutParams(params);

        wvWaitingSessionAnim.setWebViewClient(new MyWebViewClient());
        wvWaitingSessionAnim.getSettings().setJavaScriptEnabled(true);
    }

    /**
     * Cast the current session UI to {@link DefaultSessionUI} if current session UI is a instance.
     *
     * @return Return a {@link DefaultSessionUI} object if current session is a implementation of it. Return null otherwise.
     */
    @Nullable
    protected DefaultSessionUI getDefaultSessionUI() {
        SessionUI sessionUI = getSessionUI();
        if (sessionUI instanceof DefaultSessionUI) {
            return (DefaultSessionUI) sessionUI;
        }
        return null;
    }

    /**
     * Return the current {@link SessionUI}
     *
     * @return Return the current {@link SessionUI} or null if there's none
     */
    @Nullable
    protected SessionUI getSessionUI() {
        return Glance.getSessionUIInstance();
    }

    /**
     * Anchor two views using ConstraintLayout chains
     *
     * @param parentLayout  the parent layout hosting the views to be anchored
     * @param view1         source view trying to link
     * @param direction1    the ConstraintSet direction of the source view (top, bottom, lef, right)
     * @param view2         target view to be linked to
     * @param direction2    the ConstraintSet direction of the target view (top, bottom, lef, right)
     * @param marginDimenId the dimen resource id containing the margin value to be added between the views in dp
     */
    @SuppressWarnings("SameParameterValue")
    protected void anchorViews(ConstraintLayout parentLayout, View view1, int direction1,
                               View view2, int direction2, int marginDimenId) {
        ConstraintSet anchorSet = new ConstraintSet();
        anchorSet.clone(parentLayout);

        anchorSet.connect(view1.getId(), direction1, view2.getId(), direction2,
                getResources().getDimensionPixelSize(marginDimenId));

        anchorSet.applyTo(parentLayout);
    }

    protected void showCloseButton() {
        int topRightCornerPadding = getResources().getDimensionPixelSize(R.dimen.glance_dialog_close_button_padding);
        clDialogSessionContainer.setPadding(clDialogSessionContainer.getPaddingLeft(), topRightCornerPadding,
                topRightCornerPadding, clDialogSessionContainer.getPaddingBottom()); // give back the space on the top-right
        //corner so the close button can have some room

        ibCloseDialog.setVisibility(View.VISIBLE);
    }

    protected void setupTermsNConditionsView() {
        String statement = getString(R.string.glance_terms_and_condition_statement);
        String label = getString(R.string.glance_terms_and_condition_link_label);
        SpannableString ss = new SpannableString(statement + " " + label);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View textView) {
                setMode(DialogSessionMode.TERMS_CONDITIONS_FULL);
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(ContextCompat.getColor(activity, R.color.glance_session_ui_dialog_terms_text_color));
            }
        };

        int totalSize = ss.length();
        int labelSize = label.length();
        ss.setSpan(clickableSpan, totalSize - labelSize, totalSize, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        gtvTermsAndConditions.setText(ss);
        gtvTermsAndConditions.setMovementMethod(LinkMovementMethod.getInstance());
        gtvTermsAndConditions.setHighlightColor(Color.TRANSPARENT);
    }

    protected void sendPresenceTermsDisplayed() {
        Glance.sendPresenceTermsDisplayed();

        DefaultSessionUI defaultSessionUI = getDefaultSessionUI();
        if (defaultSessionUI != null) {
            Map<String, String> params = new HashMap<String, String>() {{
                put("status", "displayed");
            }};
            CommonUtils.getInstance().onSDKEventTriggered(defaultSessionUI, "sendPresenceTermsDisplayed", params);
        }
    }

    private static class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(request.getUrl().toString());
            return true;
        }
    }
}
