package net.glance.defaultui.java;

import static net.glance.android.WidgetVisibilityMode.FULL_MODE;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;

import net.glance.android.Glance;
import net.glance.android.SessionUI;
import net.glance.android.StartParams;
import net.glance.android.WidgetCorner;
import net.glance.android.WidgetVisibilityMode;
import net.glance.defaultui.java.util.GlanceSDKEventSenderListener;
import net.glance.defaultui.java.util.CommonUtils;
import net.glance.defaultui.kotlin.SessionUIWidgetModeListener;
import net.glance.glancevideo.CameraManager;
import net.glance.glancevideo.videosession.VideoSession;

import java.util.HashMap;
import java.util.Map;

/***
 * <p>
 *  Class that holds all common variables, constants and methods definitions to be used through all
 *  the new Magnesium UI implementation.
 *
 */
public abstract class BaseSessionUI extends SessionUI implements SessionUIWidgetCommonControlListener,
        VisitorVideoControlListener, SessionUIWidgetModeListener, VideoSession.VideoSessionListener {

    // Miscellaneous
    protected static final String TAG = "SessionUI";

    protected Context mContext;

    protected Handler mHandler;

    @Nullable
    protected GlanceSDKEventSenderListener glanceSDKEventSenderListener;

    protected SessionUIPermissionListener permissionListener;

    SessionState state;

    protected CameraManager cameraManager;

    // we needed to create an external flag to control whether the camera manager should open the
    // camera right away in order to fix the issue #GD-19506 and its side effetcs. We'll consider
    // this flag default value as true.
    protected Boolean shouldOpenCameraWhenAvailable;

    protected View widgetRootView;

    protected boolean setDragListener = true;

    protected WidgetCorner currentCorner = WidgetCorner.TOP_LEFT; //default

    protected WidgetMode currentMode;

    protected WidgetMode modeBeforeMinimizing; // to tab

    protected WidgetMode modeBeforeExpanding; // to the large widget

    protected boolean fullView = false;

    protected boolean showWidgetOnLoadingLayout = true;

    // Tab widget views
    protected CardView cvSessionUiTabWidget;

    protected ImageView ivSessionUiTabWidgetIcon;

    // Visitor video
    protected VideoSession videoSession;

    @Nullable
    protected StartParams videoStartParams;

    // Miscellaneous
    protected abstract void setUpSessionViewMoveGestureHandler();

    protected abstract float[] generateCornerCoordinates(View view);

    protected abstract void calculateNearestCorner();

    protected abstract void moveWidgetToNearestCorner();

    protected abstract void moveWidgetToCorner(WidgetCorner corner);

    protected abstract int getVisitorVideoViewIdByMode(WidgetMode targetMode);

    public void setGlanceSDKEventSenderListener(@Nullable GlanceSDKEventSenderListener sdkEventSenderListener) {
        SessionUI sessionUIInstance = Glance.getSessionUIInstance();
        if (sessionUIInstance instanceof BaseSessionUI) {
            glanceSDKEventSenderListener = sdkEventSenderListener;
        }
    }

    @Nullable
    public GlanceSDKEventSenderListener getGlanceSDKEventSenderListener() {
        return glanceSDKEventSenderListener;
    }

    protected String getString(int stringResId) {
        return mContext != null ? mContext.getResources().getString(stringResId) : "";
    }

    protected int getColor(int colorResId) {
        return mContext != null ? ContextCompat.getColor(mContext, colorResId) : 0;
    }

    protected float getDimen(int dimenResId) {
        return mContext != null ? mContext.getResources().getDimension(dimenResId) : 0f;
    }

    /***
     * Interrupts any other accessibility readout in order to run some we want (otherwise the user
     * would need to hear all of them, which are useless most of times for the given action).
     */
    protected void interruptA11yReadout() {
        AccessibilityManager accessibilityManager = (AccessibilityManager) mContext.getSystemService(Context.ACCESSIBILITY_SERVICE);
        if (accessibilityManager.isEnabled()) {
            accessibilityManager.interrupt();
        }
    }

    protected void setIsFullView(boolean isFullView) {
        Log.d(TAG, "new fullView state: " + isFullView);
        fullView = isFullView;
    }

    protected void setOnPermissionResultListener(SessionUIPermissionListener listener) {
        permissionListener = listener;
    }

    protected void closeCamera() {
        if (cameraManager != null && cameraManager.isCameraOpened()) {
            cameraManager.onCloseCamera();
        }
    }

    protected void runOnMainThread(Runnable task) {
        if (mHandler != null) { // avoids NPE caused by race condition issues when ending a session
            mHandler.post(task);
        }
    }

    protected void runOnMainThreadDelayed(Runnable task, long delay) {
        if (mHandler != null) { // avoids NPE caused by race condition issues when ending a session
            mHandler.postDelayed(task, delay);
        }
    }

    protected boolean hasAgentVideo(WidgetMode targetMode) {
        return targetMode != WidgetMode.TAB_MODE
                && targetMode != WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE;
    }

    protected boolean isTwoWayVideo(WidgetMode targetMode) {
        return targetMode == WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE;
    }


    protected boolean isLargeMode(WidgetMode targetMode) {
        return targetMode == WidgetMode.LARGE_VIDEO;
    }

    protected boolean onLeftCorner() {
        return currentCorner == WidgetCorner.TOP_LEFT || currentCorner == WidgetCorner.BOTTOM_LEFT;
    }

    protected boolean onRightCorner() {
        return currentCorner == WidgetCorner.TOP_RIGHT || currentCorner == WidgetCorner.BOTTOM_RIGHT;
    }

    /***
     * Changes the color of the icon and disables the click in order to prevent the user to use the
     * feature until the pre conditions are met again.
     */
    protected void setButtonBlockedState(boolean blocked, ImageButton button) {
        button.setAlpha(blocked ? 0.3f : 1f);
        button.setEnabled(!blocked);
    }

    /***
     * Sends the new widget corner location so the agent side view window can update it accordingly.
     */
    protected void sendNewLocation() {
        Glance.updateWidgetLocation(currentCorner);

        Map<String, String> params = new HashMap<String, String>() {{
            put("widgetlocation", String.format("message:widgetlocation;location:%s", WidgetCorner.toServerString(currentCorner)));
        }};
        CommonUtils.getInstance().onSDKEventTriggered(this, "sendNewLocation", params);
    }

    /***
     * Sends the new widget visibility so the agent side view window can update it accordingly.
     */
    protected void updateWidgetVisibility(WidgetVisibilityMode visibilityMode) {
        Glance.updateWidgetVisibility(visibilityMode);

        Map<String, String> params = new HashMap<String, String>() {{
            put("widgetvisibility", String.format("message:widgetvisibility;visibility:%s", visibilityMode));
        }};
        CommonUtils.getInstance().onSDKEventTriggered(this, "updateWidgetVisibility", params);
    }

    protected void setAccessibilityLabelAndAction(View view, int actionResId, int labelResId) {
        ViewCompat.replaceAccessibilityAction(view, AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLICK, getString(actionResId), null);
        view.setContentDescription(getString(labelResId));
    }
}