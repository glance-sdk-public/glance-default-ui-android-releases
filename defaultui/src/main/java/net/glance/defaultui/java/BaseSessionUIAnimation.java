package net.glance.defaultui.java;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.PorterDuff;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;

import androidx.annotation.NonNull;

import net.glance.android.WidgetCorner;
import net.glance.defaultui.R;
import net.glance.defaultui.java.util.CommonUtils;

import java.util.HashMap;
import java.util.Map;

/***
 * <p>
 *  Class that holds all common animation implementations for the new Magnesium UI widget, including
 *  transitions between modes, pulsating tab color, etc.
 *
 */
public abstract class BaseSessionUIAnimation extends BaseSessionUI {

    protected ValueAnimator tabBackgroundColorAnimator;

    protected ValueAnimator tabIconColorAnimator;

    // can't use the value from the animator object because it isn't consistent
    protected boolean tabColorAnimStarted = false;

    private static final int TAB_PULSATING_COLOR_ANIMATION_DURATION = 2000;

    private static final int TAB_PULSATING_COLOR_ANIMATION_START_DELAY = 2000;

    private static final Map<WidgetCorner, Integer> ANIMATION_PIVOT_Y_TYPES = new HashMap<WidgetCorner, Integer>() {{
        put(WidgetCorner.TOP_LEFT, Animation.RELATIVE_TO_SELF);
        put(WidgetCorner.TOP_RIGHT, Animation.RELATIVE_TO_SELF);
        put(WidgetCorner.BOTTOM_LEFT, Animation.RELATIVE_TO_PARENT);
        put(WidgetCorner.BOTTOM_RIGHT, Animation.RELATIVE_TO_PARENT);
    }};

    private static final Map<WidgetCorner, Float> ANIMATION_PIVOT_Y_VALUES = new HashMap<WidgetCorner, Float>() {{
        put(WidgetCorner.TOP_LEFT, 0f);
        put(WidgetCorner.TOP_RIGHT, 1f);
        put(WidgetCorner.BOTTOM_LEFT, 1f);
        put(WidgetCorner.BOTTOM_RIGHT, 1f);
    }};

    private static final Map<WidgetMode, Float> ANIMATION_INIT_Y_SCALE_VALUES = new HashMap<WidgetMode, Float>() {{
        put(WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE, 1.25f);
        put(WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE, 2f);
        put(WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE, 3f);
    }};

    private static final Map<WidgetMode, Float> ANIMATION_INIT_X_SCALE_VALUES = new HashMap<WidgetMode, Float>() {{
        put(WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE, 2f);
        put(WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE, 3f);
        put(WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE, 3f);
    }};

    private static final Map<WidgetCorner, Integer> ANIMATION_PIVOT_X_TYPES = new HashMap<WidgetCorner, Integer>() {{
        put(WidgetCorner.TOP_LEFT, Animation.RELATIVE_TO_SELF);
        put(WidgetCorner.TOP_RIGHT, Animation.RELATIVE_TO_PARENT);
        put(WidgetCorner.BOTTOM_LEFT, Animation.RELATIVE_TO_PARENT);
        put(WidgetCorner.BOTTOM_RIGHT, Animation.RELATIVE_TO_PARENT);
    }};

    private static final Map<WidgetCorner, Float> ANIMATION_PIVOT_X_VALUES = new HashMap<WidgetCorner, Float>() {{
        put(WidgetCorner.TOP_LEFT, 0f);
        put(WidgetCorner.TOP_RIGHT, 1f);
        put(WidgetCorner.BOTTOM_LEFT, 0f);
        put(WidgetCorner.BOTTOM_RIGHT, 1f);
    }};

    protected void startTabColorAnimations() {
        if (widgetRootView.getId() == R.id.session_ui_tab_widget
                && (!tabColorAnimStarted || (tabBackgroundColorAnimator != null
                && tabIconColorAnimator != null))) {
            tabColorAnimStarted = true;

            runOnMainThreadDelayed(() -> {
                configTabBgColorAnimation(false);
                configTabIconColorAnimation(false);
            }, TAB_PULSATING_COLOR_ANIMATION_START_DELAY);
        }
    }

    protected void stopTabColorAnimations() {
        runOnMainThread(() -> {
            if (currentMode == WidgetMode.TAB_MODE
                    && tabBackgroundColorAnimator != null
                    && tabIconColorAnimator != null) {
                tabColorAnimStarted = false;

                tabBackgroundColorAnimator.removeAllListeners();
                tabBackgroundColorAnimator.cancel();
                tabBackgroundColorAnimator.setDuration(0);
                cvSessionUiTabWidget.animate().setListener(null);
                cvSessionUiTabWidget.animate().setUpdateListener(null);
                cvSessionUiTabWidget.setCardBackgroundColor(getColor(R.color.glance_session_ui_tab_widget_bg_color));

                tabIconColorAnimator.removeAllListeners();
                tabIconColorAnimator.cancel();
                tabIconColorAnimator.setDuration(0);
                ivSessionUiTabWidgetIcon.animate().setListener(null);
                ivSessionUiTabWidgetIcon.animate().setUpdateListener(null);
                ivSessionUiTabWidgetIcon.setColorFilter(getColor(R.color.glance_session_ui_tab_widget_icon_color));
            }
        });
    }

    protected void configTabBgColorAnimation(boolean inversedColors) {
        tabBackgroundColorAnimator = new ValueAnimator();

        if (inversedColors) {
            tabBackgroundColorAnimator.setIntValues(getColor(R.color.glance_session_ui_tab_widget_target_pulse_bg_color),
                    getColor(R.color.glance_session_ui_tab_widget_bg_color));
        } else {
            tabBackgroundColorAnimator.setIntValues(getColor(R.color.glance_session_ui_tab_widget_bg_color),
                    getColor(R.color.glance_session_ui_tab_widget_target_pulse_bg_color));
        }

        tabBackgroundColorAnimator.setEvaluator(new ArgbEvaluator());
        tabBackgroundColorAnimator.addUpdateListener(valueAnimator -> cvSessionUiTabWidget.setCardBackgroundColor((Integer) tabBackgroundColorAnimator.getAnimatedValue()));
        tabBackgroundColorAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(@NonNull Animator animation) {
                // no op
            }

            @Override
            public void onAnimationEnd(@NonNull Animator animation) {
                runOnMainThread(() -> configTabBgColorAnimation(!inversedColors));
            }

            @Override
            public void onAnimationCancel(@NonNull Animator animation) {
                // no op
            }

            @Override
            public void onAnimationRepeat(@NonNull Animator animation) {
                // no op
            }
        });

        tabBackgroundColorAnimator.setDuration(TAB_PULSATING_COLOR_ANIMATION_DURATION);
        tabBackgroundColorAnimator.start();
    }

    private void configTabIconColorAnimation(boolean inversedColors) {
        tabIconColorAnimator = new ValueAnimator();

        if (inversedColors) {
            tabIconColorAnimator.setIntValues(getColor(android.R.color.white),
                    getColor(R.color.glance_session_ui_tab_widget_icon_color));
        } else {
            tabIconColorAnimator.setIntValues(getColor(R.color.glance_session_ui_tab_widget_icon_color),
                    getColor(android.R.color.white));
        }

        tabIconColorAnimator.setEvaluator(new ArgbEvaluator());
        tabIconColorAnimator.addUpdateListener(valueAnimator -> ivSessionUiTabWidgetIcon.setColorFilter((Integer) tabIconColorAnimator.getAnimatedValue(), PorterDuff.Mode.SRC_IN));
        tabIconColorAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(@NonNull Animator animation) {
                // no op
            }

            @Override
            public void onAnimationEnd(@NonNull Animator animation) {
                runOnMainThread(() -> configTabIconColorAnimation(!inversedColors));
            }

            @Override
            public void onAnimationCancel(@NonNull Animator animation) {
                // no op
            }

            @Override
            public void onAnimationRepeat(@NonNull Animator animation) {
                // no op
            }
        });

        tabIconColorAnimator.setDuration(TAB_PULSATING_COLOR_ANIMATION_DURATION);
        tabIconColorAnimator.start();
    }

    protected void animateWidgetToTabTransition() {
        runOnMainThread(() -> {
            widgetRootView.setOnTouchListener(null);
            setDragListener = false;

            showWidgetOnLoadingLayout = false;

            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0f);
            alphaAnimation.setDuration(100);
            alphaAnimation.setRepeatCount(0);
            alphaAnimation.setRepeatMode(Animation.REVERSE);
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    runOnMainThread(() -> {
                        widgetRootView.setVisibility(View.INVISIBLE);

                        onTabMode();

                        if (isTwoWayVideo(modeBeforeMinimizing)) {
                            videoSession.stopEncoding();
                            cameraManager.onCloseCamera();
                            CommonUtils.getInstance().updateVisitorVideoState(true, BaseSessionUIAnimation.this);

                            shouldOpenCameraWhenAvailable = state.videoEnabled;
                        }

                        moveWidgetToCorner(currentCorner);
                        widgetRootView.setVisibility(View.VISIBLE);
                        animateTabSize();
                    });
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            widgetRootView.startAnimation(alphaAnimation);
        });
    }

    private void animateTabSize() {
        runOnMainThread(() -> {
            long animDuration = 300;

            Animation tabDecreaseSizeAnim = new ScaleAnimation(
                    getCurrentModeAnimValue(ANIMATION_INIT_X_SCALE_VALUES), 1f, // Start and end values for the X axis scaling
                    getCurrentModeAnimValue(ANIMATION_INIT_Y_SCALE_VALUES), 1f, // Start and end values for the Y axis scaling
                    getCurrentCornerAnimValue(ANIMATION_PIVOT_X_TYPES), getCurrentCornerAnimPivotValue(ANIMATION_PIVOT_X_VALUES), // Pivot point of X scaling
                    getCurrentCornerAnimValue(ANIMATION_PIVOT_Y_TYPES), getCurrentCornerAnimPivotValue(ANIMATION_PIVOT_Y_VALUES)); // Pivot point of Y scaling
            tabDecreaseSizeAnim.setFillAfter(true); // Needed to keep the result of the animation
            tabDecreaseSizeAnim.setDuration(animDuration);

            AlphaAnimation tabIconAlphaIncreaseAnim = new AlphaAnimation(0.5f, 1f);
            tabIconAlphaIncreaseAnim.setDuration(animDuration);

            AnimationSet s = new AnimationSet(false);//  false means don't share interpolators
            s.addAnimation(tabDecreaseSizeAnim);
            s.addAnimation(tabIconAlphaIncreaseAnim);
            s.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    setUpSessionViewMoveGestureHandler();
                    startTabColorAnimations();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            widgetRootView.startAnimation(s);

            showWidgetOnLoadingLayout = true;
        });
    }

    protected void animateTabToWidgetTransition(float lastMotionXCoordinate) {
        runOnMainThread(() -> {
            widgetRootView.setOnTouchListener(null);
            setDragListener = false;

            showWidgetOnLoadingLayout = false;

            stopTabColorAnimations();

            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0f);
            alphaAnimation.setDuration(100);
            alphaAnimation.setRepeatCount(0);
            alphaAnimation.setRepeatMode(Animation.REVERSE);
            alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    widgetRootView.setVisibility(View.INVISIBLE);
                    onClickToExpandWidget();

                    widgetRootView.post(() -> {
                        moveWidgetToCorner(currentCorner);
                        float initialX = lastMotionXCoordinate + widgetRootView.getWidth();
                        if (currentCorner == WidgetCorner.TOP_LEFT || currentCorner == WidgetCorner.BOTTOM_LEFT) {
                            initialX = lastMotionXCoordinate;
                        }

                        if (isTwoWayVideo(currentMode)) {
                            videoSession.resume();
                        }

                        if (state.videoEnabled) {
                            CommonUtils.getInstance().updateVisitorVideoState(false, BaseSessionUIAnimation.this);
                        }

                        // make sure that the expanded widget will have the same last UI properties
                        // as the tab, so we can achieve a smooth animation between them
                        widgetRootView.setAlpha(0f);
                        widgetRootView.setScaleX(0.5f);
                        widgetRootView.setScaleY(0.5f);
                        widgetRootView.setX(initialX);
                        widgetRootView.setVisibility(View.VISIBLE);

                        animateWidgetTranslation();
                    });
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            widgetRootView.startAnimation(alphaAnimation);
        });
    }

    private void animateWidgetTranslation() {
        float[] coordinates = generateCornerCoordinates(widgetRootView);
        float leftX = coordinates[0];
        float rightX = coordinates[1];
        float topY = coordinates[2];
        float bottomY = coordinates[3];

        float targetX = rightX;
        if (currentCorner == WidgetCorner.TOP_LEFT || currentCorner == WidgetCorner.BOTTOM_LEFT) {
            targetX = leftX;
        }

        float targetY = topY;
        if (currentCorner == WidgetCorner.BOTTOM_LEFT || currentCorner == WidgetCorner.BOTTOM_RIGHT) {
            targetY = bottomY;
        }

        widgetRootView.animate()
                .setDuration(300)
                .alphaBy(1f)
                .scaleX(1f)
                .scaleY(1f)
                .x(targetX)
                .y(targetY)
                .withEndAction(this::setUpSessionViewMoveGestureHandler);

        showWidgetOnLoadingLayout = true;
    }

    private Float getCurrentModeAnimValue(Map<WidgetMode, Float> animValuesMap) {
        return animValuesMap != null && modeBeforeMinimizing != null && animValuesMap.containsKey(modeBeforeMinimizing) ? animValuesMap.get(modeBeforeMinimizing) : Float.valueOf(0f);
    }

    private Integer getCurrentCornerAnimValue(Map<WidgetCorner, Integer> animValuesMap) {
        return animValuesMap != null && currentCorner != null && animValuesMap.containsKey(currentCorner) ? animValuesMap.get(currentCorner) : Integer.valueOf(0);
    }

    private Float getCurrentCornerAnimPivotValue(Map<WidgetCorner, Float> animValuesMap) {
        return animValuesMap != null && currentCorner != null && animValuesMap.containsKey(currentCorner) ? animValuesMap.get(currentCorner) : Float.valueOf(0);
    }
}