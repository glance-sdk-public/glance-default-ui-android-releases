package net.glance.defaultui.java;

import static android.view.RoundedCorner.POSITION_BOTTOM_LEFT;
import static android.view.RoundedCorner.POSITION_BOTTOM_RIGHT;
import static android.view.RoundedCorner.POSITION_TOP_LEFT;
import static android.view.RoundedCorner.POSITION_TOP_RIGHT;
import static net.glance.android.VisitorVideoSizeMode.VIDEO_LARGE_MODE;
import static net.glance.android.VisitorVideoSizeMode.VIDEO_SMALL_MODE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.RoundedCorner;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowInsets;
import android.view.accessibility.AccessibilityEvent;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;

import net.glance.android.DialogSessionMode;
import net.glance.android.Glance;
import net.glance.android.GlanceAndroidPreferences;
import net.glance.android.SessionDialogListener;
import net.glance.android.SessionUICompletionListener;
import net.glance.android.Util;
import net.glance.android.VisitorVideoSizeMode;
import net.glance.android.WidgetCorner;
import net.glance.defaultui.R;
import net.glance.defaultui.java.util.CommonUtils;
import net.glance.defaultui.java.util.SessionDialogListenerAdapter;
import net.glance.glancevideo.AutoFitTextureView;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/***
 *  Class that holds all common views and behaviors for the new Magnesium UI widget scenarios,
 *  including buttons, videos and modes.
 * <p>
 *  Note that these commom views holds the **same IDs** (except by surface/texture views) regardless
 *  the XML layout file where they are being used so we can improve reusing.
 */
public abstract class BaseSessionUIView extends BaseSessionUIAnimation {

    protected WeakReference<Activity> mForegroundActivity;

    protected SessionDialog sessionDialog;

    private static final float SESSION_VIDEO_WIDGET_BOTTOM_LEFT_CORNER_SPACE_RATE = 0.95f;

    private static final float SESSION_VIDEO_WIDGET_TOP_RIGHT_CORNER_SPACE_RATE = 0.05f;

    private static final int HALF = 2;

    private static final double LARGE_AGENT_VIDEO_SCREEN_SIZE = 0.7f;

    public static final float LARGE_WIDGET_VISITOR_VIDEO_SCREEN_SIZE = 0.25f;

    private static final int MOVE_GESTURE_EDGE_GAP_LIMIT = 15;

    private static final int MIN_LONG_PRESS_DELAY = 100;

    private static long LAST_TOUCH_IN_MILLIS = 0;

    protected boolean mIsActivityChangingConfigurations;

    protected boolean viewsLoaded = false;

    // Border view
    protected FrameLayout mSessionUIBorder;

    // App Share widget views
    protected RelativeLayout rlSessionUiAppShareAnchorBox;

    // Main session UI widget buttons views
    protected ConstraintLayout clSessionUiVideoBtContainer;

    protected ImageButton ibEndSession;

    // Agent Video views
    protected CardView cvAgentVideoContainer;

    protected CardView clAgentVideoOffContainer;

    protected CardView cvVisitorBackVideoContainer;

    protected ImageButton ibExpandContractVideo;

    // Visitor Video views
    protected CardView clVisitorVideoContainer;

    protected FrameLayout flVisitorVideoContainer;

    protected AutoFitTextureView aftvVisitorVideo;

    protected ConstraintLayout clVisitorVideoOff;

    // Large Video views
    private boolean wasVisitorVideoOffBeforeFlip = false;

    protected ConstraintLayout clLargeVideoWidgetBackground;

    protected RelativeLayout rlAgentLargeVideoContainer;

    protected RelativeLayout rlVisitorVideoContainer;

    protected AutoFitTextureView aftvVisitorBackCamera;

    protected RelativeLayout rlLargeVideoBtContainer;

    protected ImageButton ibVisitorCameraMode;

    // App Video state
    protected ImageButton ibVideoState;

    /***
     * Mandatory methods
     */
    protected abstract void showWidgetFromStartVideoMode();

    @SuppressWarnings("SameParameterValue")
    protected abstract void showSessionDialog(SessionDialogListener listener, DialogSessionMode mode,
                                              String warningMessage, boolean dismiss);

    protected SessionUICompletionListener completionListener;

    private View.OnTouchListener widgetDragListener;

    protected void initWidgetDragListener() {
        widgetDragListener = new View.OnTouchListener() {
            float dX, dY, startX, startY;
            boolean isMove = false;

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                float screenWidth = Util.getScreenWidth();

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        LAST_TOUCH_IN_MILLIS = System.currentTimeMillis();

                        dX = view.getX() - motionEvent.getRawX();
                        dY = view.getY() - motionEvent.getRawY();

                        startX = motionEvent.getX();
                        startY = motionEvent.getY();

                        isMove = false;

                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (currentMode != WidgetMode.TAB_MODE) {
                            view.setX(motionEvent.getRawX() + dX);
                        }

                        view.setY(motionEvent.getRawY() + dY);

                        float leftViewSide = view.getX();
                        float rightViewSide = leftViewSide + view.getWidth();

                        if (currentMode == WidgetMode.TAB_MODE
                                && startX >= leftViewSide && startX <= rightViewSide
                                && Math.abs(dX) >= view.getWidth()) {
                            animateTabToWidgetTransition(leftViewSide);
                        }

                        if (currentMode == WidgetMode.TAB_MODE
                                && ((onLeftCorner() && leftViewSide >= 0)
                                || (onRightCorner() && rightViewSide < screenWidth - MOVE_GESTURE_EDGE_GAP_LIMIT))) {
                            isMove = false;
                        }

                        if (currentMode != WidgetMode.TAB_MODE
                                && (leftViewSide <= MOVE_GESTURE_EDGE_GAP_LIMIT
                                || rightViewSide >= screenWidth - MOVE_GESTURE_EDGE_GAP_LIMIT)) {

                            LAST_TOUCH_IN_MILLIS = 0;

                            calculateNearestCorner();

                            animateWidgetToTabTransition();
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        long currentTime = System.currentTimeMillis();
                        if (currentTime - LAST_TOUCH_IN_MILLIS >= MIN_LONG_PRESS_DELAY) {
                            LAST_TOUCH_IN_MILLIS = 0;

                            if (currentMode != WidgetMode.TAB_MODE || isMove) {
                                moveWidgetToNearestCorner();
                            }
                        }

                        if (currentMode == WidgetMode.TAB_MODE && !isMove) {
                            animateTabToWidgetTransition(view.getX());
                        }

                        break;
                    default:
                        return false;
                }
                return true;
            }
        };
    }

    protected void loadBorderView(ViewGroup rootView) {
        mSessionUIBorder = rootView.findViewById(R.id.session_ui_frame_border);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            final WindowInsets insets = rootView.getRootWindowInsets();

            if (insets != null) {
                final RoundedCorner topLeftCorner = insets.getRoundedCorner(POSITION_TOP_LEFT);
                final RoundedCorner topRightCorner = insets.getRoundedCorner(POSITION_TOP_RIGHT);
                final RoundedCorner bottomLeftCorner = insets.getRoundedCorner(POSITION_BOTTOM_LEFT);
                final RoundedCorner bottomRightCorner = insets.getRoundedCorner(POSITION_BOTTOM_RIGHT);

                final int topLeft = topLeftCorner != null ? topLeftCorner.getRadius() : 0;
                final int topRight = topRightCorner != null ? topRightCorner.getRadius() : 0;
                final int bottomLeft = bottomLeftCorner != null ? bottomLeftCorner.getRadius() : 0;
                final int bottomRight = bottomRightCorner != null ? bottomRightCorner.getRadius() : 0;

                GradientDrawable shape = new GradientDrawable();
                shape.setShape(GradientDrawable.RECTANGLE);
                shape.setStroke((int) getDimen(R.dimen.glance_border_width),
                        getColor(net.glance.android.R.color.glance_border_color));
                // The corners are ordered top-left, top-right, bottom-right, bottom-left.
                shape.setCornerRadii(new float[]{topLeft, topLeft, topRight, topRight,
                        bottomLeft, bottomLeft, bottomRight, bottomRight});

                mSessionUIBorder.setBackground(shape);
            }
        }
    }

    protected void loadTabWidget(View rootView) {
        this.widgetRootView = rootView;

        cvSessionUiTabWidget = rootView.findViewById(R.id.cvSessionUiTabWidget);
        ivSessionUiTabWidgetIcon = rootView.findViewById(R.id.ivSessionUiTabWidgetIcon);

        startTabColorAnimations();
    }

    /***
     * It loads/reloads the entire app share widget view, instantiating all subviews and setting
     * gestures and listeners.
     *
     * @param rootView The widget root view.
     */
    protected void loadAppShareView(View rootView) {
        this.widgetRootView = rootView;

        rlSessionUiAppShareAnchorBox = rootView.findViewById(R.id.rlSessionUiAppShareAnchorBox);

        ibEndSession = rootView.findViewById(R.id.ibEndSession);

        configureCommonButtonsListeners();

        if (setDragListener) { // only for the first load right after the agent joins
            setUpSessionViewMoveGestureHandler();
        }
    }

    /***
     * It loads/reloads the entire large video widget view, instantiating all subviews and setting
     * the listeners.
     *
     * @param rootView The widget root view.
     */
    protected void loadLargeVideoWidget(View rootView) {
        this.widgetRootView = rootView;

        // Agent views
        loadAgentVideoViews(WidgetMode.LARGE_VIDEO);

        // set correct height for agent video
        RelativeLayout.LayoutParams agentVideoOffParams = (RelativeLayout.LayoutParams) clAgentVideoOffContainer.getLayoutParams();
        agentVideoOffParams.height = (int) (Util.getScreenHeight() * LARGE_AGENT_VIDEO_SCREEN_SIZE);
        clAgentVideoOffContainer.setLayoutParams(agentVideoOffParams);

        RelativeLayout.LayoutParams agentVideoparams = (RelativeLayout.LayoutParams) cvAgentVideoContainer.getLayoutParams();
        agentVideoparams.height = (int) (Util.getScreenHeight() * LARGE_AGENT_VIDEO_SCREEN_SIZE);
        cvAgentVideoContainer.setLayoutParams(agentVideoparams);

        // Visitor views
        rlVisitorVideoContainer = rootView.findViewById(R.id.rlVisitorVideoContainer);
        aftvVisitorBackCamera = rootView.findViewById(R.id.aftvVisitorBackCamera);
        rlLargeVideoBtContainer = rootView.findViewById(R.id.rlLargeVideoBtContainer);

        // set correct height for visitor video
        loadVisitorVideoViews();

        int visitorVideoSize = (int) (Util.getScreenWidth() * LARGE_WIDGET_VISITOR_VIDEO_SCREEN_SIZE);
        RelativeLayout.LayoutParams visitorVideoContainerParams = (RelativeLayout.LayoutParams) rlVisitorVideoContainer.getLayoutParams();
        visitorVideoContainerParams.height = visitorVideoSize;
        visitorVideoContainerParams.width = visitorVideoSize;
        rlVisitorVideoContainer.setLayoutParams(visitorVideoContainerParams);
        // need to apply the height value for both container and cardview holding the video view itself
        RelativeLayout.LayoutParams visitorVideoParams = (RelativeLayout.LayoutParams) clVisitorVideoContainer.getLayoutParams();
        visitorVideoParams.height = visitorVideoSize;
        visitorVideoParams.width = visitorVideoSize;
        clVisitorVideoContainer.setLayoutParams(visitorVideoParams);
        // end

        RelativeLayout.LayoutParams visitorBackVideoPrams = (RelativeLayout.LayoutParams) cvVisitorBackVideoContainer.getLayoutParams();
        visitorBackVideoPrams.height = (int) (Util.getScreenHeight() * LARGE_AGENT_VIDEO_SCREEN_SIZE);
        cvVisitorBackVideoContainer.setLayoutParams(visitorBackVideoPrams);

        cameraManager.initCameraWhenAvailable(true);
        changeVisitorVideoViewState(WidgetMode.LARGE_VIDEO);

        ibExpandContractVideo = rootView.findViewById(R.id.ibExpandContractVideo);
        ibExpandContractVideo.setImageResource(R.drawable.glance_ic_contract_session_video);
        loadIbExpandContractVideoStateView();

        ibVideoState = rlLargeVideoBtContainer.findViewById(R.id.ibVideoState);
        ibVideoState.post(() -> { // center the visitor video view with respect to the @ibVideoState
            int[] ibVideoStateCurrentPosXY = new int[2];
            ibVideoState.getLocationOnScreen(ibVideoStateCurrentPosXY); // 0 - left; 1 - top;
            int ibVideoStateCurrentX = ibVideoStateCurrentPosXY[0];

            int[] rlVisitorVideoContainerCurrentPosXY = new int[2];
            rlVisitorVideoContainer.getLocationOnScreen(rlVisitorVideoContainerCurrentPosXY); // 0 - left; 1 - top;
            int rlVisitorVideoContainerCurrentX = rlVisitorVideoContainerCurrentPosXY[0];

            int videoBtMiddle = ibVideoStateCurrentX + (ibVideoState.getWidth() / HALF);
            int visitorVideoNewX = videoBtMiddle - (rlVisitorVideoContainer.getWidth() / HALF);

            RelativeLayout.LayoutParams visitorVideoContainerParameters = (RelativeLayout.LayoutParams) rlVisitorVideoContainer.getLayoutParams();
            visitorVideoContainerParameters.bottomMargin = -1 * (rlLargeVideoBtContainer.getHeight() / 2); // if we have any differences to be fixed in other devices, consider using the space amount between the video state button and its parent top
            visitorVideoContainerParameters.leftMargin = visitorVideoNewX - rlVisitorVideoContainerCurrentX;
            rlVisitorVideoContainer.setLayoutParams(visitorVideoContainerParameters);

            // decrease the visitor video off icon size to be proportional with respect to its container view
            ImageButton ibVisitorVideoVideoOffIcon = rlVisitorVideoContainer.findViewById(R.id.ibVisitorVideoVideoOffIcon);
            ConstraintLayout.LayoutParams videoIconParams = (ConstraintLayout.LayoutParams) ibVisitorVideoVideoOffIcon.getLayoutParams();
            int iconSize = (int) getDimen(R.dimen.glance_session_ui_large_widget_icon_visitor_video_off_size);
            videoIconParams.height = iconSize;
            videoIconParams.width = iconSize;
            videoIconParams.bottomMargin = (int) getDimen(R.dimen.glance_session_ui_large_widget_icon_visitor_video_off_margin_bottom);
            ibVisitorVideoVideoOffIcon.setLayoutParams(videoIconParams);
        });
        loadVideoStateView();

        ibVisitorCameraMode = rootView.findViewById(R.id.ibVisitorCameraMode);
        ibVisitorCameraMode.setOnClickListener(v -> {
            ibVisitorCameraMode.setActivated(!ibVisitorCameraMode.isActivated());

            if (!cameraManager.isFrontCameraSelected()) { // currently back camera
                state.videoEnabled = wasVisitorVideoOffBeforeFlip;
                ibVideoState.setActivated(state.videoEnabled);

                cvVisitorBackVideoContainer.setVisibility(View.INVISIBLE);
                rlVisitorVideoContainer.setVisibility(View.VISIBLE);

                if (ibExpandContractVideo != null) {
                    ibExpandContractVideo.setVisibility(Glance.isAgentVideoEnabled() ? View.VISIBLE : View.INVISIBLE);
                }

                CommonUtils.getInstance().updateVisitorVideoState(!state.videoEnabled, this);
            } else { // currently front camera
                wasVisitorVideoOffBeforeFlip = state.videoEnabled;

                ibVideoState.setActivated(true);

                cvVisitorBackVideoContainer.setVisibility(View.VISIBLE);
                rlVisitorVideoContainer.setVisibility(View.GONE);
                ibExpandContractVideo.setVisibility(View.INVISIBLE);

                CommonUtils.getInstance().updateVisitorVideoState(false, this);
            }

            cameraManager.flipCamera(cameraManager.isFrontCameraSelected() ? R.id.aftvVisitorBackCamera : R.id.aftvVisitorVideo);

            configureIbVisitorCameraModeA11y();

            cvAgentVideoContainer.setImportantForAccessibility(ibVisitorCameraMode.isActivated() ? View.IMPORTANT_FOR_ACCESSIBILITY_YES : View.IMPORTANT_FOR_ACCESSIBILITY_NO);
            rootView.announceForAccessibility(getString(cameraManager.isFrontCameraSelected() ? R.string.glance_accessibility_visitor_front_camera_selected : R.string.glance_accessibility_visitor_back_camera_selected));

            sendNewVideosSize();
        });

        ibVisitorCameraMode.setActivated(true); // since we always start showing the front camera
        setButtonBlockedState(!state.videoEnabled, ibVisitorCameraMode);

        configureIbVisitorCameraModeA11y();

        ibEndSession = rlLargeVideoBtContainer.findViewById(R.id.ibEndSession);

        ibEndSession.setOnClickListener(v -> onClickEndSession());
        ViewCompat.replaceAccessibilityAction(ibEndSession, AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLICK,
                getString(R.string.glance_accessibility_end_session_action_description), null);

        if (!isTwoWayVideo(currentMode)) {
            setIsFullView(true);
        }

        if (!state.videoEnabled) {
            CommonUtils.getInstance().updateVisitorVideoState(true, this); // for more context, see GD-19746
        }
    }

    private void configureIbVisitorCameraModeA11y() {
        setAccessibilityLabelAndAction(ibVisitorCameraMode,
                ibVisitorCameraMode.isActivated() ? R.string.glance_accessibility_flip_to_back_camera_action_description : R.string.glance_accessibility_flip_to_front_camera_action_description,
                R.string.glance_accessibility_flip_camera_button_label);
    }

    /***
     * It loads/reloads the entire video (agent/two-way) widget view, instantiating all subviews and
     * setting gestures and listeners.
     *
     * @param rootView The widget root view.
     * @param targetMode The widget mode we want to target
     */
    protected void loadViews(View rootView, WidgetMode targetMode) {
        this.widgetRootView = rootView;

        clSessionUiVideoBtContainer = rootView.findViewById(R.id.clSessionUiVideoBtContainer);

        ibEndSession = rootView.findViewById(R.id.ibEndSession);

        loadAgentVideoViews(targetMode);

        loadVisitorVideoViews();
        if (clVisitorVideoContainer != null) { // can be null when the visitor didn't join yet or is app share
            changeVisitorVideoViewState(currentMode);
        }

        ibVideoState = rootView.findViewById(R.id.ibVideoState);

        if (setDragListener) { // only for the first load right after the agent joins
            setUpSessionViewMoveGestureHandler();
        }
        configureCommonButtonsListeners();
        configureVisitorVideoControlsState();

        if (isTwoWayVideo(targetMode)) {
            ibExpandContractVideo = rootView.findViewById(R.id.ibExpandContractVideo);
            loadIbExpandContractVideoStateView();

            if (!state.videoEnabled) {
                CommonUtils.getInstance().updateVisitorVideoState(true, this); // for more context, see GD-19746
            }
        }

        viewsLoaded = true;
        if (completionListener != null) {
            completionListener.onWidgetViewsLoaded();
        }

        rootView.post(() -> rootView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED));
    }

    private void loadAgentVideoViews(WidgetMode targetMode) {
        cvAgentVideoContainer = widgetRootView.findViewById(R.id.cvAgentVideoContainer);
        cvAgentVideoContainer.setContentDescription(getString(R.string.glance_accessibility_agent_video_on));

        clAgentVideoOffContainer = widgetRootView.findViewById(R.id.clAgentVideoOffContainer);
        clAgentVideoOffContainer.setContentDescription(getString(R.string.glance_accessibility_agent_video_off));

        if (targetMode == WidgetMode.LARGE_VIDEO) {
            rlAgentLargeVideoContainer = widgetRootView.findViewById(R.id.rlAgentLargeVideoContainer);
            clAgentVideoOffContainer = rlAgentLargeVideoContainer.findViewById(R.id.clAgentVideoOffContainer);
        }

        cvVisitorBackVideoContainer = widgetRootView.findViewById(R.id.cvVisitorBackVideoContainer);

        changeAgentVideoState(targetMode);
    }

    @Override
    public void changeAgentVideoState(boolean isAgentVideoEnabled) {
        // we are getting crashes after ending an session, this log is just to try to catch its source
        Log.d(TAG, "changeAgentVideoState: " + isAgentVideoEnabled);
        runOnMainThread(() -> changeAgentVideoState(currentMode, isAgentVideoEnabled));
    }

    protected void changeAgentVideoState(WidgetMode targetMode) {
        boolean isAgentVideoEnabled = Glance.isAgentVideoEnabled();
        changeAgentVideoState(targetMode, isAgentVideoEnabled);
    }

    protected void changeAgentVideoState(WidgetMode targetMode, boolean isAgentVideoEnabled) {
        runOnMainThread(() -> {
            if (widgetRootView != null && clAgentVideoOffContainer != null && hasAgentVideo(targetMode)) {
                clAgentVideoOffContainer.setVisibility(isAgentVideoEnabled ? View.INVISIBLE : View.VISIBLE);

                cvAgentVideoContainer.setVisibility(isAgentVideoEnabled ? View.VISIBLE : View.INVISIBLE);

                if (ibExpandContractVideo != null) {
                    ibExpandContractVideo.setVisibility(isAgentVideoEnabled ? View.VISIBLE : View.INVISIBLE);
                }

                if (widgetRootView != null) {
                    widgetRootView.announceForAccessibility(getString(isAgentVideoEnabled ? R.string.glance_accessibility_agent_video_on : R.string.glance_accessibility_agent_video_off));
                }
            }
        });
    }

    private void loadVisitorVideoViews() {
        clVisitorVideoContainer = widgetRootView.findViewById(R.id.clVisitorVideoContainer);
        if (clVisitorVideoContainer != null) { // can be null when the visitor didn't join yet or is app share
            flVisitorVideoContainer = widgetRootView.findViewById(R.id.flVisitorVideoContainer);
            aftvVisitorVideo = clVisitorVideoContainer.findViewById(R.id.aftvVisitorVideo);
            clVisitorVideoOff = clVisitorVideoContainer.findViewById(R.id.clVisitorVideoOff);
        }
    }

    protected int getVisitorVideoViewIdByMode(WidgetMode targetMode) {
        if (isTwoWayVideo(targetMode)) {
            return R.id.aftvVisitorVideo;
        } else if (targetMode == WidgetMode.LARGE_VIDEO) {
            return cameraManager.isFrontCameraSelected() ? R.id.aftvVisitorVideo : R.id.aftvVisitorBackCamera;
        }
        return -1;
    }

    protected void setUpSessionViewMoveGestureHandler() {
        runOnMainThread(() -> {
            if (this.widgetRootView != null) {
                this.widgetRootView.setOnTouchListener(widgetDragListener);
            }
        });
    }

    private void configureCommonButtonsListeners() {
        ibEndSession.setOnClickListener(v -> onClickEndSession());
        ViewCompat.replaceAccessibilityAction(ibEndSession, AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLICK,
                getString(R.string.glance_accessibility_end_session_action_description), null);
    }

    private void configureVisitorVideoControlsState() {
        loadVideoStateView();
    }

    private void loadVideoStateView() {
        if (ibVideoState != null) { // only when the visitor joins
            wasVisitorVideoOffBeforeFlip = state.videoEnabled;

            ibVideoState.setActivated(state.videoEnabled);

            ibVideoState.setOnClickListener(v -> {
                state.videoEnabled = !state.videoEnabled;
                ibVideoState.setActivated(state.videoEnabled);

                if (ibVisitorCameraMode != null) {
                    setButtonBlockedState(!state.videoEnabled, ibVisitorCameraMode);
                }

                SessionState.setVideoEnabled(mContext, state.videoEnabled);
                CommonUtils.getInstance().updateVisitorVideoState(!state.videoEnabled, this);

                onVideoStateChanged();

                configureIbVideoStateA11y();

                if (clVisitorVideoContainer != null) {
                    clVisitorVideoContainer.announceForAccessibility(getString(state.videoEnabled ? R.string.glance_accessibility_visitor_video_on : R.string.glance_accessibility_visitor_video_off));
                }
            });
            configureIbVideoStateA11y();

            // Do not allow the users to try to change their video state if the camera permission wasn't given.
            // If it was never ever requested before, do it.
            int permissionCheck = ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA);
            if (GlanceAndroidPreferences.hasCameraPermissionBeenAsked(mContext) && permissionCheck != PackageManager.PERMISSION_GRANTED) {
                setVisitorVideoUnavailable();
            } else if (!Glance.isPresenceConnected() && !GlanceAndroidPreferences.hasCameraPermissionBeenAsked(mContext)) {
                setOnPermissionResultListener(new SessionUIPermissionListener() {
                    @Override
                    public void onPermissionRequestGranted(String permission) {
                        cameraManager.onOpenCamera();
                        Glance.addVisitorVideo(getStartVideoMode());

                        sessionDialog.dismiss();
                    }

                    @Override
                    public void onPermissionRequestDenied(String permission) {
                        setVisitorVideoUnavailable();

                        sessionDialog.dismiss();
                    }
                });

                runOnMainThread(() -> showSessionDialog(new SessionDialogListenerAdapter() {
                    @Override
                    public void onAcceptTerms() {
                        sessionDialog.requestPermission(Manifest.permission.CAMERA);
                        GlanceAndroidPreferences.setCameraPermissionBeenAsked(mForegroundActivity.get(), true);
                    }

                    @Override
                    public void onDeclinedTerms() {
                        setVisitorVideoUnavailable();
                    }
                }, DialogSessionMode.PROMPT, getString(R.string.glance_first_camera_permission_prompt_warning), false));
                // if declined, reuse the code inside the IF above to hide the video visitor icon
                // if accepted, check if it will show and stream the visitor video. otherwise, make sure it works
            }
        }
    }

    private void configureIbVideoStateA11y() {
        setAccessibilityLabelAndAction(ibVideoState, state.videoEnabled ? R.string.glance_accessibility_disable_action_description : R.string.glance_accessibility_enable_action_description, state.videoEnabled ? R.string.glance_accessibility_video_on_button_label : R.string.glance_accessibility_video_off_button_label);
    }

    private void setVisitorVideoUnavailable() {
        ibVideoState.setAlpha(0f);
        ibVideoState.setEnabled(false);

        state.videoEnabled = false;
        switchVisitorVideoViews();
        CommonUtils.getInstance().updateVisitorVideoState(true, this);
    }

    protected void loadIbExpandContractVideoStateView() {
        ibExpandContractVideo.setOnClickListener(v -> {
            cameraManager.onCloseCamera();// force camera to run a fresh setup before opening again
            if (fullView) {
                onClickToContractWidget();
            } else {
                onLargeVideoMode();
            }
            setIsFullView(!fullView);

            configureIbExpandContractVideoA11y();
        });

        configureIbExpandContractVideoA11y();
    }

    private void configureIbExpandContractVideoA11y() {
        setAccessibilityLabelAndAction(ibExpandContractVideo, fullView ? R.string.glance_accessibility_contract_action_description : R.string.glance_accessibility_expand_action_description, fullView ? R.string.glance_accessibility_contract_button_label : R.string.glance_accessibility_expand_button_label);
    }

    protected void moveWidgetToNearestCorner() {
        calculateNearestCorner();

        moveWidgetToCorner(currentCorner);
    }

    protected void calculateNearestCorner() {
        int[] l = new int[2]; // 0 - left; 1 - top;
        widgetRootView.getLocationInWindow(l);
        int leftX = l[0];
        int topY = l[1];

        int screenWidth = Util.getScreenWidth();
        int screenHeight = Util.getScreenHeight();

        if (topY <= screenHeight / HALF) { // push to top
            if (leftX >= screenWidth / HALF) {
                currentCorner = WidgetCorner.TOP_RIGHT;
            } else {
                currentCorner = WidgetCorner.TOP_LEFT;
            }
        } else {
            if (leftX >= screenWidth / HALF) {
                currentCorner = WidgetCorner.BOTTOM_RIGHT;
            } else {
                currentCorner = WidgetCorner.BOTTOM_LEFT;
            }
        }
    }

    protected void moveWidgetToCorner(WidgetCorner corner) {
        currentCorner = corner;

        float[] coordinates = generateCornerCoordinates(widgetRootView);
        float leftX = coordinates[0];
        float rightX = coordinates[1];
        float topY = coordinates[2];
        float bottomY = coordinates[3];

        switch (corner) {
            case TOP_LEFT:
                widgetRootView.setX(leftX);
                widgetRootView.setY(topY);
                break;
            case TOP_RIGHT:
                widgetRootView.setX(rightX);
                widgetRootView.setY(topY);
                break;
            case BOTTOM_LEFT:
                widgetRootView.setX(leftX);
                widgetRootView.setY(bottomY);
                break;
            case BOTTOM_RIGHT:
                widgetRootView.setX(rightX);
                widgetRootView.setY(bottomY);
                break;
            default:
                break;
        }

        onMovedTabToCorner(corner);

        sendNewLocation();
    }

    private void onMovedTabToCorner(WidgetCorner corner) {
        if (currentMode == WidgetMode.TAB_MODE) {
            CardView.LayoutParams iconParams = (CardView.LayoutParams) ivSessionUiTabWidgetIcon.getLayoutParams();
            int cornerMargin = (int) getDimen(R.dimen.glance_session_ui_tab_widget_corner_side_margin);
            int iconMargin = (int) getDimen(R.dimen.glance_session_ui_tab_widget_icon_side_margin);

            int width = (int) getDimen(R.dimen.glance_session_ui_tab_widget_width);

            switch (corner) {
                case TOP_LEFT:
                case BOTTOM_LEFT:
                    widgetRootView.setX(cornerMargin);
                    iconParams.leftMargin = iconMargin;
                    iconParams.rightMargin = 0;
                    ivSessionUiTabWidgetIcon.setImageResource(R.drawable.glance_ic_session_tab_widget_right);
                    break;
                case TOP_RIGHT:
                case BOTTOM_RIGHT:
                    widgetRootView.setX(Util.getScreenWidth() - width - cornerMargin);
                    iconParams.rightMargin = iconMargin;
                    iconParams.leftMargin = 0;
                    ivSessionUiTabWidgetIcon.setImageResource(R.drawable.glance_ic_session_tab_widget_left);
                    break;
                default:
                    break;
            }
            ivSessionUiTabWidgetIcon.setLayoutParams(iconParams);
        }
    }

    protected float[] generateCornerCoordinates(View view) {
        float[] coordinates = new float[4];

        int screenWidth = Util.getScreenWidth();
        int screenHeight = Util.getScreenHeight();

        HashMap<WidgetMode, Integer> widthByMode = new HashMap<WidgetMode, Integer>() {{
            put(WidgetMode.TAB_MODE, R.dimen.glance_session_ui_tab_widget_width);
            put(WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE, R.dimen.glance_session_ui_app_share_widget_width);
            put(WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE, R.dimen.glance_session_ui_video_widget_width);
            put(WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE, R.dimen.glance_session_ui_video_widget_width);
        }};

        HashMap<WidgetMode, Integer> heightByMode = new HashMap<WidgetMode, Integer>() {{
            put(WidgetMode.TAB_MODE, R.dimen.glance_session_ui_tab_widget_height);
            put(WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE, R.dimen.glance_session_ui_app_share_widget_height);
            put(WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE, R.dimen.glance_session_ui_video_widget_container_height);
            put(WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE, R.dimen.glance_session_ui_two_way_video_widget_container_height);
        }};

        int width = getDimenAux(widthByMode);
        int height = getDimenAux(heightByMode);

        // - left X
        coordinates[0] = screenWidth * SESSION_VIDEO_WIDGET_TOP_RIGHT_CORNER_SPACE_RATE;

        // need to subtract the view width because this method places the view's left side at
        // the given X, so the view might be cut
        // - right X
        coordinates[1] = screenWidth * SESSION_VIDEO_WIDGET_BOTTOM_LEFT_CORNER_SPACE_RATE - width;

        // - top Y
        coordinates[2] = screenHeight * SESSION_VIDEO_WIDGET_TOP_RIGHT_CORNER_SPACE_RATE;

        // - bottom Y
        coordinates[3] = screenHeight * SESSION_VIDEO_WIDGET_BOTTOM_LEFT_CORNER_SPACE_RATE - height;

        return coordinates;
    }

    private int getDimenAux(Map<WidgetMode, Integer> measureByMode) {
        Integer dimenId = measureByMode.get(currentMode);
        return dimenId != null ? (int) getDimen(dimenId) : 0;
    }

    protected void dismissView(View view, boolean remove) {
        if (view != null) {
            ViewParent parentView = view.getParent();
            view.clearAnimation();
            view.setVisibility(View.GONE);
            if (remove && parentView instanceof ViewGroup) {
                ((ViewGroup) parentView).removeView(view);
            }
        }
    }

    private void switchVisitorVideoViews() {
        if (flVisitorVideoContainer != null) { // may be null in a device rotation with videoOff mode
            flVisitorVideoContainer.setVisibility(state.videoEnabled ? View.VISIBLE : View.GONE);
        }

        if (clVisitorVideoOff != null) { // may be null in a device rotation with videoOff mode
            clVisitorVideoOff.setVisibility(state.videoEnabled ? View.GONE : View.VISIBLE);
        }
    }

    protected void changeVisitorVideoViewState(WidgetMode targetMode) {
        if (widgetRootView != null) {
            switchVisitorVideoViews();

            if (cameraManager.isCameraOpened()) {
                if (cameraManager.isFrontCameraSelected()) { // camera currently showing on squared visitor video
                    cameraManager.onCloseCamera();
                    if (!state.videoEnabled) {
                        // prevent camera to open in some situations
                        cameraManager.initCameraWhenAvailable(false);
                    }
                } else {
                    // currently showing back camera on the full view, so when flipping to the front
                    // camera, we should start with it disabled, so no need to open the camera right away
                    cameraManager.flipCamera(R.id.aftvVisitorVideo, !isLargeMode(currentMode));

                    // show agent off view again
                    cvVisitorBackVideoContainer.setVisibility(View.GONE);

                    // show the squared visitor video back
                    rlVisitorVideoContainer.setVisibility(View.VISIBLE);
                    flVisitorVideoContainer.setVisibility(View.VISIBLE);
                    clVisitorVideoOff.setVisibility(state.videoEnabled ? View.GONE : View.VISIBLE);

                    ibVisitorCameraMode.setActivated(true); // front camera
                }
            } else if (state.videoEnabled) {
                if (aftvVisitorVideo.isAvailable()) { // camera is closed
                    cameraManager.onOpenCamera(clVisitorVideoContainer.getWidth(),
                            clVisitorVideoContainer.getHeight());
                } else { // camera should open but the surface is not ready yet
                    cameraManager.initCameraWhenAvailable(true);
                }
            }

            if (ibExpandContractVideo != null && isLargeMode(currentMode)) {
                if (Glance.isAgentVideoEnabled() && cameraManager.isFrontCameraSelected()) {
                    ibExpandContractVideo.setVisibility(View.VISIBLE);
                } else {
                    ibExpandContractVideo.setVisibility(View.INVISIBLE);
                }
            }

            clVisitorVideoContainer.setContentDescription(getString(state.videoEnabled ? R.string.glance_accessibility_visitor_video_on : R.string.glance_accessibility_visitor_video_off));

            if (ibVisitorCameraMode != null) {
                cvAgentVideoContainer.setImportantForAccessibility(ibVisitorCameraMode.isActivated() ? View.IMPORTANT_FOR_ACCESSIBILITY_YES : View.IMPORTANT_FOR_ACCESSIBILITY_NO);
            }

            sendNewVideosSize(targetMode);
        }
    }

    private void sendNewVideosSize() {
        sendNewVideosSize(currentMode);
    }

    private void sendNewVideosSize(WidgetMode widgetMode) {
        VisitorVideoSizeMode videoMode = null;
        int height;
        int width = 0;

        if (!isLargeMode(widgetMode)) {
            videoMode = VIDEO_SMALL_MODE;
            width = clVisitorVideoContainer.getWidth();
            height = clVisitorVideoContainer.getHeight();
            Glance.updateVisitorVideoSize(width, height, videoMode);
        } else if (cvVisitorBackVideoContainer != null) {
            videoMode = VIDEO_LARGE_MODE;
            height = cvVisitorBackVideoContainer.getHeight();
            width = height;
            cvVisitorBackVideoContainer.post(() -> {
                Glance.updateVisitorVideoSize(height, height, VIDEO_LARGE_MODE);
            });
        } else {
            height = 0;
        }

        if (videoMode != null) {
            Map<String, String> params = new HashMap<>();
            params.put("videosize", String.format("message:videosize;size:%s;previewwidth:%d;previewheight:%d", videoMode.getValue(), width, height));

            CommonUtils.getInstance().onSDKEventTriggered(this, "sendNewVideosSize", params);
        }
    }
}
