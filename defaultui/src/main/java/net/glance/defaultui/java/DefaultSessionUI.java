package net.glance.defaultui.java;

import static net.glance.android.DialogSessionMode.TWO_WAY_VIDEO_AND_APP_SHARING;
import static net.glance.android.VisitorVideoSizeMode.VIDEO_SMALL_MODE;
import static net.glance.android.WidgetVisibilityMode.FULL_MODE;
import static net.glance.android.WidgetVisibilityMode.TAB_MODE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;

import androidx.annotation.Keep;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ProcessLifecycleOwner;

import net.glance.android.DialogSessionMode;
import net.glance.android.Event;
import net.glance.android.EventConstants;
import net.glance.android.Glance;
import net.glance.android.SessionDialogListener;
import net.glance.android.SessionUICompletionListener;
import net.glance.android.SessionUIListener;
import net.glance.android.StartParams;
import net.glance.android.VideoMode;
import net.glance.android.VisitorInitParams;
import net.glance.android.VisitorListener;
import net.glance.android.WidgetCorner;
import net.glance.android.WidgetVisibilityMode;
import net.glance.android.api.GlanceTimeout;
import net.glance.defaultui.R;
import net.glance.defaultui.java.blurdialogfragment.BlurDialogEngine;
import net.glance.defaultui.java.util.CommonUtils;
import net.glance.defaultui.java.util.SessionDialogListenerAdapter;
import net.glance.glancevideo.CameraManager;
import net.glance.glancevideo.GlanceSize;
import net.glance.glancevideo.videosession.VideoSession;

import java.lang.ref.WeakReference;

public class DefaultSessionUI extends BaseSessionUIView {

    interface PendingLifecycleTask {
        void onLifecycleResume();
    }

    private WeakReference<Activity> mPreviousForegroundActivity;

    private PendingLifecycleTask pendingLifecycleTask;

    private static StartParams sessionParams;

    private SessionUIListener listener;

    private SessionDialogListener dialogListener;

    private boolean areTermsAccepted = false;

    private ViewGroup mSessionUIContainer;

    private View mSessionUIContainerView;

    private BlurDialogEngine mBlurEngine;

    private boolean foundDimensions = false;

    private boolean isVisitorVideoAdded = false;

    @Keep
    public DefaultSessionUI(Context context, Activity activity, StartParams startParams) {
        mContext = context;
        mForegroundActivity = new WeakReference<>(activity);
        mHandler = new Handler(Looper.getMainLooper());

        sessionParams = startParams;

        SessionState.reset(context); // make sure we'll have a fresh session
        state = SessionState.loadFromPreferences(context);

        initWidgetDragListener();

        runOnMainThread(() -> ProcessLifecycleOwner.get().getLifecycle().addObserver(new DefaultLifecycleObserver() {
            @Override
            public void onResume(@NonNull LifecycleOwner owner) {
                if (pendingLifecycleTask != null) {
                    pendingLifecycleTask.onLifecycleResume();
                    pendingLifecycleTask = null;
                }

                if ((isTwoWayVideo(currentMode) || isLargeMode(currentMode)) && state.videoEnabled) {
                    videoSession.resume();
                    cameraManager.onOpenCamera();
                    CommonUtils.getInstance().updateVisitorVideoState(false, DefaultSessionUI.this);
                }
            }

            @Override
            public void onPause(@NonNull LifecycleOwner owner) {
                if ((isTwoWayVideo(currentMode) || isLargeMode(currentMode)) && state.videoEnabled && !videoSession.isPaused()) {
                    videoSession.stopEncoding();
                    cameraManager.onCloseCamera();
                    CommonUtils.getInstance().updateVisitorVideoState(true, DefaultSessionUI.this);
                }
            }
        }));
    }

    public static void init(Activity activity, VisitorInitParams initParams, boolean maskKeyboard,
                            VisitorListener listener) {
        Glance.init(activity, initParams);
        Glance.maskKeyboard(maskKeyboard);
        if (listener != null) {
            Glance.addVisitorListener(listener);
        }

        StartParams startParams = new StartParams();
        startParams.setKey(initParams.getVisitorId());
        Glance.setSessionUIImplementation(DefaultSessionUI.class, startParams);
    }

    public static void init(Activity activity, StartParams startParams, boolean maskKeyboard,
                            int groupId, String visitorId, VisitorListener eventsListener) {
        init(activity, startParams, maskKeyboard, groupId, visitorId, null, eventsListener);
    }

    public static void init(Activity activity, StartParams startParams, boolean maskKeyboard,
                            int groupId, String visitorId, String glanceServer,
                            VisitorListener eventsListener) {
        sessionParams = startParams;
        Glance.init(activity, groupId, visitorId, glanceServer, eventsListener);
        Glance.maskKeyboard(maskKeyboard);
        Glance.setSessionUIImplementation(DefaultSessionUI.class, sessionParams);
    }

    public static void startSession() {
        startSession(null);
    }

    public static void startSession(@Nullable GlanceTimeout startSessionTimeout) {
        if (sessionParams != null) {
            Glance.startSession(sessionParams, false, startSessionTimeout);
        } else {
            Log.w(TAG, "DefaultSessionUI not initialized. You need to call DefaultSessionUI.init(...) first");
        }
    }

    public void setVisitorVideoAdded(boolean connected) {
        isVisitorVideoAdded = connected;
    }

    @Override
    public boolean isVisitorVideoAdded() {
        return isVisitorVideoAdded;
    }

    @Override
    public boolean isVisitorVideoStreaming() {
        return videoSession != null && videoSession.isVisitorVideoStreaming();
    }

    @Override
    public void setTermsAccepted(boolean accepted) {
        areTermsAccepted = accepted;
    }

    @Override
    public boolean areTermsAccepted() {
        return areTermsAccepted;
    }

    @Override
    public void startVisitorVideo(StartParams sparams, int groupId, boolean invokeShowWidget) {
        videoStartParams = sparams;

        if (videoStartParams.getVideo() != VideoMode.VideoOff) {
            videoSession = VideoSession.getInstance();

            boolean alreadyConnected = videoSession.isConnected();
            videoSession.initWithGroupId(groupId);
            videoSession.setListener(this);
            if (alreadyConnected) {
                // If was connected already, start again with new parameters
                // Since camera is already open, deviceConnected won't happen again
                videoSession.deviceConnected();
                videoSession.start(videoStartParams);
            }

            isVisitorVideoAdded = true;

            if (invokeShowWidget) {
                showWidgetFromStartVideoMode();
            }
        }
    }

    @Override
    public void setSessionParams(StartParams params) {
        sessionParams = params;
    }

    @Override
    public void processUserMessage(Event event) {
        String message = event.GetValue(EventConstants.ATTR_MESSAGE_KEY);
        if (TextUtils.isEmpty(message)) {
            return;
        }
        switch (message) {
            case EventConstants.ATTR_MESSAGE_WIDGET_LOCATION:
                if (!isLargeMode(currentMode)) {
                    String location = event.GetValue(EventConstants.ATTR_VALUE_LOCATION);
                    currentCorner = WidgetCorner.fromServer(location);

                    runOnMainThread(() -> moveWidgetToCorner(currentCorner));
                }
                break;
            case EventConstants.ATTR_MESSAGE_WIDGET_VISIBILITY:
                WidgetVisibilityMode visibility = WidgetVisibilityMode.getEnum(event.GetValue(EventConstants.ATTR_VALUE_VISIBILITY));

                runOnMainThread(() -> {
                    if (visibility.equals(TAB_MODE)) {
                        calculateNearestCorner();
                        animateWidgetToTabTransition();
                    } else {
                        animateTabToWidgetTransition(mSessionUIContainerView.getX());
                    }
                });
                break;
            case EventConstants.ATTR_MESSAGE_VISITOR_VIDEO_REQUESTED:
                if (videoSession != null && videoSession.isConnected()) {
                    break;
                }
                VideoMode videoMode = videoStartParams != null ? videoStartParams.getVideo() : VideoMode.VideoOff;
                VideoMode newMode;
                if (videoMode == VideoMode.VideoLargeVisitor) {
                    newMode = VideoMode.VideoLargeMultiway;
                } else {
                    newMode = VideoMode.VideoSmallMultiway;
                }
                if (videoMode == VideoMode.VideoOff || !areTermsAccepted) {
                    promptVisitorVideo(newMode);
                } else {
                    Glance.addVisitorVideo(newMode);
                }
                break;
            case EventConstants.ATTR_MESSAGE_PAUSE_VISITOR_VIDEO_REQUESTED:
                runOnMainThread(() -> ibVideoState.performClick());
                break;
            case EventConstants.ATTR_MESSAGE_VISITOR_VIDEO_SIZE:
                if (isTwoWayVideo(currentMode) || isLargeMode(currentMode)) {
                    runOnMainThread(() -> ibExpandContractVideo.performClick());
                }
                break;
        }
    }

    @Override
    public void promptVisitorVideo(VideoMode videoMode) {
        dialogListener = new SessionDialogListenerAdapter() {
            @Override
            public void onAcceptTerms() {
                Glance.addVisitorVideo(videoMode);
            }

            @Override
            public void onDeclinedTerms() {
                closeCamera();
            }
        };
        buildSessionDialog(DialogSessionMode.TWO_WAY_VIDEO_AND_APP_SHARING, dialogListener);
    }

    @Override
    public void onSessionKeyReceived(String sessionKey) {
        if (dialogListener == null) {
            dialogListener = new SessionDialogListenerAdapter() {
                @Override
                public void onEndSession(Activity activity) {
                    Glance.endSession();
                }
            };
        }
        onSessionKeyReceivedAux(sessionKey, dialogListener);
    }

    @Override
    public void onSessionKeyReceived(String sessionKey, SessionDialogListener dialogListener) {
        onSessionKeyReceivedAux(sessionKey, dialogListener);
    }

    private void onSessionKeyReceivedAux(String sessionKey, SessionDialogListener dialogListener) {
        sessionParams.setKey(sessionKey);
        buildSessionDialog(DialogSessionMode.SESSION_CODE, dialogListener);
    }

    private void updateAgentVideoViewId(WidgetMode mode) {
        // we can't reuse the same sessionView id for the agent video
        int agentSessionVideoId = mode == WidgetMode.LARGE_VIDEO ?
                R.id.svSessionLargeAgentVideo : mode == WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE ?
                R.id.svSessionAgentOnlyVideo : R.id.svSessionAgentVideo;
        Glance.setCustomAgentVideoSessionViewId(agentSessionVideoId);
    }

    @Override
    public void onAgentVideoConnected(SessionUICompletionListener listener) {
        completionListener = listener;

        String sessionKey;
        if (videoStartParams != null) {
            sessionKey = videoStartParams.getKeyAsString();
        } else {
            sessionKey = null;
        }
        videoStartParams = sessionParams; //TODO: remove when the group setting changes back to trigger EventMessageReceived
        if (sessionKey != null) {
            videoStartParams.setKey(sessionKey);
        }
        if (videoStartParams.getMainCallId() == null) {
            videoStartParams.setMainCallId(Glance.getCallId());
        }

        if (hasAgentVideo(currentMode)) {
            changeAgentVideoState(true);
            completionListener.onWidgetViewsLoaded();
        } else {
            boolean isLargeVideo = videoStartParams.getVideo() == VideoMode.VideoLargeVisitor ||
                    videoStartParams.getVideo() == VideoMode.VideoLargeMultiway;

            WidgetMode newMode = isLargeVideo ? WidgetMode.LARGE_VIDEO : WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE;
            setDragListener = true;
            showWidget(newMode);
        }
    }

    @Override
    public void deviceConnected() {
        Log.d(TAG, "deviceConnected");
        if (videoSession != null) {
            videoSession.deviceConnected();
            if (foundDimensions) {
                Log.d(TAG, "Starting video session...");
                if (videoStartParams != null) {
                    videoSession.start(videoStartParams);
                } else {
                    Log.d(TAG, "Can't start video session: videoStartParams is null");
                }
            } else {
                Log.w(TAG, "ISSUE: deviceConnected but no dimensions found");
            }
        }
    }

    @Override
    public void deviceDidUpdateDimensions(GlanceSize dimensions, GlanceSize deviceDimensions) {
        Log.d(TAG, String.format("deviceDidUpdateDimensions -- camera: %d x %d | device screen: %d x %d",
                dimensions.getWidth(), dimensions.getHeight(), deviceDimensions.getWidth(),
                deviceDimensions.getHeight()));
        foundDimensions = true;
        if (videoSession != null) {
            videoSession.updateDimensions(dimensions, deviceDimensions);
        }
    }

    @Override
    public void showSessionDialog(SessionDialogListener listener) {
        DialogSessionMode mode = sessionParams.getVideo() != VideoMode.VideoOff ?
                TWO_WAY_VIDEO_AND_APP_SHARING : null;
        showSessionDialogNoMsgAux(listener, mode);
    }

    @Override
    public void showSessionDialog(SessionDialogListener listener, DialogSessionMode mode) {
        showSessionDialogNoMsgAux(listener, mode);
    }

    @Override
    public void showSessionDialog(SessionDialogListener listener, DialogSessionMode mode, String warningMessage, boolean dismiss) {
        dialogListener = listener;
        buildSessionDialog(mode, dialogListener, warningMessage, dismiss);
    }

    private void showSessionDialogNoMsgAux(SessionDialogListener listener, DialogSessionMode mode) {
        dialogListener = listener;
        buildSessionDialog(mode, dialogListener);
    }

    @Override
    public void hideSessionDialog() {
        if (sessionDialog != null) {
            sessionDialog.dismiss();
        }
    }

    @Override
    public void onConfigurationChanged() {
        showWidget(currentMode, true);
    }

    @Override
    public void onActivityChanged(final Activity activity, boolean isChangingConfigurations) {
        mPreviousForegroundActivity = new WeakReference<>(mForegroundActivity.get());
        mForegroundActivity = new WeakReference<>(activity);
        mIsActivityChangingConfigurations = isChangingConfigurations;
        setDragListener = true;

        showWidget(currentMode);
        restoreSessionDialog(activity);
    }

    private void restoreSessionDialog(@NonNull final Activity activity) {
        Fragment dialog = ((AppCompatActivity) activity).getSupportFragmentManager().findFragmentByTag(SessionDialog.TAG);
        if (dialog instanceof SessionDialog) {
            sessionDialog = (SessionDialog) dialog;
            sessionDialog.setSessionDialogListener(dialogListener);
        }
    }

    public void onPermissionResult(String permission) {
        if (permissionListener != null) {
            int permissionCheck = ContextCompat.checkSelfPermission(mForegroundActivity.get(), Manifest.permission.CAMERA);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                permissionListener.onPermissionRequestGranted(permission);
            } else {
                permissionListener.onPermissionRequestDenied(permission);
            }
        }
    }

    private void buildSessionDialog(DialogSessionMode mode,
                                    SessionDialogListener listener) {
        buildSessionDialog(mode, listener, null, true);
    }

    @SuppressLint("CommitTransaction") //we can't call commit() otherwise it will make the dark blurred background to be hidden
    private void buildSessionDialog(DialogSessionMode mode,
                                    SessionDialogListener listener, String warningMessage, boolean dismiss) {
        runOnMainThread(() -> {
            Activity compatActivity = mForegroundActivity.get();
            if (!(compatActivity instanceof AppCompatActivity)) {
                Log.e(TAG, "Activity must be a subclass of AppCompatActivity");
                return;
            }

            FragmentManager fragmentManager = ((AppCompatActivity) compatActivity).getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            Fragment prev = fragmentManager.findFragmentByTag(SessionDialog.TAG);
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            pendingLifecycleTask = () -> {
                AppCompatActivity activity = (AppCompatActivity) mForegroundActivity.get();
                if (activity == null) {
                    return;
                }
                addBlurredDialogBackground(activity);
                sessionDialog = SessionDialog.newInstance(mode, warningMessage, dismiss);
                sessionDialog.setSessionParams(sessionParams);
                sessionDialog.setSessionDialogListener(listener);
                sessionDialog.show(fragmentManager.beginTransaction(), SessionDialog.TAG);
            };

            Lifecycle.State currentState = ProcessLifecycleOwner.get().getLifecycle().getCurrentState();
            if (currentState == Lifecycle.State.RESUMED) {
                pendingLifecycleTask.onLifecycleResume();
                pendingLifecycleTask = null;
            } else {
                Log.d(TAG, String.format("Could not show the dialog now because the current " +
                                "Lifecycle.State (%s) is different from the expected RESUMED value",
                        currentState));
            }
        });
    }

    private void addBlurredDialogBackground(Activity activity) {
        View inflatedView = View.inflate(activity, R.layout.dialog_underneath_view, null);
        ConstraintLayout.LayoutParams llp = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT);
        activity.addContentView(inflatedView, llp);
    }

    private void showWidget(WidgetMode mode) {
        showWidget(mode, false);
    }

    private void showWidget(WidgetMode mode, Boolean forceRelayout) {
        if (mode == null) {
            return;
        }

        runOnMainThread(() -> {

            Activity activity = mForegroundActivity.get();

            state = SessionState.loadFromPreferences(activity);

            ViewGroup rootView = (ViewGroup) activity.findViewById(android.R.id.content).getRootView();

            updateAgentVideoViewId(mode);

            boolean activityChanged = mode == currentMode
                    && mPreviousForegroundActivity != null
                    && mForegroundActivity != null
                    && !mPreviousForegroundActivity.get().equals(mForegroundActivity.get());

            boolean inflateView = forceRelayout || activityChanged || mSessionUIContainer == null;

            if (inflateView) {
                dismissView(mSessionUIContainer, true);
                mSessionUIContainer = (ViewGroup) activity.getLayoutInflater().inflate(R.layout.session_ui_layout, rootView, false);
                mSessionUIContainerView = null;
                rootView.addView(mSessionUIContainer); //README: not attaching the container to the activity root since we need to reuse it in all other activities

                if (activityChanged || cameraManager == null) {
                    if (cameraManager != null) {
                        cameraManager.onCloseCamera();
                    }
                    cameraManager = new CameraManager(this, activity, mSessionUIContainer, null, R.id.aftvVisitorVideo);
                    listener = cameraManager;
                }

                loadBorderView(mSessionUIContainer.findViewById(R.id.flSessionUILayout));
                loadWidgetByMode(mode);
                interruptA11yReadout();
                if (mSessionUIContainerView != null) {
                    mSessionUIContainerView.announceForAccessibility(getString(R.string.glance_accessibility_share_screen));
                }
                if (completionListener != null) {
                    completionListener.onWidgetViewsLoaded();
                }
            } else if (mode != currentMode) { // we are changing the widget mode
                loadWidgetByMode(mode);
            }
        });
    }

    @Override
    public void showWidget() {
        WidgetMode mode = getWidgetModeFromStartParams();
        setDragListener = true;

        showWidget(mode);
    }

    @Override
    protected void showWidgetFromStartVideoMode() {
        VideoMode videoMode = getStartVideoMode();

        // if we don't have a video mode set up, just keep showing the current widget
        if (videoMode != null && videoMode != VideoMode.VideoOff) {
            WidgetMode mode = getWidgetModeFromVideoMode(videoMode);
            setDragListener = true;
            showWidget(mode);
        }
    }

    @Override
    public VideoMode getStartVideoMode() {
        VideoMode presenceVideoMode = Glance.getPresenceVideoMode();
        return Glance.isPresenceConnected() ?
                (presenceVideoMode != VideoMode.VideoOff ?
                        presenceVideoMode : VideoMode.VideoSmallMultiway) : (videoStartParams != null ?
                videoStartParams.getVideo() : VideoMode.VideoOff);
    }

    @Override
    public void hide() {
        runOnMainThread(() -> {
            Log.d("SessionUI", "hide");

            closeCamera();

            hideSessionDialog();

            if (currentMode == WidgetMode.LARGE_VIDEO) {
                clLargeVideoWidgetBackground.setVisibility(View.GONE);
                mBlurEngine.onDismiss();
            }

            if (mSessionUIBorder != null) {
                mSessionUIBorder.setVisibility(View.GONE);
            }

            if (mSessionUIContainerView != null) {
                mSessionUIContainerView.announceForAccessibility(getString(R.string.glance_accessibility_stop_share_screen));

                dismissView(mSessionUIContainerView, true);
                mSessionUIContainerView = null;
            }

            if (mSessionUIContainer != null) {
                mSessionUIContainer.removeAllViews();
                dismissView(mSessionUIContainer, true);
                mSessionUIContainer = null;
            }

            SessionState.reset(mForegroundActivity.get());

            currentMode = null;
            modeBeforeExpanding = null;
            modeBeforeMinimizing = null;
            fullView = false;

            mContext = null;
            mForegroundActivity = null;
            mPreviousForegroundActivity = null;
            mIsActivityChangingConfigurations = false;
            mHandler = null;
            sessionParams = null;

            dialogListener = null;
            sessionDialog = null;
            cameraManager = null;

            isVisitorVideoAdded = false;
            areTermsAccepted = false;

            if (videoSession != null) {
                videoSession.end();
                videoSession = null;
            }
        });
    }

    private void loadWidgetByMode(WidgetMode mode) {
        if (mode == null) {
            Log.e(TAG, "Attempted to load a widget with null mode");
            return;
        }

        switch (mode) {
            case APP_SHARE_WITH_PHONE_AUDIO_MODE:
                onAppShareWithPhoneAudioMode();
                break;
            case AGENT_WITH_PHONE_AUDIO_MODE:
                onAgentWithPhoneAudioMode();
                break;
            case TWO_WAY_VIDEO_WITH_VISITOR_MODE:
                onTwoWayVideoWithVisitorMode();
                break;
            case LARGE_VIDEO:
                onLargeVideoMode();
                break;
            case TAB_MODE:
                onTabMode();
                break;
            default:
                break;
        }

        if (clVisitorVideoContainer != null) {
            int dimen = (int) getDimen(R.dimen.glance_session_ui_visitor_video_widget_height);
            Glance.updateVisitorVideoSize(dimen, dimen, VIDEO_SMALL_MODE);
        } else {
            Glance.updateVisitorVideoSize(0, 0, VIDEO_SMALL_MODE);
        }

        mSessionUIBorder.setVisibility(View.VISIBLE);
    }

    private WidgetMode getWidgetModeFromStartParams() {
        VideoMode videoMode = sessionParams.getVideo();
        return getWidgetModeFromVideoMode(videoMode);
    }

    public WidgetMode getWidgetModeFromVideoMode(VideoMode videoMode) {
        WidgetMode mode = WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE;

        if (videoMode != null) {
            switch (videoMode) {
                case VideoSmallVisitor:
                case VideoSmallMultiway:
                    mode = WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE;
                    break;
                case VideoLargeVisitor:
                case VideoLargeMultiway:
                    mode = WidgetMode.LARGE_VIDEO;
                    break;
                case VideoOff: // app share
                default:
                    break;
            }
        } else {
            Log.w(TAG, "Video mode invalid, setting APP_SHARE_WITH_PHONE_AUDIO_MODE as the default");
        }

        return mode;
    }

    @Override
    public void onClickToExpandWidget() {
        loadWidgetByMode(modeBeforeMinimizing);

        updateWidgetVisibility(FULL_MODE);
    }

    @Override
    public void onClickToContractWidget() {
        // then we are contracting the large widget that was loaded from the beginning, so we need
        // to contract it to one of the two-way-video small widget matching the session params
        if (!isTwoWayVideo(modeBeforeExpanding)) {
            modeBeforeExpanding = getWidgetModeFromVideoMode(VideoMode.VideoSmallVisitor);
        }

        updateAgentVideoViewId(modeBeforeExpanding);
        loadWidgetByMode(modeBeforeExpanding);

        clLargeVideoWidgetBackground.setVisibility(View.GONE);
        mBlurEngine.onDismiss();

        Glance.restartAgentVideo();
    }

    @Override
    public void onClickEndSession() {
        Log.d("SESSION UI", "------------END SESSION----------");
        buildSessionDialog(DialogSessionMode.END_SESSION, Glance.getVisitorSessionDialogListener());
    }

    @Override
    public void onVideoStateChanged() {
        changeVisitorVideoViewState(currentMode);
    }

    @Override
    public void onTabMode() {
        if (currentMode != WidgetMode.TAB_MODE) {
            modeBeforeMinimizing = currentMode;
        }

        loadRootView(WidgetMode.TAB_MODE);
        loadTabWidget(mSessionUIContainerView);

        currentMode = WidgetMode.TAB_MODE;

        moveAndShowWidget();
        setUpSessionViewMoveGestureHandler();
        updateWidgetVisibility(TAB_MODE);
    }

    @Override
    public void onAppShareWithPhoneAudioMode() {
        onAppShareModeAux(WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE);

        currentMode = WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE;
    }

    @Override
    public void onAgentWithPhoneAudioMode() {
        onAgentModeAux(WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE);

        currentMode = WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE;
    }

    @Override
    public void onTwoWayVideoWithVisitorMode() {
        loadTwoWayVideoLayout(WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE);

        currentMode = WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE;
    }

    @Override
    public void onLargeVideoMode() {
        updateAgentVideoViewId(WidgetMode.LARGE_VIDEO);

        loadRootView(WidgetMode.LARGE_VIDEO);

        // preparing the background view to be blurried
        clLargeVideoWidgetBackground = mSessionUIContainer.findViewById(R.id.session_ui_large_video_background);
        clLargeVideoWidgetBackground.setBackgroundColor(getColor(R.color.glance_session_ui_large_video_widget_background_color));
        clLargeVideoWidgetBackground.setVisibility(View.VISIBLE);
        clLargeVideoWidgetBackground.setClickable(true);

        // blurring the background
        mBlurEngine = new BlurDialogEngine(mForegroundActivity.get());
        mBlurEngine.setBlurRadius(3);
        mBlurEngine.setDownScaleFactor(3f);
        mBlurEngine.setBlurActionBar(true);
        mBlurEngine.onResume();

        loadLargeVideoWidget(mSessionUIContainerView);

        mSessionUIContainerView.setVisibility(View.VISIBLE);

        modeBeforeExpanding = currentMode;
        currentMode = WidgetMode.LARGE_VIDEO;

        Glance.restartAgentVideo();
    }

    @SuppressWarnings("SameParameterValue")
    private void onAppShareModeAux(WidgetMode targetMode) {
        loadRootView(targetMode);
        loadAppShareView(mSessionUIContainerView);

        moveAndShowWidget(showWidgetOnLoadingLayout);
    }

    @SuppressWarnings("SameParameterValue")
    private void onAgentModeAux(WidgetMode targetMode) {
        loadRootView(targetMode);
        loadViews(mSessionUIContainerView, targetMode);

        moveAndShowWidget();
    }

    /***
     * In order to make things easier, we split the video widget layouts into two main xml files,
     * one for agent mode only and another for the two-way-video scenario. So it's necessary to
     * re-assign the main container view component in order to load the required views.
     */
    @SuppressWarnings("SameParameterValue")
    private void loadTwoWayVideoLayout(WidgetMode targetMode) {
        loadRootView(targetMode);
        loadViews(mSessionUIContainerView, targetMode);

        moveAndShowWidget();
    }

    private void loadRootView(WidgetMode targetMode) {
        int containerViewId = mSessionUIContainerView == null ? -1 : mSessionUIContainerView.getId();

        boolean isTab = targetMode == WidgetMode.TAB_MODE;
        boolean isAppShare = targetMode == WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE;
        boolean isAgentVideo = targetMode == WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE;
        boolean isLargeVideo = targetMode == WidgetMode.LARGE_VIDEO;

        if (containerViewId == -1) {
            loadRootViewAux(targetMode);
        } else if ((isTab && containerViewId != R.id.session_ui_tab_widget)
                || (isAppShare && containerViewId != R.id.session_ui_app_share)
                || (isAgentVideo && containerViewId != R.id.session_ui_agent_video)
                || (!isAgentVideo && containerViewId != R.id.session_ui_agent_and_visitor_video)
                || (isLargeVideo && containerViewId != R.id.session_ui_large_video)) {
            dismissView(mSessionUIContainerView, false);

            loadRootViewAux(targetMode);
        } // else: we trying to reuse the same layout, so no need for changes
    }

    private void loadRootViewAux(WidgetMode targetMode) {
        int layoutId = 0;
        boolean changeCameraManagerRoot = false;

        switch (targetMode) {
            case TAB_MODE:
                layoutId = R.id.session_ui_tab_widget;
                break;
            case APP_SHARE_WITH_PHONE_AUDIO_MODE:
                layoutId = R.id.session_ui_app_share;
                break;
            case AGENT_WITH_PHONE_AUDIO_MODE:
                layoutId = R.id.session_ui_agent_video;
                break;
            case TWO_WAY_VIDEO_WITH_VISITOR_MODE:
                layoutId = R.id.session_ui_agent_and_visitor_video;
                changeCameraManagerRoot = true;
                break;
            case LARGE_VIDEO:
                layoutId = R.id.session_ui_large_video;
                changeCameraManagerRoot = true;
            default:
                break;
        }

        mSessionUIContainerView = mSessionUIContainer.findViewById(layoutId);

        if (changeCameraManagerRoot) {
            cameraManager.onVideoSurfaceViewChanged(getVisitorVideoViewIdByMode(targetMode));

            cameraManager.initCameraWhenAvailable(shouldOpenCameraWhenAvailable != null ? shouldOpenCameraWhenAvailable : state.videoEnabled);
            shouldOpenCameraWhenAvailable = null;

            cameraManager.onRootViewChanged(mSessionUIContainerView);
        }
    }

    private void moveAndShowWidget() {
        moveAndShowWidget(true);
    }

    private void moveAndShowWidget(boolean show) {
        runOnMainThread(() -> mSessionUIContainerView.post(() -> {
            // height and width will be ready to use
            moveWidgetToCorner(currentCorner);

            if (show) {
                mSessionUIContainerView.setVisibility(View.VISIBLE);
            }
        }));
    }

    // VideoSessionListener methods
    @Override
    public void videoSessionDidStartVideoCapture(VideoSession videoSession) {
        Log.d(TAG, "videoSessionDidStartVideoCapture");
    }

    @Override
    public void videoSessionDidConnectStreamer(VideoSession videoSession) {
        Log.d(TAG, "videoSessionDidConnectStreamer");
    }

    @Override
    public void videoSessionDidDisconnnectStreamer(VideoSession videoSession) {
        Log.d(TAG, "videoSessionDidDisconnnectStreamer");
    }

    @Override
    public void videoSessionDidConnectVideoSource(VideoSession videoSession) {
        Log.d(TAG, "videoSessionDidConnectVideoSource");
    }

    @Override
    public void videoSessionDidDisconnectVideoSource(VideoSession videoSession) {
        Log.d(TAG, "videoSessionDidDisconnectVideoSource");
    }

    @Override
    public void videoSessionDidFailToConnectVideoSource(VideoSession videoSession, Error error) {
        Log.d(TAG, "videoSessionDidFailToConnectVideoSource");
    }

    @Override
    public void videoSessionDidFailConnectStreamer(VideoSession videoSession, Error error) {
        Log.d(TAG, "videoSessionDidFailConnectStreamer");
    }

    @Override
    public void videoSessionDidStart(VideoSession videoSession) {
        Log.d(TAG, "videoSessionDidStart");
    }

    @Override
    public void videoSessionDidEnd(VideoSession videoSession) {
        Log.d(TAG, "videoSessionDidEnd");
    }

    @Override
    public void videoSessionWillStartStreaming(VideoSession videoSession) {
        Log.d(TAG, "videoSessionWillStartStreaming");
    }

    @Override
    public void videoSessionWillStopStreaming(VideoSession videoSession) {
        Log.d(TAG, "videoSessionWillStopStreaming");
    }

    @Override
    public void videoSessionWillChangeQuality(VideoSession videoSession) {
        Log.d(TAG, "videoSessionWillChangeQuality");
    }

    @Override
    public void videoSessionInvitation(VideoSession videoSession, String sessionType, String username, String sessionKey) {
        Log.d(TAG, "videoSessionInvitation");
    }

    @Override
    public void videoSessionEncoderSurfaceAndTexture(Surface surface, SurfaceTexture surfaceTexture) {
        if (this.listener != null) {
            mHandler.post(() -> {
                listener.sessionUIEncoderSurfaceAndTexture(surface, surfaceTexture);
                if (videoSession != null) {
                    videoSession.encoderSurfaceAttached(surface);
                }
            });
        }
    }

    @Override
    public void videoSessionDidResizeDimensions(int width, int height) {
        Log.d(TAG, String.format("videoSessionDidResizeDimensions: %s x %s", width,
                height));
        if (this.listener != null) {
            this.listener.sessionUIDidResizeDimensions(width, height);
        }
    }
}