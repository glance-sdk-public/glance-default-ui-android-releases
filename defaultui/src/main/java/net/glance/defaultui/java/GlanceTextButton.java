package net.glance.defaultui.java;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;

import net.glance.defaultui.R;

public class GlanceTextButton extends androidx.appcompat.widget.AppCompatButton {

    private enum ButtonType {
        Positive(0),
        @SuppressWarnings("unused") // might be used by the customers
        Negative(1);

        public final int type;

        ButtonType(int type) {
            this.type = type;
        }
    }

    public GlanceTextButton(@NonNull Context context) {
        super(context, null, R.style.GlanceDialogButtonTextStyle); //TODO: already tried to pass -1
        init(context, null);
    }

    public GlanceTextButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs, R.style.GlanceDialogButtonTextStyle);
        init(context, attrs);
    }

    @SuppressWarnings("unused") // @defStyleAttr might be used by some specific theme configuration
    public GlanceTextButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, R.style.GlanceDialogButtonTextStyle);
        init(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measureWidth(widthMeasureSpec),
                measureHeight(heightMeasureSpec));
    }

    private int measureWidth(int measureSpec) {
        int preferred = (int) getResources().getDimension(R.dimen.glance_dialog_bt_width);
        preferred = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, preferred, getResources().getDisplayMetrics());
        return getMeasurement(measureSpec, preferred);
    }

    private int measureHeight(int measureSpec) {
        int preferred = (int) getResources().getDimension(R.dimen.glance_dialog_bt_height);
        preferred = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, preferred, getResources().getDisplayMetrics());
        return getMeasurement(measureSpec, preferred);
    }

    private int getMeasurement(int measureSpec, int preferred) {
        int specSize = MeasureSpec.getSize(measureSpec);
        int measurement;

        switch (MeasureSpec.getMode(measureSpec)) {
            case MeasureSpec.EXACTLY:
                // This means the width of this view has been given.
                measurement = specSize;
                break;
            case MeasureSpec.AT_MOST:
                // Take the minimum of the preferred size and what
                // we were told to be.
                measurement = Math.min(preferred, specSize);
                break;
            default:
                measurement = preferred;
                break;
        }

        return measurement;
    }

    private void init(Context context, AttributeSet attrs) {
        float scaledDensity = getResources().getDisplayMetrics().scaledDensity;
        int maxFontSize = Math.round(getTextSize() / scaledDensity);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(this, 2, maxFontSize, 1, TypedValue.COMPLEX_UNIT_SP);

        if (attrs != null) {
            TypedValue typedValue = new TypedValue();
            int[] attribute = new int[]{R.styleable.GlanceTextButton_glance_text_bt_type};
            TypedArray array = context.obtainStyledAttributes(typedValue.resourceId, attribute);
            int type = array.getInt(0, ButtonType.Positive.type);
            array.recycle();

            if (type != ButtonType.Positive.type) {
                switch (type) {
                    case 0:
                        setPositive();
                        break;
                    case 1:
                        setNegative();
                        break;
                    default:
                        break;
                }
            }
        } else {
            setPositive();
        }
    }

    private void setPositive() {
        setBackgroundResource(R.drawable.glance_dialog_rounded_primary_button_shape);
        setText(R.string.glance_accept);
    }

    private void setNegative() {
        setBackgroundResource(R.drawable.glance_dialog_rounded_secondary_button_shape);
        setText(R.string.glance_decline);
    }
}
