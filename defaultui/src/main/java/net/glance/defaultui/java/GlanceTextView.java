package net.glance.defaultui.java;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ScaleXSpan;
import android.util.AttributeSet;
import android.util.TypedValue;

import androidx.core.widget.TextViewCompat;

import net.glance.defaultui.R;

public class GlanceTextView extends androidx.appcompat.widget.AppCompatTextView {

    public enum TextType {
        Default(-1),
        Terms(0),
        Brand(1);

        public final int type;

        TextType(int type) {
            this.type = type;
        }

        public static TextType valueOfLabel(int type) {
            for (TextType e : values()) {
                if (e.type == type) {
                    return e;
                }
            }
            return Default;
        }
    }

    private TextType type;

    public GlanceTextView(Context context) {
        super(context);
        init(context, null, -1);
    }

    public GlanceTextView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, -1);
    }

    public GlanceTextView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        float scaledDensity = getResources().getDisplayMetrics().scaledDensity;
        int maxFontSize = Math.round(getTextSize() / scaledDensity);
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(this, 2, maxFontSize, 1, TypedValue.COMPLEX_UNIT_SP);

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.GlanceTextView, defStyle, 0);
            int type = a.getInt(R.styleable.GlanceTextView_glance_textview_special_type, TextType.Default.type);
            this.type = TextType.valueOfLabel(type);
            a.recycle();

            if (type != TextType.Default.type) {
                switch (type) {
                    case 0:
                        setupTermsText();
                        break;
                    case 1:
                        setupBrandText();
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public TextType getType() {
        return type;
    }

    public void setType(TextType type) {
        this.type = type;
    }

    private void setupTermsText() {
        setPaintFlags(getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    private void setupBrandText() {
        // setting letter spacing - backwards compatible solution
        CharSequence originalText = getText();
        if (originalText == null) return;

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < originalText.length(); i++) {
            builder.append(originalText.charAt(i));
            if (i + 1 < originalText.length()) {
                builder.append("\u00A0");
            }
        }

        float spacing = getResources().getDimension(R.dimen.glance_dialog_brand_letter_spacing);
        SpannableString finalText = new SpannableString(builder.toString());
        if (builder.toString().length() > 1) {
            for (int i = 1; i < builder.toString().length(); i += 2) {
                finalText.setSpan(new ScaleXSpan((spacing + 1) / 10), i, i + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        super.setText(finalText, BufferType.SPANNABLE);
    }
}
