package net.glance.defaultui.java;

import static net.glance.android.DialogSessionMode.END_SESSION;
import static net.glance.android.DialogSessionMode.SESSION_CODE;
import static net.glance.android.DialogSessionMode.WAITING_FOR_AGENT;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;

import net.glance.android.DialogSessionMode;
import net.glance.android.Glance;
import net.glance.android.GlanceAndroidPreferences;
import net.glance.android.SessionUI;
import net.glance.android.Util;
import net.glance.defaultui.R;

public class SessionDialog extends BaseSessionDialog {

    public static String TAG = "Session Dialog";

    private final static String REQUEST_CAMERA_PERMISSION_STATE = "com.glance.session.camera_permission_state";

    private boolean isRequestingCameraPermission;

    public static SessionDialog newInstance(DialogSessionMode mode, String warningMessage, Boolean dismiss) {
        SessionDialog sessionDialog = new SessionDialog();

        Bundle args = new Bundle();

        args.putSerializable(DIALOG_MODE, mode);
        args.putBoolean(DIALOG_DISMISS_KEY, dismiss);
        args.putString(DIALOG_MESSAGE_RES_ID_KEY, warningMessage);

        sessionDialog.setArguments(args);
        return sessionDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            isRequestingCameraPermission = savedInstanceState.getBoolean(REQUEST_CAMERA_PERMISSION_STATE, false);
        }
        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        setUpEvents();
        setupTermsNConditionsView();

        return rootView;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(REQUEST_CAMERA_PERMISSION_STATE, isRequestingCameraPermission);
        super.onSaveInstanceState(outState);
    }

    private void setUpEvents() {
        ibCloseDialog.setOnClickListener(v -> mHandler.post(() -> setMode(END_SESSION)));

        btAcceptTerms.setOnClickListener(positiveClickListener());

        btDeclineTerms.setOnClickListener(negativeClickListener());
    }

    private View.OnClickListener positiveClickListener() {
        return v -> {
            Bundle bundle = getArguments();
            /* Sometimes we need the dialog instance to be attached on the activity until some
              other task is done. In order to use the Fragment instance to deal with Permissions
              there, we need it to be alive otherwise, it will crash. The default behavior was to
              dismiss the dialog right after accepting/declining the prompt, but when we request
              a permission, we need to keep the dialog alive until the user takes a decision,
              so we can proceed with the flow.
             */
            if (bundle != null && bundle.getBoolean(DIALOG_DISMISS_KEY)) {
                dismiss();
            }

            if (sessionDialogListener != null) {
                if (mode == END_SESSION) {
                    sessionDialogListener.onEndSession(activity);
                } else {
                    cameraManager.onCloseCamera();
                    sessionDialogListener.onAcceptTerms();
                }
            } else {
                Log.w(TAG, "SessionDialogListener not provided, skipping...");
            }
        };
    }

    private View.OnClickListener negativeClickListener() {
        return v -> {
            cameraManager.onCloseCamera();

            boolean sendTermsDeclined = mode != END_SESSION;

            if (previousMode != null && (previousMode == WAITING_FOR_AGENT || previousMode == SESSION_CODE)) {
                mHandler.post(() -> {
                    setMode(previousMode);
                    previousMode = null;
                });
            } else {
                previousMode = null;
                mode = null;

                dismiss();
            }


            if (sendTermsDeclined && sessionDialogListener != null) {
                sessionDialogListener.onDeclinedTerms();
            }
        };
    }

    @Override
    public void onPromptMode(String promptMessage) {
        changeDialogWidth(SESSION_CODE_DIALOG_WIDTH);

        ibCloseDialog.setVisibility(View.GONE);

        clVisitorVideoContainer.setVisibility(View.GONE);
        clVisitorVideoControlButtonsContainer.setVisibility(View.GONE);
        clSessionCodeContainer.setVisibility(View.GONE);
        gtvTermsAndConditions.setVisibility(View.GONE);
        gtvPoweredByGlance.setVisibility(View.GONE);

        gtvHeader.setText(R.string.glance_prompt_dialog_title);
        gtvDescription.setText(promptMessage);

        btAcceptTerms.setText(R.string.glance_no);
        btAcceptTerms.setOnClickListener(negativeClickListener());
        btDeclineTerms.setText(R.string.glance_yes);
        btDeclineTerms.setOnClickListener(positiveClickListener());
    }

    @Override
    public void onWarningMode(String message) {
        changeDialogWidth(SESSION_CODE_DIALOG_WIDTH);

        ibCloseDialog.setVisibility(View.GONE);

        clVisitorVideoContainer.setVisibility(View.GONE);
        clVisitorVideoControlButtonsContainer.setVisibility(View.GONE);
        clSessionCodeContainer.setVisibility(View.GONE);
        gtvTermsAndConditions.setVisibility(View.GONE);
        gtvPoweredByGlance.setVisibility(View.GONE);

        gtvHeader.setText(R.string.glance_warning_dialog_title);
        gtvDescription.setText(message != null ? message : getString(R.string.glance_general_error_message));

        gtvTermsAndConditions.setVisibility(Glance.arePresenceTermsDisplayed() ? View.VISIBLE : View.GONE);

        btDeclineTerms.setVisibility(View.GONE);
        btAcceptTerms.setText(R.string.glance_accept);
        btAcceptTerms.setOnClickListener(Glance.arePresenceTermsDisplayed() ? positiveClickListener() : v -> dismiss());
    }

    @Override
    public void onTermsAndConditionsMode() {
        changeDialogTop(TERMS_DIALOG_TOP);
        changeDialogHeight(TERMS_DIALOG_HEIGHT);
        changeDialogWidth(TERMS_DIALOG_WIDTH);

        clDialogSessionContainer.setPadding(clDialogSessionContainer.getPaddingLeft(),
                clDialogSessionContainer.getPaddingTop(),
                clDialogSessionContainer.getPaddingRight(), 0); // avoid the buttons to be cut

        clVisitorVideoContainer.setVisibility(View.GONE);
        clVisitorVideoControlButtonsContainer.setVisibility(View.GONE);

        gtvHeader.setVisibility(View.GONE);
        gtvDescription.setVisibility(View.GONE);
        gtvTermsAndConditions.setVisibility(View.GONE);
        gtvPoweredByGlance.setVisibility(View.GONE);

        clTermsAndConditionsContainer.setVisibility(View.VISIBLE);
        Bundle arguments = getArguments();
        wvTermsAndConditions.loadUrl(arguments != null ? arguments.getString(DIALOG_TERMS_URL_KEY, "") : "");
    }

    @Override
    public void onSessionKeyMode(String sessionKey) {
        changeDialogWidth(SESSION_CODE_DIALOG_WIDTH);

        showCloseButton();
        setCancelable(false);
        anchorViewToCloseButton(gtvHeader, R.dimen.glance_dialog_header_margin_top_close_button);

        gtvHeader.setText(R.string.glance_agent_code);

        gtvDescription.setVisibility(View.GONE);
        clAcceptDeclineBtsContainer.setVisibility(View.GONE);
        gtvTermsAndConditions.setVisibility(View.GONE);

        showSessionCode(sessionKey);
    }

    @Override
    public void onWaitingSessionMode() {
        changeDialogWidth(SESSION_CODE_DIALOG_WIDTH);

        showCloseButton();
        setCancelable(false);
        anchorViewToCloseButton(clWaitingSessionAnimContainer, R.dimen.glance_dialog_header_margin_top_close_button);

        clWaitingSessionAnimContainer.setVisibility(View.VISIBLE);
        wvWaitingSessionAnim.loadUrl(WAITING_FOR_AGENT_ANIMATION_PATH);

        gtvHeader.setText(R.string.glance_waiting_for_the_agent);

        gtvDescription.setVisibility(View.GONE);
        clAcceptDeclineBtsContainer.setVisibility(View.GONE);
        gtvTermsAndConditions.setVisibility(View.GONE);
        gtvPoweredByGlance.setVisibility(View.GONE);

        anchorViews(clDialogSessionContainer, gtvHeader, ConstraintSet.TOP, clWaitingSessionAnimContainer,
                ConstraintSet.BOTTOM, R.dimen.glance_dialog_header_margin_top_close_button);
    }

    @Override
    public void onVisitorVideoWithControlsMode() {
        clVisitorVideoContainer.setVisibility(View.VISIBLE);

        clVisitorVideoControlButtonsContainer.setVisibility(View.VISIBLE);

        int permissionCheck = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
        if (!isRequestingCameraPermission) {
            if (GlanceAndroidPreferences.hasCameraPermissionBeenAsked(activity)) {
                if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    changeVisitorVideoViewState(); // happy path
                } else {
                    mHandler.post(() -> onWarningMode(getString(R.string.glance_denied_camera_permission_warning, Util.getApplicationName(activity))));
                }
            } else if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                doRequestCameraPermission();
            }
        } else {
            final DefaultSessionUI sessionUI = getDefaultSessionUI();
            if (sessionUI != null) {
                sessionUI.setOnPermissionResultListener(getDialogCameraPermissionResultListener());
            }
        }

        anchorViews(clDialogSessionContainer, gtvHeader, ConstraintSet.TOP, clVisitorVideoControlButtonsContainer,
                ConstraintSet.BOTTOM, R.dimen.glance_dialog_header_margin_top_close_button);

        boolean presenceEnabled = Glance.isPresenceConnected();

        if (presenceEnabled) {
            sendPresenceTermsDisplayed();
        }

        int headerStringId = presenceEnabled ? R.string.glance_two_way_video_presence_sharing_dialog_title : R.string.glance_two_way_video_sharing_dialog_title;
        int descriptionStringId = presenceEnabled ? R.string.glance_two_way_video_presence_dialog_description : R.string.glance_two_way_video_sharing_dialog_description;

        gtvHeader.setText(headerStringId);
        gtvDescription.setText(descriptionStringId);
    }

    private void doRequestCameraPermission() {
        final DefaultSessionUI sessionUI = getDefaultSessionUI();
        if (sessionUI != null) {
            sessionUI.setOnPermissionResultListener(getDialogCameraPermissionResultListener());
        }
        requestPermission(Manifest.permission.CAMERA);
        GlanceAndroidPreferences.setCameraPermissionBeenAsked(activity, true);
        isRequestingCameraPermission = true;
    }

    @Override
    public void onVisitorVideoWithControlsAndCodeMode() {
        changeDialogWidth(VISITOR_VIDEO_WITHSESSION_CODE_DIALOG_WIDTH);

        showCloseButton();
        setCancelable(false);
        anchorViewToCloseButton(clVisitorVideoContainer, R.dimen.glance_dialog_video_margin_top_close_button);

        wvTermsAndConditions.setVisibility(View.GONE);
        gtvDescription.setVisibility(View.GONE);
        clAcceptDeclineBtsContainer.setVisibility(View.GONE);

        clVisitorVideoContainer.setVisibility(View.VISIBLE);
        clVisitorVideoControlButtonsContainer.setVisibility(View.VISIBLE);

        gtvHeader.setVisibility(View.VISIBLE);
        gtvHeader.setText(R.string.glance_agent_code);

        anchorViews(clDialogSessionContainer, gtvHeader, ConstraintSet.TOP, clVisitorVideoControlButtonsContainer,
                ConstraintSet.BOTTOM, R.dimen.glance_dialog_header_margin_top_close_button);

        showSessionCode("- -");

        gtvTermsAndConditions.setVisibility(View.GONE);
    }

    @Override
    public void onEndSessionMode() {
        changeDialogWidth(END_SESSION_DIALOG_WIDTH);

        clWaitingSessionAnimContainer.setVisibility(View.GONE);

        ibCloseDialog.setVisibility(View.GONE);
        clVisitorVideoContainer.setVisibility(View.GONE);
        clVisitorVideoControlButtonsContainer.setVisibility(View.GONE);
        clSessionCodeContainer.setVisibility(View.GONE);
        gtvDescription.setVisibility(View.GONE);
        gtvTermsAndConditions.setVisibility(View.GONE);
        gtvPoweredByGlance.setVisibility(View.GONE);

        clAcceptDeclineBtsContainer.setVisibility(View.VISIBLE);
        gtvHeader.setText(R.string.glance_end_session_title);

        btAcceptTerms.setText(R.string.glance_no);
        btAcceptTerms.setOnClickListener(negativeClickListener());
        btDeclineTerms.setText(R.string.glance_yes);
        btDeclineTerms.setOnClickListener(positiveClickListener());
    }

    @Override
    public void onVideoStateChanged() {
        changeVisitorVideoViewState();
    }

    /**
     * Anchor the view top side to the close button bottom side
     *
     * @param view the view to be used as the link source
     */
    private void anchorViewToCloseButton(View view, int marginDimenId) {
        anchorViews(clDialogSessionContainer, view, ConstraintSet.TOP, ibCloseDialog,
                ConstraintSet.BOTTOM, marginDimenId);
    }

    private void showSessionCode(String sessionKey) {
        clSessionCodeContainer.setVisibility(View.VISIBLE);
        gtvSessionCode.setText(sessionKey);

        boolean isVisible = activity.getResources().getBoolean(R.bool.GLANCE_DIALOG_SHOW_GLANCE_TEXT);
        gtvPoweredByGlance.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);

        anchorViews(clDialogSessionContainer, gtvPoweredByGlance, ConstraintSet.TOP, clSessionCodeContainer,
                ConstraintSet.BOTTOM, R.dimen.glance_dialog_header_margin_top_close_button);
    }
}
