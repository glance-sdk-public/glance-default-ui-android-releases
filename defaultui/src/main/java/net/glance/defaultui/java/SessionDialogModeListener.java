package net.glance.defaultui.java;

public interface SessionDialogModeListener {

    void onPromptMode(String promptMessage);

    void onWarningMode(String message);

    void onTermsAndConditionsMode();

    void onSessionKeyMode(String sessionKey);

    void onWaitingSessionMode();

    void onVisitorVideoWithControlsMode();

    void onVisitorVideoWithControlsAndCodeMode();

    void onEndSessionMode();
}
