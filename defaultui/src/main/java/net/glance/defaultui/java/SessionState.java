package net.glance.defaultui.java;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

// Class to hold any UI state we might want to persist
class SessionState {

    private static final String TAG = "GlanceSessionState";

    private static final String PREFS_NAME = "PREF_GLANCE_VISITOR";

    public static final String PREF_VIDEO_ENABLED = "PREF_VIDEO_ENABLED";

    // Visitor video controls
    boolean videoEnabled;

    private static SharedPreferences visitorPreferences;

    private static SharedPreferences.Editor visitorPreferencesEditor;

    public SessionState() {
    }

    public static SessionState loadFromPreferences(Context context) {
        SessionState instance = new SessionState();

        instance.videoEnabled = isVideoEnabled(context);

        return instance;
    }

    private static void initiatePreferencesIfNull(Context context) {
        if (visitorPreferences == null) {
            visitorPreferences = context.getSharedPreferences(PREFS_NAME, 0);
            visitorPreferencesEditor = visitorPreferences.edit();
        }
    }

    private static void saveChanges() {
        visitorPreferencesEditor.apply();
    }

    public static void reset(Context context) {
        if (context != null) {
            initiatePreferencesIfNull(context);
            visitorPreferences.edit().clear().apply();
        } else {
            Log.w(TAG, "Could not reset since context is null");
        }
    }

    public static boolean isVideoEnabled(Context context) {
        if (context != null) {
            initiatePreferencesIfNull(context);
            return visitorPreferences.getBoolean(PREF_VIDEO_ENABLED, true);
        } else {
            Log.w(TAG, "Could not check isVideoEnabled since context is null");
            return false;
        }
    }

    public static void setVideoEnabled(Context context, boolean enabled) {
        if (context != null) {
            initiatePreferencesIfNull(context);
            visitorPreferencesEditor.putBoolean(PREF_VIDEO_ENABLED, enabled);
            saveChanges();
        } else {
            Log.w(TAG, "Could not setVideoEnabled since context is null");
        }
    }
}
