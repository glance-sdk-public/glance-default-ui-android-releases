package net.glance.defaultui.java;

public interface SessionUIPermissionListener {

    void onPermissionRequestGranted(String permission);

    void onPermissionRequestDenied(String permission);
}
