package net.glance.defaultui.java;

public interface SessionUIWidgetCommonControlListener {

    void onClickToExpandWidget(); // from tab mode

    void onClickToContractWidget(); // from large to the previous mode

    void onClickEndSession();

}
