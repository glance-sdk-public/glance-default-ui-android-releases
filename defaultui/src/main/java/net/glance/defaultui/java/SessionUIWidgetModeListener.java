package net.glance.defaultui.java;

/***
 *  Interface that will be used to define how the widget should look like depending on the options
 *  passed into the start session. Each method consider that all checkings were already made
 *  (ex.: video available?) so we just need to set the views up accordingly.
 */
public interface SessionUIWidgetModeListener {

    void onTabMode();

    void onAppShareWithPhoneAudioMode(); // only the blue headphone square with the end session

    void onAgentWithPhoneAudioMode(); // only the end session button

    void onTwoWayVideoWithVisitorMode(); // both agent and visitor videos + visitor video and end session button

    void onLargeVideoMode();
}
