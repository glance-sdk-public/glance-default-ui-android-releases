package net.glance.defaultui.java;

public interface VisitorVideoControlListener {

    void onVideoStateChanged();
}
