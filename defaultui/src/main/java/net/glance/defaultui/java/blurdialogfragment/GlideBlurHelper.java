package net.glance.defaultui.java.blurdialogfragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Simple helper used to blur a bitmap thanks to render script.
 */
final class GlideBlurHelper {

    /**
     * Log cat
     */
    private static final String TAG = GlideBlurHelper.class.getSimpleName();

    /**
     * Non instantiable class.
     */
    private GlideBlurHelper() {

    }

    /**
     * blur a given bitmap
     *
     * @param sentBitmap       bitmap to blur
     * @param radius           blur radius
     * @param canReuseInBitmap true if bitmap must be reused without blur
     * @param context          used by RenderScript, can be null if RenderScript disabled
     * @return blurred bitmap
     */
    public static Bitmap doBlur(Bitmap sentBitmap, int radius, boolean canReuseInBitmap, Context context) {
        final Bitmap[] blurredBitmap = new Bitmap[1];
        CountDownLatch latch = new CountDownLatch(1);
        ExecutorService executor = Executors.newSingleThreadExecutor();

        Bitmap bitmap;

        if (canReuseInBitmap) {
            bitmap = sentBitmap;
        } else {
            bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
        }

        executor.execute(() -> {
            Glide.with(context)
                    .asBitmap()
                    .load(bitmap)
                    .transform(new BlurTransformation(radius))
                    .into(new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, Transition<? super Bitmap> transition) {
                            blurredBitmap[0] = resource;
                            latch.countDown();
                        }

                        @Override
                        public void onLoadCleared(Drawable placeholder) {
                            // Handle cleanup if needed
                        }

                        @Override
                        public void onLoadFailed(Drawable errorDrawable) {
                            blurredBitmap[0] = null;
                            latch.countDown();
                        }
                    });

            try {
                latch.await();
            } catch (InterruptedException e) {
                Log.e(TAG, e.toString());
                blurredBitmap[0] = null;
            }
        });

        executor.shutdown();
        return blurredBitmap[0];
    }
}
