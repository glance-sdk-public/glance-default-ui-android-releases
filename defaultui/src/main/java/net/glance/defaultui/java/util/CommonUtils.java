package net.glance.defaultui.java.util;

import android.util.Pair;

import net.glance.android.Glance;
import net.glance.defaultui.java.BaseSessionUI;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommonUtils {

    private static CommonUtils instance;

    public static CommonUtils getInstance() {
        if (instance == null) {
            instance = new CommonUtils();
        }
        return instance;
    }

    public void updateVisitorVideoState(boolean paused, BaseSessionUI baseSessionUIInstance) {
        Glance.updateVisitorVideoStatus(paused);

        Map<String, String> params = new HashMap<String, String>() {{
            put("visitorvideopaused", String.format("paused:%b", paused));
        }};
        onSDKEventTriggered(baseSessionUIInstance, "updateVisitorVideoState", params);
    }

    public void onSDKEventTriggered(BaseSessionUI baseSessionUIInstance, String method, Map<String, String> params) {
        if (baseSessionUIInstance != null) {
            GlanceSDKEventSenderListener glanceSDKEventSenderListener = baseSessionUIInstance.getGlanceSDKEventSenderListener();

            if (glanceSDKEventSenderListener != null) {
                glanceSDKEventSenderListener.onEventSent(buildSDKEventDataObj(method, params));
            }
        }
    }

    public String buildSDKEventDataObj(String method, Map<String, String> params) {
        StringBuilder result = new StringBuilder("{\n");

        result.append("        type: SDK").append(",\n");
        result.append("        method: ").append(method).append(",\n");
        result.append("        params: {");

        if (params != null) {
            for (Map.Entry<String, String> param : params.entrySet()) {
                result.append("\n            ").append(param.getKey()).append(": ").append(param.getValue()).append(",");
            }
        }

        int currentLength = result.length();
        if (',' == result.charAt(currentLength - 1)) {
            result.delete(currentLength - 1, currentLength);
        }
        result.append("\n        }");
        result.append("\n    }");

        return result.toString();
    }
}
