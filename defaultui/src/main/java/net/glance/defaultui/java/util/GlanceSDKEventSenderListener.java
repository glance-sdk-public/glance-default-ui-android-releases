package net.glance.defaultui.java.util;

public interface GlanceSDKEventSenderListener {
    void onEventSent(String eventData);
}
