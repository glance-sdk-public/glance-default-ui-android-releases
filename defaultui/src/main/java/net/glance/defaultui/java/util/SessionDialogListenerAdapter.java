package net.glance.defaultui.java.util;

import android.app.Activity;

import net.glance.android.SessionDialogListener;

/**
 * Auxiliary class that provides a base implementation for {@link SessionDialogListener}
 *
 * This class can be helpful for those cases where you are interested in override some but not all
 * methods from {@link SessionDialogListener}
 */
public class SessionDialogListenerAdapter implements SessionDialogListener {

    @Override
    public void onAcceptTerms() {

    }

    @Override
    public void onDeclinedTerms() {

    }

    @Override
    public void onEndSession(Activity activity) {

    }
}
