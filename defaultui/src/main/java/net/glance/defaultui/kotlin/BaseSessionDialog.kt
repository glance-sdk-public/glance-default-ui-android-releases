package net.glance.defaultui.kotlin

import android.Manifest
import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageButton
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.os.BundleCompat
import androidx.core.view.ViewCompat
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import net.glance.android.DialogSessionMode
import net.glance.android.Glance
import net.glance.android.SessionDialogListener
import net.glance.android.SessionUI
import net.glance.android.StartParams
import net.glance.android.Util
import net.glance.defaultui.R
import net.glance.defaultui.kotlin.blurdialogfragment.BlurDialogFragment
import net.glance.defaultui.kotlin.util.CommonUtils
import net.glance.glancevideo.AutoFitTextureView
import net.glance.glancevideo.CameraManager


abstract class BaseSessionDialog : BlurDialogFragment(), SessionDialogModeListener,
    VisitorVideoControlListener {
    private var requestPermissionLauncher: ActivityResultLauncher<Array<String>>? = null

    private var state: SessionState? = null

    protected var previousMode: DialogSessionMode? = null

    protected var currentMode: DialogSessionMode? = null

    protected var dialogListener: SessionDialogListener? = null

    protected var handler: Handler? = null

    // Views
    protected var rootView: View? = null

    protected var clDialogSessionContainer: ConstraintLayout? = null

    protected var ibCloseDialog: ImageButton? = null

    // Video views
    protected var clVisitorVideoContainer: CardView? = null

    private var flVisitorVideoContainer: FrameLayout? = null

    protected var aftvVisitorVideo: AutoFitTextureView? = null

    private var clVisitorVideoOff: ConstraintLayout? = null

    protected var clVisitorVideoControlButtonsContainer: ConstraintLayout? = null

    protected var ibVideoState: ImageButton? = null

    // Waiting animation views
    protected var clWaitingSessionAnimContainer: ConstraintLayout? = null

    protected var wvWaitingSessionAnim: WebView? = null

    // Reusable labels
    protected var gtvHeader: GlanceTextView? = null

    protected var gtvDescription: GlanceTextView? = null

    // Session code views
    protected var clSessionCodeContainer: ConstraintLayout? = null

    protected var gtvSessionCode: GlanceTextView? = null

    // Terms and conditions views
    protected var gtvTermsAndConditions: GlanceTextView? = null

    protected var clTermsAndConditionsContainer: ConstraintLayout? = null

    protected var wvTermsAndConditions: WebView? = null

    // Accept and Decline buttons views
    protected var clAcceptDeclineBtsContainer: ConstraintLayout? = null

    protected var btAcceptTerms: Button? = null

    protected var btDeclineTerms: Button? = null

    // Brand label view
    protected var gtvPoweredByGlance: GlanceTextView? = null

    protected var cameraManager: CameraManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestPermissionLauncher = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { result: Map<String, Boolean> ->
            val isGranted = result[Manifest.permission.CAMERA]

            isGranted?.let {
                val sessionUI = defaultSessionUI
                sessionUI?.onPermissionResult(Manifest.permission.CAMERA)
                    ?: Log.i(
                        "Glance", String.format(
                            "Permission result received for %s , but there's " +
                                    "no SessionUI implementation available",
                            Manifest.permission.CAMERA
                        )
                    )
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        state = SessionState.loadFromPreferences(activity)

        changeDialogTop(DEFAULT_DIALOG_TOP)

        rootView = inflater.inflate(R.layout.dialog_session_kotlin, container, false)
        rootView?.visibility = View.INVISIBLE

        loadViews()
        cameraManager = CameraManager(activity, rootView, null, R.id.aftvVisitorVideo, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle = savedInstanceState ?: arguments
        if (bundle != null) {
            setMode(BundleCompat.getSerializable(bundle, DIALOG_MODE, DialogSessionMode::class.java))
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putSerializable(DIALOG_MODE, currentMode)
        super.onSaveInstanceState(outState)
    }

    fun setSessionParams(sessionParams: StartParams) {
        arguments?.let {
            it.putString(DIALOG_SESSION_KEY, sessionParams.keyAsString)
            it.putString(DIALOG_TERMS_URL_KEY, sessionParams.termsUrl)
        }
    }

    fun setSessionDialogListener(listener: SessionDialogListener?) {
        this.dialogListener = listener
    }

    fun requestPermission(permission: String) {
        requestPermissionLauncher?.launch(arrayOf(permission))
    }

    protected val dialogCameraPermissionResultListener: SessionUIPermissionListener
        get() = object : SessionUIPermissionListener {
            override fun onPermissionRequestGranted(permission: String?) {
                changeVisitorVideoViewState()
            }

            override fun onPermissionRequestDenied(permission: String?) {
                handler?.post {
                    onWarningMode(context?.getString(
                        R.string.glance_denied_camera_permission_post_warning,
                            Util.getApplicationName(activity))
                    )

                    val sessionUI: SessionUI? = sessionUI
                    if (!Glance.isPresenceConnected() && sessionUI != null) {
                        Glance.addVisitorVideo(sessionUI.startVideoMode)

                        // make sure that the agent won't be able to invite the visitor's video
                        CommonUtils.updateVisitorVideoState(true, defaultSessionUI)
                    }
                }
            }
        }

    override fun getTheme(): Int {
        return R.style.GlanceDialogStyle
    }

    override val downScaleFactor: Float
        get() = 3.0f // how blurred it should be

    override val blurRadius: Int
        get() = 3

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

        // remove the opaque view used to darken the blurred background
        val myView = activity?.findViewById<View>(R.id.cl_session_dialog_underneath_view)
        val parent = myView?.parent
        if (myView != null && parent is ViewGroup) {
            parent.removeView(myView)
        }
    }

    protected fun setMode(newMode: DialogSessionMode?) {
        val sessionUI = sessionUI ?: return

        if (currentMode == null && Glance.isPresenceConnected()
            && newMode != DialogSessionMode.WAITING_FOR_AGENT
            && newMode != DialogSessionMode.END_SESSION
        ) {
            sendPresenceTermsDisplayed()
        }

        if (this.currentMode == newMode) {
            rootView?.visibility = View.VISIBLE
            return
        }
        this.previousMode = this.currentMode
        this.currentMode = newMode

        val msg = arguments?.getString(DIALOG_MESSAGE_RES_ID_KEY)

        when (newMode) {
            DialogSessionMode.PROMPT -> {
                sessionUI.setTermsAccepted(false)
                onPromptMode(msg ?: "")
            }

            DialogSessionMode.WARNING -> onWarningMode(msg)
            DialogSessionMode.TERMS_CONDITIONS_FULL -> {
                sessionUI.setTermsAccepted(false)
                onTermsAndConditionsMode()
            }

            DialogSessionMode.SESSION_CODE -> {
                val sessionKey = arguments?.getString(DIALOG_SESSION_KEY)
                if (sessionKey?.isNotBlank() == true) {
                    onSessionKeyMode(sessionKey)
                } else {
                    throw RuntimeException("SessionKey is null or empty. Did you call setSessionParams(StartParams)?")
                }
            }

            DialogSessionMode.TWO_WAY_VIDEO_AND_APP_SHARING -> {
                sessionUI.setTermsAccepted(false)
                onVisitorVideoWithControlsMode()
            }

            DialogSessionMode.TWO_WAY_VIDEO_AND_APP_SHARING_CODE -> {
                sessionUI.setTermsAccepted(false)
                onVisitorVideoWithControlsAndCodeMode()
            }

            DialogSessionMode.WAITING_FOR_AGENT -> onWaitingSessionMode()
            DialogSessionMode.END_SESSION -> onEndSessionMode()
            else -> {}
        }
        rootView?.visibility = View.VISIBLE
    }

    /**
     * Changes dialog top position
     *
     * @param newTopInPerc the value in percentage (0.0 - 1.0) that represents the desired top
     * increment (for decrement, just pass a negative value)
     */
    protected fun changeDialogTop(newTopInPerc: Float) {
        val dialog = dialog
        dialog?.let {
            val window = dialog.window
            window?.let {
                window.setGravity(Gravity.TOP)

                val dialogParams = window.attributes
                dialogParams.y = (Util.getScreenHeight() * newTopInPerc).toInt()
                window.attributes = dialogParams
            }
        }
    }

    /**
     * Changes dialog height
     *
     * @param newHeightInPerc the value in percentage (0.0 - 1.0) that represents the desired screen
     * size height portion to be used as the dialog height
     */
    @Suppress("SameParameterValue")
    protected fun changeDialogHeight(newHeightInPerc: Float) {
        rootView?.let { view ->
            view.layoutParams?.let { params ->
                params.height = (Util.getScreenHeight() * newHeightInPerc).toInt()
                view.layoutParams = params
            }
        }
    }

    /**
     * Changes dialog width
     *
     * @param newWidthInPerc the value in percentage (0.0 - 1.0) that represents the desired screen
     * size width portion to be used as the dialog width
     */
    protected fun changeDialogWidth(newWidthInPerc: Float) {
        rootView?.let { view ->
            view.layoutParams?.let { params ->
                params.width = (Util.getScreenWidth() * newWidthInPerc).toInt()
                view.layoutParams = params
            }
        }
    }

    private fun loadViews() {
        clVisitorVideoContainer = rootView?.findViewById(R.id.clVisitorVideoContainer)
        flVisitorVideoContainer = rootView?.findViewById(R.id.flVisitorVideoContainer)
        aftvVisitorVideo = clVisitorVideoContainer?.findViewById(R.id.aftvVisitorVideo)
        clVisitorVideoOff = clVisitorVideoContainer?.findViewById(R.id.clVisitorVideoOff)
        configureVisitorVideoView()

        clVisitorVideoControlButtonsContainer =
            rootView?.findViewById(R.id.clVisitorVideoControlButtonsContainer)
        ibVideoState = clVisitorVideoControlButtonsContainer?.findViewById(R.id.ibVideoState)
        configureVisitorVideoControlsState()

        clDialogSessionContainer = rootView?.findViewById(R.id.clDialogSessionContainer)

        ibCloseDialog = rootView?.findViewById<ImageButton>(R.id.ibCloseDialog)?.also {
            ViewCompat.replaceAccessibilityAction(
                it, AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLICK,
                getString(R.string.glance_accessibility_close_button_action_description), null
            )
        }

        clWaitingSessionAnimContainer = rootView?.findViewById(R.id.clWaitingSessionAnimContainer)
        wvWaitingSessionAnim =
            clWaitingSessionAnimContainer?.findViewById(R.id.wvWaitingSessionAnim)
        configureWaitingSessionView()

        gtvHeader = rootView?.findViewById(R.id.tvGlanceAppSharingDialogTitle)
        gtvDescription = rootView?.findViewById(R.id.tvAppSharingDialogDescription)

        clSessionCodeContainer = rootView?.findViewById(R.id.clSessionCodeContainer)
        gtvSessionCode = clSessionCodeContainer?.findViewById(R.id.gtvSessionCode)

        gtvTermsAndConditions = rootView?.findViewById(R.id.tvGlanceTermsLink)
        gtvPoweredByGlance = rootView?.findViewById(R.id.tvGlancePoweredBy)
        if (context?.resources?.getBoolean(R.bool.GLANCE_DIALOG_SHOW_GLANCE_TEXT) == false) {
            gtvPoweredByGlance?.visibility = View.INVISIBLE
        }

        clTermsAndConditionsContainer = rootView?.findViewById(R.id.clTermsAndConditionsContainer)
        wvTermsAndConditions =
            clTermsAndConditionsContainer?.findViewById(R.id.wvTermsAndConditions)
        configureTermsView()

        clAcceptDeclineBtsContainer = rootView?.findViewById(R.id.cl_accept_decline_layout)
        btAcceptTerms = clAcceptDeclineBtsContainer?.findViewById(R.id.btAcceptSharingApp)
        btDeclineTerms = clAcceptDeclineBtsContainer?.findViewById(R.id.btDeclineSharingApp)
    }

    protected fun changeVisitorVideoViewState() {
        cameraManager?.initCameraWhenAvailable(state?.videoEnabled == true)

        flVisitorVideoContainer?.visibility = if (state?.videoEnabled == true) View.VISIBLE else View.GONE
        clVisitorVideoOff?.visibility = if (state?.videoEnabled == true) View.GONE else View.VISIBLE

        if (state?.videoEnabled == true && aftvVisitorVideo?.isAvailable == true) {
            cameraManager?.onOpenCamera(clVisitorVideoContainer?.width ?: 0, clVisitorVideoContainer?.height ?: 0)
        } else if (cameraManager?.isCameraOpened == true) { // on the first time, the aftvVisitorVideo might not be available
            cameraManager?.onCloseCamera()
        }

        clVisitorVideoContainer?.contentDescription = clVisitorVideoContainer?.context?.getString(
            if (state?.videoEnabled == true) R.string.glance_accessibility_visitor_video_on else R.string.glance_accessibility_visitor_video_off
        )
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun configureTermsView() {
        val params = wvTermsAndConditions?.layoutParams as ConstraintLayout.LayoutParams
        params.height = (Util.getScreenHeight() * TERMS_DIALOG_MAX_WEBVIEW_HEIGHT).toInt()

        wvTermsAndConditions?.let {
            it.layoutParams = params

            // maybe add some loading view while the website gets completed rendered
            it.webViewClient = MyWebViewClient()
            it.settings.javaScriptEnabled = true
        }
    }

    private fun configureVisitorVideoView() {
        val params = clVisitorVideoContainer?.layoutParams as ConstraintLayout.LayoutParams
        val size = if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            (Util.getScreenWidth() * VISITOR_VIDEO_VIEW_SIZE_PORTRAIT).toInt()
        } else {
            (Util.getScreenHeight() * VISITOR_VIDEO_VIEW_SIZE_LANDSCAPE).toInt()
        }
        params.height = size
        params.width = size
        clVisitorVideoContainer?.layoutParams = params
    }

    private fun configureVisitorVideoControlsState() {
        ibVideoState?.isActivated = state?.videoEnabled == true

        ibVideoState?.setOnClickListener {
            state?.videoEnabled = state?.videoEnabled == false
            SessionState.setVideoEnabled(activity, state?.videoEnabled == true)
            ibVideoState?.isActivated = state?.videoEnabled == true

            if (state?.videoEnabled == false) {
                val sessionUI = defaultSessionUI
                sessionUI?.let {
                    sessionUI.isVisitorVideoAdded = false
                }
            }

            onVideoStateChanged()

            configureIbVideoStateA11y()
            clVisitorVideoContainer?.announceForAccessibility(getString(if (state?.videoEnabled == true) R.string.glance_accessibility_visitor_video_on else R.string.glance_accessibility_visitor_video_off))
        }
        configureIbVideoStateA11y()
    }

    private fun configureIbVideoStateA11y() {
        val videoState = state?.videoEnabled == true
        setAccessibilityLabelAndAction(
            ibVideoState,
            if (videoState) R.string.glance_accessibility_disable_action_description else R.string.glance_accessibility_enable_action_description,
            if (videoState) R.string.glance_accessibility_video_on_button_label else R.string.glance_accessibility_video_off_button_label
        )
    }

    @SuppressLint("UseRequireInsteadOfGet")
    protected fun setAccessibilityLabelAndAction(view: View?, actionResId: Int, labelResId: Int) {
        if (view != null) {
            ViewCompat.replaceAccessibilityAction(
                view,
                AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLICK,
                getString(actionResId),
                null
            )
            view.contentDescription = getString(labelResId)
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    protected fun configureWaitingSessionView() {
        val params = wvWaitingSessionAnim?.layoutParams as ConstraintLayout.LayoutParams
        params.height = (Util.getScreenHeight() * WAITING_SESSION_MAX_WEBVIEW_HEIGHT).toInt()
        wvWaitingSessionAnim?.layoutParams = params

        wvWaitingSessionAnim?.webViewClient =
            MyWebViewClient()
        wvWaitingSessionAnim?.settings?.javaScriptEnabled = true
    }

    protected val defaultSessionUI: DefaultSessionUI?
        /**
         * Cast the current session UI to [DefaultSessionUI] if current session UI is a instance.
         *
         * @return Return a [DefaultSessionUI] object if current session is a implementation of it. Return null otherwise.
         */
        get() {
            val sessionUI = sessionUI
            if (sessionUI is DefaultSessionUI) {
                return sessionUI
            }
            return null
        }

    protected val sessionUI: SessionUI?
        /**
         * Return the current [SessionUI]
         *
         * @return Return the current [SessionUI] or null if there's none
         */
        get() = Glance.getSessionUIInstance()

    /**
     * Anchor two views using ConstraintLayout chains
     *
     * @param parentLayout  the parent layout hosting the views to be anchored
     * @param view1         source view trying to link
     * @param direction1    the ConstraintSet direction of the source view (top, bottom, lef, right)
     * @param view2         target view to be linked to
     * @param direction2    the ConstraintSet direction of the target view (top, bottom, lef, right)
     * @param marginDimenId the dimen resource id containing the margin value to be added between the views in dp
     */
    @Suppress("SameParameterValue")
    protected fun anchorViews(
        parentLayout: ConstraintLayout?, view1: View?, direction1: Int,
        view2: View?, direction2: Int, marginDimenId: Int
    ) {

        if (parentLayout == null || view1 == null || view2 == null) {
            return
        }

        val anchorSet = ConstraintSet()
        anchorSet.clone(parentLayout)

        anchorSet.connect(
            view1.id, direction1, view2.id, direction2,
            resources.getDimensionPixelSize(marginDimenId)
        )

        anchorSet.applyTo(parentLayout)
    }

    protected fun showCloseButton() {
        val topRightCornerPadding =
            context?.resources?.getDimensionPixelSize(R.dimen.glance_dialog_close_button_padding) ?: 0

        clDialogSessionContainer?.let {
            it.setPadding(it.paddingLeft, topRightCornerPadding, topRightCornerPadding, it.paddingBottom)
        } // give back the space on the top-right

        //corner so the close button can have some room
        ibCloseDialog?.visibility = View.VISIBLE
    }

    protected fun setupTermsNConditionsView() {
        val statement = context?.getString(R.string.glance_terms_and_condition_statement) ?: ""
        val label = context?.getString(R.string.glance_terms_and_condition_link_label) ?: ""
        val ss = SpannableString("$statement $label")

        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                setMode(DialogSessionMode.TERMS_CONDITIONS_FULL)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                context?.let {
                    ds.color = ContextCompat.getColor(it, R.color.glance_session_ui_dialog_terms_text_color)
                }
            }
        }

        val totalSize = ss.length
        val labelSize = label.length
        ss.setSpan(
            clickableSpan,
            totalSize - labelSize,
            totalSize,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        gtvTermsAndConditions?.let {
            it.text = ss
            it.movementMethod = LinkMovementMethod.getInstance()
            it.highlightColor = Color.TRANSPARENT
        }
    }

    protected fun sendPresenceTermsDisplayed() {
        Glance.sendPresenceTermsDisplayed()

        if (defaultSessionUI != null) {
            CommonUtils.onSDKEventTriggered(defaultSessionUI as BaseSessionUI?, "sendPresenceTermsDisplayed", mapOf("status" to "displayed"))
        }
    }

    private class MyWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            view.loadUrl(request.url.toString())
            return true
        }
    }

    companion object {
        // SIZES PERCENTAGES
        protected const val DEFAULT_DIALOG_TOP: Float = 0.15f

        @JvmStatic
        val TERMS_DIALOG_TOP: Float = 0.05f

        @JvmStatic
        val TERMS_DIALOG_HEIGHT: Float = 0.85f

        @JvmStatic
        val TERMS_DIALOG_WIDTH: Float = 0.85f

        @JvmStatic
        val SESSION_CODE_DIALOG_WIDTH: Float = 0.80f

        @JvmStatic
        val VISITOR_VIDEO_WITHSESSION_CODE_DIALOG_WIDTH: Float = 0.90f

        @JvmStatic
        val END_SESSION_DIALOG_WIDTH: Float = 0.80f

        protected const val WAITING_SESSION_MAX_WEBVIEW_HEIGHT: Float = 0.05f

        protected const val TERMS_DIALOG_MAX_WEBVIEW_HEIGHT: Float = 0.685f

        protected const val VISITOR_VIDEO_VIEW_SIZE_PORTRAIT: Float = 0.4f

        protected const val VISITOR_VIDEO_VIEW_SIZE_LANDSCAPE: Float = 0.3f

        @JvmStatic
        val DIALOG_MODE: String = "DIALOG_MODE"

        @JvmStatic
        val DIALOG_DISMISS_KEY: String = "DIALOG_DISMISS_KEY"

        @JvmStatic
        val DIALOG_MESSAGE_RES_ID_KEY: String = "DIALOG_MESSAGE_RES_ID_KEY"

        protected const val DIALOG_SESSION_KEY: String = "DIALOG_SESSION_KEY"

        @JvmStatic
        val DIALOG_TERMS_URL_KEY: String = "DIALOG_TERMS_URL_KEY"

        @JvmStatic
        val WAITING_FOR_AGENT_ANIMATION_PATH: String =
            "file:///android_asset/waiting_session.html"
    }
}
