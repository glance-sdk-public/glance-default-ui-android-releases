package net.glance.defaultui.kotlin

import android.content.Context
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.accessibility.AccessibilityManager
import android.widget.ImageButton
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import net.glance.android.Glance
import net.glance.android.SessionUI
import net.glance.android.StartParams
import net.glance.android.WidgetCorner
import net.glance.android.WidgetVisibilityMode
import net.glance.defaultui.kotlin.util.CommonUtils
import net.glance.defaultui.kotlin.util.GlanceSDKEventSenderListener
import net.glance.glancevideo.CameraManager
import net.glance.glancevideo.videosession.VideoSession


/***
 *
 *
 * Class that holds all common variables, constants and methods definitions to be used through all
 * the new Magnesium UI implementation.
 *
 */
abstract class BaseSessionUI : SessionUI(), SessionUIWidgetCommonControlListener,
    VisitorVideoControlListener, SessionUIWidgetModeListener, VideoSession.VideoSessionListener {

    protected var context: Context? = null

    protected var handler: Handler? = null

    protected var permissionListener: SessionUIPermissionListener? = null

    private var glanceSDKEventSenderListener: GlanceSDKEventSenderListener? = null

    var state: SessionState? = null

    protected var cameraManager: CameraManager? = null

    // we needed to create an external flag to control whether the camera manager should open the
    // camera right away in order to fix the issue #GD-19506 and its side effetcs. We'll consider
    // this flag default value as true.

    protected var shouldOpenCameraWhenAvailable: Boolean? = null

    protected var widgetRootView: View? = null

    protected var setDragListener: Boolean = true

    protected var currentCorner: WidgetCorner = WidgetCorner.TOP_LEFT //default

    protected var currentMode: WidgetMode? = null

    protected var modeBeforeMinimizing: WidgetMode? = null // to tab

    protected var modeBeforeExpanding: WidgetMode? = null // to the large widget

    protected var fullView: Boolean = false

    protected var showWidgetOnLoadingLayout: Boolean = true

    // Tab widget views
    protected var cvSessionUiTabWidget: CardView? = null

    protected var ivSessionUiTabWidgetIcon: ImageView? = null

    // Visitor video
    protected var videoSession: VideoSession? = null

    protected var videoStartParams: StartParams? = null

    // Miscellaneous
    protected abstract fun setUpSessionViewMoveGestureHandler()

    protected abstract fun generateCornerCoordinates(view: View?): FloatArray?

    protected abstract fun calculateNearestCorner()

    protected abstract fun moveWidgetToNearestCorner()

    protected abstract fun moveWidgetToCorner(corner: WidgetCorner)

    protected abstract fun getVisitorVideoViewIdByMode(targetMode: WidgetMode?): Int

    fun setGlanceSDKEventSenderListener(sdkEventSenderListener: GlanceSDKEventSenderListener?) {
        val sessionUI = Glance.getSessionUIInstance()
        if (sessionUI is BaseSessionUI) {
            glanceSDKEventSenderListener = sdkEventSenderListener
        }
    }

    fun getGlanceSDKEventSenderListener() : GlanceSDKEventSenderListener? {
        return glanceSDKEventSenderListener
    }

    protected fun getString(stringResId: Int): String {
        return mContext?.let { it.resources?.getString(stringResId) } ?: ""
    }

    protected fun getColor(colorResId: Int): Int {
        return mContext?.let { ContextCompat.getColor(it, colorResId) } ?: 0
    }

    protected fun getDimen(dimenResId: Int): Float {
        return mContext?.resources?.getDimension(dimenResId) ?: 0f
    }

    /***
     * Interrupts any other accessibility readout in order to run some we want (otherwise the user
     * would need to hear all of them, which are useless most of times for the given action).
     */
    protected fun interruptA11yReadout() {
        val accessibilityManager =
            mContext?.getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager
        if (accessibilityManager.isEnabled) {
            accessibilityManager.interrupt()
        }
    }

    protected fun setIsFullView(isFullView: Boolean) {
        Log.d(TAG, "new fullView state: $isFullView")
        fullView = isFullView
    }

    fun setOnPermissionResultListener(listener: SessionUIPermissionListener?) {
        permissionListener = listener
    }

    protected fun closeCamera() {
        if (cameraManager?.isCameraOpened == true) {
            cameraManager?.onCloseCamera()
        }
    }

    protected fun runOnMainThread(task: Runnable?) {
        task?.let {
            handler?.post(it)
        }
    }

    @Suppress("SameParameterValue")
    protected fun runOnMainThreadDelayed(task: Runnable?, delay: Long) {
        task?.let {
            handler?.postDelayed(it, delay)
        }
    }

    protected fun hasAgentVideo(targetMode: WidgetMode?): Boolean {
        return (targetMode != null && targetMode != WidgetMode.TAB_MODE
                && targetMode != WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE)
    }

    protected fun isTwoWayVideo(targetMode: WidgetMode?): Boolean {
        return targetMode == WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE
    }


    protected fun isLargeMode(targetMode: WidgetMode?): Boolean {
        return targetMode == WidgetMode.LARGE_VIDEO
    }

    protected fun onLeftCorner(): Boolean {
        return currentCorner == WidgetCorner.TOP_LEFT || currentCorner == WidgetCorner.BOTTOM_LEFT
    }

    protected fun onRightCorner(): Boolean {
        return currentCorner == WidgetCorner.TOP_RIGHT || currentCorner == WidgetCorner.BOTTOM_RIGHT
    }

    /***
     * Changes the color of the icon and disables the click in order to prevent the user to use the
     * feature until the pre conditions are met again.
     */
    protected fun setButtonBlockedState(blocked: Boolean, button: ImageButton?) {
        if (button != null) {
            button.alpha = if (blocked) 0.3f else 1f
            button.isEnabled = !blocked
        }
    }

    /***
     * Sends the new widget corner location so the agent side view window can update it accordingly.
     */
    protected fun sendNewLocation() {
        Glance.updateWidgetLocation(currentCorner)

        CommonUtils.onSDKEventTriggered(
            this, "sendNewLocation",
            mapOf(
                "widgetlocation" to String.format(
                    "message:widgetlocation;location:%s",
                    WidgetCorner.toServerString(currentCorner)
                )
            )
        )
    }

    /***
     * Sends the new widget visibility so the agent side view window can update it accordingly.
     */
    protected fun updateWidgetVisibility(visibilityMode: WidgetVisibilityMode?) {
        Glance.updateWidgetVisibility(visibilityMode)

        CommonUtils.onSDKEventTriggered(
            this, "updateWidgetVisibility", mapOf(
                "widgetvisibility" to String.format(
                    "message:widgetvisibility;visibility:%s",
                    visibilityMode
                )
            )
        )
    }

    protected fun setAccessibilityLabelAndAction(view: View?, actionResId: Int, labelResId: Int) {
        if (view != null) {
            ViewCompat.replaceAccessibilityAction(
                view,
                AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLICK,
                getString(actionResId),
                null
            )
            view.contentDescription = getString(labelResId)
        }
    }

    companion object {
        // Miscellaneous
        const val TAG: String = "SessionUI"
    }
}