package net.glance.defaultui.kotlin

import android.animation.Animator
import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.graphics.PorterDuff
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.ScaleAnimation
import net.glance.android.Glance
import net.glance.android.WidgetCorner
import net.glance.defaultui.R
import net.glance.defaultui.kotlin.util.CommonUtils

/***
 *
 *
 * Class that holds all common animation implementations for the new Magnesium UI widget, including
 * transitions between modes, pulsating tab color, etc.
 *
 */
abstract class BaseSessionUIAnimation : BaseSessionUI() {
    private var tabBackgroundColorAnimator: ValueAnimator? = null

    private var tabIconColorAnimator: ValueAnimator? = null

    // can't use the value from the animator object because it isn't consistent
    private var tabColorAnimStarted: Boolean = false

    protected fun startTabColorAnimations() {
        if (widgetRootView?.id == R.id.session_ui_tab_widget
            && (!tabColorAnimStarted || (tabBackgroundColorAnimator != null
                    && tabIconColorAnimator != null))
        ) {
            tabColorAnimStarted = true

            runOnMainThreadDelayed({
                configTabBgColorAnimation(false)
                configTabIconColorAnimation(false)
            }, TAB_PULSATING_COLOR_ANIMATION_START_DELAY.toLong())
        }
    }

    private fun stopTabColorAnimations() {
        runOnMainThread {
            if (currentMode == WidgetMode.TAB_MODE && tabBackgroundColorAnimator != null && tabIconColorAnimator != null) {
                tabColorAnimStarted = false

                tabBackgroundColorAnimator?.let {
                    it.removeAllListeners()
                    it.cancel()
                    it.setDuration(0)
                }

                cvSessionUiTabWidget?.let {
                    it.animate().setListener(null)
                    it.animate().setUpdateListener(null)
                    it.setCardBackgroundColor(getColor(R.color.glance_session_ui_tab_widget_bg_color))
                }

                tabIconColorAnimator?.let {
                    it.removeAllListeners()
                    it.cancel()
                    it.setDuration(0)
                }

                ivSessionUiTabWidgetIcon?.let {
                    it.animate().setListener(null)
                    it.animate().setUpdateListener(null)
                    it.setColorFilter(getColor(R.color.glance_session_ui_tab_widget_icon_color))
                }
            }
        }
    }

    protected fun configTabBgColorAnimation(inversedColors: Boolean) {
        tabBackgroundColorAnimator = ValueAnimator()

        if (inversedColors) {
            tabBackgroundColorAnimator?.setIntValues(
                getColor(R.color.glance_session_ui_tab_widget_target_pulse_bg_color),
                getColor(R.color.glance_session_ui_tab_widget_bg_color)
            )
        } else {
            tabBackgroundColorAnimator?.setIntValues(
                getColor(R.color.glance_session_ui_tab_widget_bg_color),
                getColor(R.color.glance_session_ui_tab_widget_target_pulse_bg_color)
            )
        }

        tabBackgroundColorAnimator?.let { it ->
            it.setEvaluator(ArgbEvaluator())
            it.addUpdateListener {
                cvSessionUiTabWidget?.setCardBackgroundColor(
                    (it.animatedValue as Int)
                )
            }
            it.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    // no op
                }

                override fun onAnimationEnd(animation: Animator) {
                    runOnMainThread { configTabBgColorAnimation(!inversedColors) }
                }

                override fun onAnimationCancel(animation: Animator) {
                    // no op
                }

                override fun onAnimationRepeat(animation: Animator) {
                    // no op
                }
            })

            it.setDuration(TAB_PULSATING_COLOR_ANIMATION_DURATION.toLong())
            it.start()
        }
    }

    private fun configTabIconColorAnimation(inversedColors: Boolean) {
        tabIconColorAnimator = ValueAnimator()

        tabIconColorAnimator?.let {
            if (inversedColors) {
                it.setIntValues(
                    getColor(android.R.color.white),
                    getColor(R.color.glance_session_ui_tab_widget_icon_color)
                )
            } else {
                it.setIntValues(
                    getColor(R.color.glance_session_ui_tab_widget_icon_color),
                    getColor(android.R.color.white)
                )
            }

            it.setEvaluator(ArgbEvaluator())
            it.addUpdateListener {
                ivSessionUiTabWidgetIcon?.setColorFilter(
                    (it.animatedValue as Int), PorterDuff.Mode.SRC_IN
                )
            }
            it.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {
                    // no op
                }

                override fun onAnimationEnd(animation: Animator) {
                    runOnMainThread { configTabIconColorAnimation(!inversedColors) }
                }

                override fun onAnimationCancel(animation: Animator) {
                    // no op
                }

                override fun onAnimationRepeat(animation: Animator) {
                    // no op
                }
            })

            it.setDuration(TAB_PULSATING_COLOR_ANIMATION_DURATION.toLong())
            it.start()
        }
    }

    protected fun animateWidgetToTabTransition() {
        runOnMainThread {
            widgetRootView?.setOnTouchListener(null)
            setDragListener = false

            showWidgetOnLoadingLayout = false

            val alphaAnimation = AlphaAnimation(1.0f, 0f)
            alphaAnimation.duration = 100
            alphaAnimation.repeatCount = 0
            alphaAnimation.repeatMode = Animation.REVERSE
            alphaAnimation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                }

                override fun onAnimationEnd(animation: Animation) {
                    runOnMainThread {
                        widgetRootView?.visibility = View.INVISIBLE
                        onTabMode()

                        if (isTwoWayVideo(modeBeforeMinimizing)) {
                            videoSession?.stopEncoding()
                            cameraManager?.onCloseCamera()

                            CommonUtils.updateVisitorVideoState(true, this@BaseSessionUIAnimation);

                            shouldOpenCameraWhenAvailable = state?.videoEnabled
                        }

                        moveWidgetToCorner(currentCorner)
                        widgetRootView?.visibility = View.VISIBLE
                        animateTabSize()
                    }
                }

                override fun onAnimationRepeat(animation: Animation) {
                }
            })
            widgetRootView?.startAnimation(alphaAnimation)
        }
    }

    private fun animateTabSize() {
        runOnMainThread {
            val animDuration: Long = 300
            val tabDecreaseSizeAnim: Animation = ScaleAnimation(
                getCurrentModeAnimValue(ANIMATION_INIT_X_SCALE_VALUES),
                1f,  // Start and end values for the X axis scaling
                getCurrentModeAnimValue(ANIMATION_INIT_Y_SCALE_VALUES),
                1f,  // Start and end values for the Y axis scaling
                getCurrentCornerAnimValue(ANIMATION_PIVOT_X_TYPES),
                getCurrentCornerAnimPivotValue(ANIMATION_PIVOT_X_VALUES),  // Pivot point of X scaling
                getCurrentCornerAnimValue(ANIMATION_PIVOT_Y_TYPES),
                getCurrentCornerAnimPivotValue(ANIMATION_PIVOT_Y_VALUES)
            ) // Pivot point of Y scaling
            tabDecreaseSizeAnim.fillAfter = true // Needed to keep the result of the animation
            tabDecreaseSizeAnim.duration = animDuration

            val tabIconAlphaIncreaseAnim = AlphaAnimation(0.5f, 1f)
            tabIconAlphaIncreaseAnim.duration = animDuration

            val s = AnimationSet(false) //  false means don't share interpolators
            s.addAnimation(tabDecreaseSizeAnim)
            s.addAnimation(tabIconAlphaIncreaseAnim)
            s.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                }

                override fun onAnimationEnd(animation: Animation) {
                    setUpSessionViewMoveGestureHandler()
                    startTabColorAnimations()
                }

                override fun onAnimationRepeat(animation: Animation) {
                }
            })
            widgetRootView?.startAnimation(s)
            showWidgetOnLoadingLayout = true
        }
    }

    protected fun animateTabToWidgetTransition(lastMotionXCoordinate: Float) {
        runOnMainThread {
            widgetRootView?.setOnTouchListener(null)
            setDragListener = false

            showWidgetOnLoadingLayout = false

            stopTabColorAnimations()

            val alphaAnimation = AlphaAnimation(1.0f, 0f)
            alphaAnimation.duration = 100
            alphaAnimation.repeatCount = 0
            alphaAnimation.repeatMode = Animation.REVERSE
            alphaAnimation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                }

                override fun onAnimationEnd(animation: Animation) {
                    widgetRootView?.visibility = View.INVISIBLE

                    onClickToExpandWidget()

                    widgetRootView?.post {
                        moveWidgetToCorner(currentCorner)
                        var initialX = lastMotionXCoordinate + (widgetRootView?.width ?: 0)
                        if (currentCorner == WidgetCorner.TOP_LEFT || currentCorner == WidgetCorner.BOTTOM_LEFT) {
                            initialX = lastMotionXCoordinate
                        }

                        if (isTwoWayVideo(currentMode)) {
                            videoSession?.resume()
                        }

                        if (state?.videoEnabled == true) {
                            CommonUtils.updateVisitorVideoState(false, this@BaseSessionUIAnimation);
                        }

                        // make sure that the expanded widget will have the same last UI properties
                        // as the tab, so we can achieve a smooth animation between them
                        widgetRootView?.let {
                            it.alpha = 0f
                            it.scaleX = 0.5f
                            it.scaleY = 0.5f
                            it.x = initialX
                            it.visibility = View.VISIBLE
                        }
                        animateWidgetTranslation()
                    }
                }

                override fun onAnimationRepeat(animation: Animation) {
                }
            })
            widgetRootView?.startAnimation(alphaAnimation)
        }
    }

    private fun animateWidgetTranslation() {
        val coordinates = generateCornerCoordinates(widgetRootView)
        val leftX = coordinates?.get(0) ?: 0f
        val rightX = coordinates?.get(1) ?: 0f
        val topY = coordinates?.get(2) ?: 0f
        val bottomY = coordinates?.get(3) ?: 0f

        var targetX = rightX
        if (currentCorner == WidgetCorner.TOP_LEFT || currentCorner == WidgetCorner.BOTTOM_LEFT) {
            targetX = leftX
        }

        var targetY = topY
        if (currentCorner == WidgetCorner.BOTTOM_LEFT || currentCorner == WidgetCorner.BOTTOM_RIGHT) {
            targetY = bottomY
        }

        widgetRootView
            ?.animate()
            ?.setDuration(300)
            ?.alphaBy(1f)
            ?.scaleX(1f)
            ?.scaleY(1f)
            ?.x(targetX)
            ?.y(targetY)?.withEndAction { this.setUpSessionViewMoveGestureHandler() }

        showWidgetOnLoadingLayout = true
    }

    private fun getCurrentModeAnimValue(animValuesMap: Map<WidgetMode, Float?>?): Float {
        return modeBeforeMinimizing?.let { animValuesMap?.get(it) } ?: 0f
    }

    private fun getCurrentCornerAnimValue(animValuesMap: Map<WidgetCorner, Int>?): Int {
        return animValuesMap?.get(currentCorner) ?: 0
    }

    private fun getCurrentCornerAnimPivotValue(animValuesMap: Map<WidgetCorner, Float>?): Float {
        return animValuesMap?.get(currentCorner) ?: 0f
    }

    companion object {
        private const val TAB_PULSATING_COLOR_ANIMATION_DURATION = 2000

        private const val TAB_PULSATING_COLOR_ANIMATION_START_DELAY = 2000

        private val ANIMATION_PIVOT_Y_TYPES: Map<WidgetCorner, Int> =
            object : HashMap<WidgetCorner, Int>() {
                init {
                    put(WidgetCorner.TOP_LEFT, Animation.RELATIVE_TO_SELF)
                    put(WidgetCorner.TOP_RIGHT, Animation.RELATIVE_TO_SELF)
                    put(WidgetCorner.BOTTOM_LEFT, Animation.RELATIVE_TO_PARENT)
                    put(WidgetCorner.BOTTOM_RIGHT, Animation.RELATIVE_TO_PARENT)
                }
            }

        private val ANIMATION_PIVOT_Y_VALUES: Map<WidgetCorner, Float> =
            object : HashMap<WidgetCorner, Float>() {
                init {
                    put(WidgetCorner.TOP_LEFT, 0f)
                    put(WidgetCorner.TOP_RIGHT, 1f)
                    put(WidgetCorner.BOTTOM_LEFT, 1f)
                    put(WidgetCorner.BOTTOM_RIGHT, 1f)
                }
            }

        private val ANIMATION_INIT_Y_SCALE_VALUES: Map<WidgetMode, Float?> =
            object : HashMap<WidgetMode, Float>() {
                init {
                    put(WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE, 1.25f)
                    put(WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE, 2f)
                    put(WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE, 3f)
                }
            }

        private val ANIMATION_INIT_X_SCALE_VALUES: Map<WidgetMode, Float?> =
            object : HashMap<WidgetMode, Float>() {
                init {
                    put(WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE, 2f)
                    put(WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE, 3f)
                    put(WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE, 3f)
                }
            }

        private val ANIMATION_PIVOT_X_TYPES: Map<WidgetCorner, Int> =
            object : HashMap<WidgetCorner, Int>() {
                init {
                    put(WidgetCorner.TOP_LEFT, Animation.RELATIVE_TO_SELF)
                    put(WidgetCorner.TOP_RIGHT, Animation.RELATIVE_TO_PARENT)
                    put(WidgetCorner.BOTTOM_LEFT, Animation.RELATIVE_TO_PARENT)
                    put(WidgetCorner.BOTTOM_RIGHT, Animation.RELATIVE_TO_PARENT)
                }
            }

        private val ANIMATION_PIVOT_X_VALUES: Map<WidgetCorner, Float> =
            object : HashMap<WidgetCorner, Float>() {
                init {
                    put(WidgetCorner.TOP_LEFT, 0f)
                    put(WidgetCorner.TOP_RIGHT, 1f)
                    put(WidgetCorner.BOTTOM_LEFT, 0f)
                    put(WidgetCorner.BOTTOM_RIGHT, 1f)
                }
            }
    }
}