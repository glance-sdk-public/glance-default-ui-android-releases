package net.glance.defaultui.kotlin

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.util.Log
import android.view.MotionEvent
import android.view.RoundedCorner
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.view.ViewParent
import android.view.accessibility.AccessibilityEvent
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.RelativeLayout
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import androidx.core.view.isVisible
import net.glance.android.DialogSessionMode
import net.glance.android.Glance
import net.glance.android.GlanceAndroidPreferences
import net.glance.android.SessionDialogListener
import net.glance.android.SessionUICompletionListener
import net.glance.android.Util
import net.glance.android.VisitorVideoSizeMode
import net.glance.android.WidgetCorner
import net.glance.defaultui.R
import net.glance.defaultui.kotlin.util.CommonUtils
import net.glance.defaultui.kotlin.util.SessionDialogListenerAdapter
import net.glance.glancevideo.AutoFitTextureView
import kotlin.math.abs


/***
 * Class that holds all common views and behaviors for the new Magnesium UI widget scenarios,
 * including buttons, videos and modes.
 *
 *
 * Note that these commom views holds the **same IDs** (except by surface/texture views) regardless
 * the XML layout file where they are being used so we can improve reusing.
 */
abstract class BaseSessionUIView : BaseSessionUIAnimation() {
    protected var sessionDialog: SessionDialog? = null

    protected var isActivityChangingConfigurations: Boolean = false

    private var viewsLoaded: Boolean = false

    // Border view
    protected var sessionUIBorder: FrameLayout? = null

    // App Share widget views
    private var rlSessionUiAppShareAnchorBox: RelativeLayout? = null

    // Main session UI widget buttons views
    private var clSessionUiVideoBtContainer: ConstraintLayout? = null

    protected var ibEndSession: ImageButton? = null

    // Agent Video views
    protected var cvAgentVideoContainer: CardView? = null

    private var clAgentVideoOffContainer: CardView? = null

    private var cvVisitorBackVideoContainer: CardView? = null

    protected var ibExpandContractVideo: ImageButton? = null

    // Visitor Video views
    protected var clVisitorVideoContainer: CardView? = null

    private var flVisitorVideoContainer: FrameLayout? = null

    protected var aftvVisitorVideo: AutoFitTextureView? = null

    private var clVisitorVideoOff: ConstraintLayout? = null

    // Large Video views
    private var wasVisitorVideoOffBeforeFlip = false

    protected var clLargeVideoWidgetBackground: ConstraintLayout? = null

    private var rlAgentLargeVideoContainer: RelativeLayout? = null

    private var rlVisitorVideoContainer: RelativeLayout? = null

    private var aftvVisitorBackCamera: AutoFitTextureView? = null

    private var rlLargeVideoBtContainer: RelativeLayout? = null

    private var ibVisitorCameraMode: ImageButton? = null

    // App Video state
    protected var ibVideoState: ImageButton? = null

    /***
     * Mandatory methods
     */
    protected abstract fun showWidgetFromStartVideoMode()

    @Suppress("SameParameterValue")
    protected abstract fun showSessionDialog(
        listener: SessionDialogListener?, mode: DialogSessionMode?,
        warningMessage: String?, dismiss: Boolean
    )

    protected var completionListener: SessionUICompletionListener? = null

    private var widgetDragListener: OnTouchListener? = null

    protected fun initWidgetDragListener() {
        widgetDragListener = object : OnTouchListener {
            var dX: Float = 0f
            var dY: Float = 0f
            var startX: Float = 0f
            var startY: Float = 0f
            var isMove: Boolean = false

            @SuppressLint("ClickableViewAccessibility")
            override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
                val screenWidth = Util.getScreenWidth().toFloat()

                when (motionEvent.action) {
                    MotionEvent.ACTION_DOWN -> {
                        LAST_TOUCH_IN_MILLIS = System.currentTimeMillis()

                        dX = view.x - motionEvent.rawX
                        dY = view.y - motionEvent.rawY

                        startX = motionEvent.x
                        startY = motionEvent.y

                        isMove = false
                    }

                    MotionEvent.ACTION_MOVE -> {
                        if (currentMode != WidgetMode.TAB_MODE) {
                            view.x = motionEvent.rawX + dX
                        }

                        view.y = motionEvent.rawY + dY

                        val leftViewSide = view.x
                        val rightViewSide = leftViewSide + view.width

                        if (currentMode == WidgetMode.TAB_MODE && startX >= leftViewSide && startX <= rightViewSide && abs(
                                dX.toDouble()
                            ) >= view.width
                        ) {
                            animateTabToWidgetTransition(leftViewSide)
                        }

                        if (currentMode == WidgetMode.TAB_MODE
                            && ((onLeftCorner() && leftViewSide >= 0)
                                    || (onRightCorner() && rightViewSide < screenWidth - MOVE_GESTURE_EDGE_GAP_LIMIT))
                        ) {
                            isMove = false
                        }

                        if (currentMode != WidgetMode.TAB_MODE
                            && (leftViewSide <= MOVE_GESTURE_EDGE_GAP_LIMIT
                                    || rightViewSide >= screenWidth - MOVE_GESTURE_EDGE_GAP_LIMIT)
                        ) {
                            LAST_TOUCH_IN_MILLIS = 0

                            calculateNearestCorner()

                            animateWidgetToTabTransition()
                        }
                    }

                    MotionEvent.ACTION_UP -> {
                        val currentTime = System.currentTimeMillis()
                        if (currentTime - LAST_TOUCH_IN_MILLIS >= MIN_LONG_PRESS_DELAY) {
                            LAST_TOUCH_IN_MILLIS = 0

                            if (currentMode != WidgetMode.TAB_MODE || isMove) {
                                moveWidgetToNearestCorner()
                            }
                        }

                        if (currentMode == WidgetMode.TAB_MODE && !isMove) {
                            animateTabToWidgetTransition(view.x)
                        }
                    }

                    else -> return false
                }
                return true
            }
        }
    }

    protected fun loadBorderView(rootView: ViewGroup) {
        sessionUIBorder = rootView.findViewById(R.id.session_ui_frame_border)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            val insets = rootView.rootWindowInsets

            insets?.let {
                val topLeftCorner = insets.getRoundedCorner(RoundedCorner.POSITION_TOP_LEFT)
                val topRightCorner = insets.getRoundedCorner(RoundedCorner.POSITION_TOP_RIGHT)
                val bottomLeftCorner = insets.getRoundedCorner(RoundedCorner.POSITION_BOTTOM_LEFT)
                val bottomRightCorner = insets.getRoundedCorner(RoundedCorner.POSITION_BOTTOM_RIGHT)

                val topLeft = topLeftCorner?.radius ?: 0
                val topRight = topRightCorner?.radius ?: 0
                val bottomLeft = bottomLeftCorner?.radius ?: 0
                val bottomRight = bottomRightCorner?.radius ?: 0

                val shape = GradientDrawable()
                shape.shape = GradientDrawable.RECTANGLE
                shape.setStroke(
                    getDimen(R.dimen.glance_border_width).toInt(),
                    getColor(net.glance.android.R.color.glance_border_color)
                )
                // The corners are ordered top-left, top-right, bottom-right, bottom-left.
                shape.cornerRadii = floatArrayOf(
                    topLeft.toFloat(),
                    topLeft.toFloat(),
                    topRight.toFloat(),
                    topRight.toFloat(),
                    bottomLeft.toFloat(),
                    bottomLeft.toFloat(),
                    bottomRight.toFloat(),
                    bottomRight.toFloat()
                )

                sessionUIBorder?.background = shape
            }
        }
    }

    protected fun loadTabWidget(rootView: View?) {
        this.widgetRootView = rootView

        cvSessionUiTabWidget = rootView?.findViewById(R.id.cvSessionUiTabWidget)
        ivSessionUiTabWidgetIcon = rootView?.findViewById(R.id.ivSessionUiTabWidgetIcon)

        startTabColorAnimations()
    }

    /***
     * It loads/reloads the entire app share widget view, instantiating all subviews and setting
     * gestures and listeners.
     *
     * @param rootView The widget root view.
     */
    protected fun loadAppShareView(rootView: View?) {
        this.widgetRootView = rootView

        rlSessionUiAppShareAnchorBox = rootView?.findViewById(R.id.rlSessionUiAppShareAnchorBox)

        ibEndSession = rootView?.findViewById(R.id.ibEndSession)

        configureCommonButtonsListeners()

        if (setDragListener) { // only for the first load right after the agent joins
            setUpSessionViewMoveGestureHandler()
        }
    }

    /***
     * It loads/reloads the entire large video widget view, instantiating all subviews and setting
     * the listeners.
     *
     * @param rootView The widget root view.
     */
    protected fun loadLargeVideoWidget(rootView: View?) {
        this.widgetRootView = rootView

        // Agent views
        loadAgentVideoViews(WidgetMode.LARGE_VIDEO)

        // set correct height for agent video
        val agentVideoOffParams =
            clAgentVideoOffContainer?.layoutParams as RelativeLayout.LayoutParams
        agentVideoOffParams.height =
            (Util.getScreenHeight() * LARGE_AGENT_VIDEO_SCREEN_SIZE).toInt()
        clAgentVideoOffContainer?.layoutParams = agentVideoOffParams

        val agentVideoparams = cvAgentVideoContainer?.layoutParams as RelativeLayout.LayoutParams
        agentVideoparams.height = (Util.getScreenHeight() * LARGE_AGENT_VIDEO_SCREEN_SIZE).toInt()
        cvAgentVideoContainer?.layoutParams = agentVideoparams

        // Visitor views
        rlVisitorVideoContainer = rootView?.findViewById(R.id.rlVisitorVideoContainer)
        aftvVisitorBackCamera = rootView?.findViewById(R.id.aftvVisitorBackCamera)
        rlLargeVideoBtContainer = rootView?.findViewById(R.id.rlLargeVideoBtContainer)

        // set correct height for visitor video
        loadVisitorVideoViews()

        val visitorVideoSize =
            (Util.getScreenWidth() * LARGE_WIDGET_VISITOR_VIDEO_SCREEN_SIZE).toInt()
        val visitorVideoContainerParams =
            rlVisitorVideoContainer?.layoutParams as RelativeLayout.LayoutParams
        visitorVideoContainerParams.height = visitorVideoSize
        visitorVideoContainerParams.width = visitorVideoSize
        rlVisitorVideoContainer?.setLayoutParams(visitorVideoContainerParams)
        // need to apply the height value for both container and cardview holding the video view itself
        val visitorVideoParams =
            clVisitorVideoContainer?.layoutParams as RelativeLayout.LayoutParams
        visitorVideoParams.height = visitorVideoSize
        visitorVideoParams.width = visitorVideoSize
        clVisitorVideoContainer?.layoutParams = visitorVideoParams

        // end
        val visitorBackVideoPrams =
            cvVisitorBackVideoContainer?.layoutParams as RelativeLayout.LayoutParams
        visitorBackVideoPrams.height =
            (Util.getScreenHeight() * LARGE_AGENT_VIDEO_SCREEN_SIZE).toInt()
        cvVisitorBackVideoContainer?.layoutParams = visitorBackVideoPrams

        cameraManager?.initCameraWhenAvailable(true)
        changeVisitorVideoViewState(WidgetMode.LARGE_VIDEO)

        ibExpandContractVideo = rootView?.findViewById(R.id.ibExpandContractVideo)
        ibExpandContractVideo?.setImageResource(R.drawable.glance_ic_contract_session_video)
        loadIbExpandContractVideoStateView()

        ibVideoState = rlLargeVideoBtContainer?.findViewById(R.id.ibVideoState)
        ibVideoState?.let {
            it.post {
                // center the visitor video view with respect to the @ibVideoState
                val ibVideoStateCurrentPosXY = IntArray(2)
                it.getLocationOnScreen(ibVideoStateCurrentPosXY) // 0 - left; 1 - top;
                val ibVideoStateCurrentX = ibVideoStateCurrentPosXY[0]

                val rlVisitorVideoContainerCurrentPosXY = IntArray(2)
                rlVisitorVideoContainer?.getLocationOnScreen(rlVisitorVideoContainerCurrentPosXY) // 0 - left; 1 - top;
                val rlVisitorVideoContainerCurrentX = rlVisitorVideoContainerCurrentPosXY[0]

                val videoBtMiddle = ibVideoStateCurrentX + (it.width / HALF)
                val visitorVideoNewX =
                    videoBtMiddle - ((rlVisitorVideoContainer?.width ?: 0) / HALF)

                val visitorVideoContainerParameters =
                    rlVisitorVideoContainer?.layoutParams as RelativeLayout.LayoutParams
                visitorVideoContainerParameters.bottomMargin =
                    -1 * ((rlLargeVideoBtContainer?.height
                        ?: 0) / 2) // if we have any differences to be fixed in other devices, consider using the space amount between the video state button and its parent top
                visitorVideoContainerParameters.leftMargin =
                    visitorVideoNewX - rlVisitorVideoContainerCurrentX
                rlVisitorVideoContainer?.setLayoutParams(visitorVideoContainerParameters)

                // decrease the visitor video off icon size to be proportional with respect to its container view
                val ibVisitorVideoVideoOffIcon =
                    rlVisitorVideoContainer?.findViewById<ImageButton>(R.id.ibVisitorVideoVideoOffIcon)
                val videoIconParams =
                    ibVisitorVideoVideoOffIcon?.layoutParams as ConstraintLayout.LayoutParams
                val iconSize =
                    getDimen(R.dimen.glance_session_ui_large_widget_icon_visitor_video_off_size).toInt()
                videoIconParams.height = iconSize
                videoIconParams.width = iconSize
                videoIconParams.bottomMargin =
                    getDimen(R.dimen.glance_session_ui_large_widget_icon_visitor_video_off_margin_bottom).toInt()
                ibVisitorVideoVideoOffIcon.layoutParams = videoIconParams
            }
        }

        loadVideoStateView()

        ibVisitorCameraMode = rootView?.findViewById(R.id.ibVisitorCameraMode)
        ibVisitorCameraMode?.let { it ->
            it.setOnClickListener {
                it.isActivated = !it.isActivated
                if (cameraManager?.isFrontCameraSelected == false) { // currently back camera
                    state?.videoEnabled = wasVisitorVideoOffBeforeFlip
                    ibVideoState?.isActivated = state?.videoEnabled == true

                    cvVisitorBackVideoContainer?.visibility = View.INVISIBLE
                    rlVisitorVideoContainer?.visibility = View.VISIBLE

                    ibExpandContractVideo?.let {
                        it.visibility =
                            if (Glance.isAgentVideoEnabled()) View.VISIBLE else View.INVISIBLE
                    }

                    CommonUtils.updateVisitorVideoState(state?.videoEnabled == false, this);
                } else { // currently front camera
                    wasVisitorVideoOffBeforeFlip = state?.videoEnabled == true

                    ibVideoState?.isActivated = true

                    cvVisitorBackVideoContainer?.visibility = View.VISIBLE
                    rlVisitorVideoContainer?.visibility = View.GONE
                    ibExpandContractVideo?.setVisibility(View.INVISIBLE)

                    CommonUtils.updateVisitorVideoState(false, this);
                }

                cameraManager?.flipCamera(if (cameraManager?.isFrontCameraSelected == true) R.id.aftvVisitorBackCamera else R.id.aftvVisitorVideo)

                configureIbVisitorCameraModeA11y()

                cvAgentVideoContainer?.importantForAccessibility =
                    if (it.isActivated) View.IMPORTANT_FOR_ACCESSIBILITY_YES else View.IMPORTANT_FOR_ACCESSIBILITY_NO
                rootView?.announceForAccessibility(
                    context?.getString(
                        if (cameraManager?.isFrontCameraSelected == true) {
                            R.string.glance_accessibility_visitor_front_camera_selected
                        } else {
                            R.string.glance_accessibility_visitor_back_camera_selected
                        }
                    )
                )
                sendNewVideosSize(currentMode)
            }

            it.isActivated = true // since we always start showing the front camera
            setButtonBlockedState(state?.videoEnabled == false, it)
        }

        configureIbVisitorCameraModeA11y()

        ibEndSession = rlLargeVideoBtContainer?.findViewById(R.id.ibEndSession)

        ibEndSession?.let {
            it.setOnClickListener { onClickEndSession() }
            ViewCompat.replaceAccessibilityAction(
                it, AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLICK,
                getString(R.string.glance_accessibility_end_session_action_description), null
            )
        }

        if (!isTwoWayVideo(currentMode)) {
            setIsFullView(true)
        }

        if (state?.videoEnabled == false) {
            CommonUtils.updateVisitorVideoState(
                true,
                this
            )
            // for more context, see GD-19746
        }
    }

    private fun configureIbVisitorCameraModeA11y() {
        setAccessibilityLabelAndAction(
            ibVisitorCameraMode,
            if (ibVisitorCameraMode?.isActivated == true) R.string.glance_accessibility_flip_to_back_camera_action_description else R.string.glance_accessibility_flip_to_front_camera_action_description,
            R.string.glance_accessibility_flip_camera_button_label
        )
    }

    /***
     * It loads/reloads the entire video (agent/two-way) widget view, instantiating all subviews and
     * setting gestures and listeners.
     *
     * @param rootView The widget root view.
     * @param targetMode The widget mode we want to target
     */
    protected fun loadViews(rootView: View?, targetMode: WidgetMode) {
        this.widgetRootView = rootView

        clSessionUiVideoBtContainer = rootView?.findViewById(R.id.clSessionUiVideoBtContainer)

        ibEndSession = rootView?.findViewById(R.id.ibEndSession)

        loadAgentVideoViews(targetMode)

        loadVisitorVideoViews()

        clVisitorVideoContainer?.let {// can be null when the visitor didn't join yet or is app share
            changeVisitorVideoViewState(currentMode)
        }

        ibVideoState = rootView?.findViewById(R.id.ibVideoState)

        if (setDragListener) { // only for the first load right after the agent joins
            setUpSessionViewMoveGestureHandler()
        }
        configureCommonButtonsListeners()
        configureVisitorVideoControlsState()

        if (isTwoWayVideo(targetMode)) {
            ibExpandContractVideo = rootView?.findViewById(R.id.ibExpandContractVideo)
            loadIbExpandContractVideoStateView()

            if (state?.videoEnabled == false) {
                CommonUtils.updateVisitorVideoState(true, this); // for more context, see GD-19746
            }
        }

        viewsLoaded = true
        completionListener?.onWidgetViewsLoaded()

        rootView?.post { rootView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED) }
    }

    private fun loadAgentVideoViews(targetMode: WidgetMode) {
        cvAgentVideoContainer = widgetRootView?.findViewById(R.id.cvAgentVideoContainer)
        cvAgentVideoContainer?.setContentDescription(getString(R.string.glance_accessibility_agent_video_on))

        clAgentVideoOffContainer = widgetRootView?.findViewById(R.id.clAgentVideoOffContainer)
        clAgentVideoOffContainer?.setContentDescription(getString(R.string.glance_accessibility_agent_video_off))

        if (targetMode == WidgetMode.LARGE_VIDEO) {
            rlAgentLargeVideoContainer =
                widgetRootView?.findViewById(R.id.rlAgentLargeVideoContainer)
            clAgentVideoOffContainer =
                rlAgentLargeVideoContainer?.findViewById(R.id.clAgentVideoOffContainer)
        }

        cvVisitorBackVideoContainer =
            widgetRootView?.findViewById(R.id.cvVisitorBackVideoContainer)

        changeAgentVideoState(targetMode)
    }

    override fun changeAgentVideoState(isAgentVideoEnabled: Boolean) {
        // we are getting crashes after ending an session, this log is just to try to catch its source
        Log.d(TAG, "changeAgentVideoState: $isAgentVideoEnabled")
        runOnMainThread { changeAgentVideoState(currentMode, isAgentVideoEnabled) }
    }

    private fun changeAgentVideoState(targetMode: WidgetMode?) {
        val isAgentVideoEnabled = Glance.isAgentVideoEnabled()
        changeAgentVideoState(targetMode, isAgentVideoEnabled)
    }

    private fun changeAgentVideoState(targetMode: WidgetMode?, isAgentVideoEnabled: Boolean) {
        runOnMainThread {
            if (widgetRootView != null && clAgentVideoOffContainer != null && hasAgentVideo(
                    targetMode
                )
            ) {
                clAgentVideoOffContainer?.visibility =
                    if (isAgentVideoEnabled) View.INVISIBLE else View.VISIBLE

                cvAgentVideoContainer?.visibility =
                    if (isAgentVideoEnabled) View.VISIBLE else View.INVISIBLE

                ibExpandContractVideo?.let {
                    ibExpandContractVideo?.visibility =
                        if (isAgentVideoEnabled) View.VISIBLE else View.INVISIBLE
                }

                widgetRootView?.announceForAccessibility(getString(if (isAgentVideoEnabled) R.string.glance_accessibility_agent_video_on else R.string.glance_accessibility_agent_video_off))
            }
        }
    }

    private fun loadVisitorVideoViews() {
        clVisitorVideoContainer = widgetRootView?.findViewById(R.id.clVisitorVideoContainer)
        clVisitorVideoContainer?.let {// can be null when the visitor didn't join yet or is app share
            flVisitorVideoContainer = widgetRootView?.findViewById(R.id.flVisitorVideoContainer)
            aftvVisitorVideo = it.findViewById(R.id.aftvVisitorVideo)
            clVisitorVideoOff = it.findViewById(R.id.clVisitorVideoOff)
        }
    }

    override fun getVisitorVideoViewIdByMode(targetMode: WidgetMode?): Int {
        return when {
            isTwoWayVideo(targetMode) -> R.id.aftvVisitorVideo

            targetMode == WidgetMode.LARGE_VIDEO -> {
                if (cameraManager?.isFrontCameraSelected == true) {
                    R.id.aftvVisitorVideo
                } else {
                    R.id.aftvVisitorBackCamera
                }
            }

            else -> -1
        }
    }

    override fun setUpSessionViewMoveGestureHandler() {
        runOnMainThread {
            widgetRootView?.setOnTouchListener(widgetDragListener)
        }
    }

    private fun configureCommonButtonsListeners() {
        ibEndSession?.let {
            it.setOnClickListener { onClickEndSession() }
            ViewCompat.replaceAccessibilityAction(
                it, AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLICK,
                getString(R.string.glance_accessibility_end_session_action_description), null
            )
        }
    }

    private fun configureVisitorVideoControlsState() {
        loadVideoStateView()
    }

    private fun loadVideoStateView() {
        ibVideoState?.let {// only when the visitor joins
            wasVisitorVideoOffBeforeFlip = state?.videoEnabled == true

            ibVideoState?.isActivated = state?.videoEnabled == true

            ibVideoState?.setOnClickListener {
                state?.videoEnabled = state?.videoEnabled == false
                it.isActivated = state?.videoEnabled == true

                ibVisitorCameraMode?.let {
                    setButtonBlockedState(state?.videoEnabled == false, ibVisitorCameraMode)
                }

                SessionState.setVideoEnabled(mContext, state?.videoEnabled == true)
                CommonUtils.updateVisitorVideoState(state?.videoEnabled == true, this);

                onVideoStateChanged()

                configureIbVideoStateA11y()
                clVisitorVideoContainer?.announceForAccessibility(getString(if (state?.videoEnabled == true) R.string.glance_accessibility_visitor_video_on else R.string.glance_accessibility_visitor_video_off))
            }
            configureIbVideoStateA11y()

            // Do not allow the users to try to change their video state if the camera permission wasn't given.
            // If it was never ever requested before, do it.
            val permissionCheck = mContext?.let {
                ContextCompat.checkSelfPermission(it, Manifest.permission.CAMERA)
            } ?: PackageManager.PERMISSION_DENIED
            if (GlanceAndroidPreferences.hasCameraPermissionBeenAsked(mContext) && permissionCheck != PackageManager.PERMISSION_GRANTED) {
                setVisitorVideoUnavailable()
            } else if (!Glance.isPresenceConnected() && !GlanceAndroidPreferences.hasCameraPermissionBeenAsked(
                    mContext
                )
            ) {
                setOnPermissionResultListener(object : SessionUIPermissionListener {
                    override fun onPermissionRequestGranted(permission: String?) {
                        cameraManager?.onOpenCamera()
                        Glance.addVisitorVideo(startVideoMode)

                        sessionDialog?.dismiss()
                    }

                    override fun onPermissionRequestDenied(permission: String?) {
                        setVisitorVideoUnavailable()

                        sessionDialog?.dismiss()
                    }
                })

                runOnMainThread {
                    showSessionDialog(
                        object : SessionDialogListenerAdapter() {
                            override fun onAcceptTerms() {
                                sessionDialog?.requestPermission(Manifest.permission.CAMERA)
                                GlanceAndroidPreferences.setCameraPermissionBeenAsked(
                                    mForegroundActivity?.get(),
                                    true
                                )
                            }

                            override fun onDeclinedTerms() {
                                setVisitorVideoUnavailable()
                            }
                        },
                        DialogSessionMode.PROMPT,
                        getString(R.string.glance_first_camera_permission_prompt_warning),
                        false
                    )
                }
                // if declined, reuse the code inside the IF above to hide the video visitor icon
                // if accepted, check if it will show and stream the visitor video. otherwise, make sure it works
            }
        }
    }

    private fun configureIbVideoStateA11y() {
        setAccessibilityLabelAndAction(
            ibVideoState,
            if (state?.videoEnabled == true) R.string.glance_accessibility_disable_action_description else R.string.glance_accessibility_enable_action_description,
            if (state?.videoEnabled == true) R.string.glance_accessibility_video_on_button_label else R.string.glance_accessibility_video_off_button_label
        )
    }

    private fun setVisitorVideoUnavailable() {
        ibVideoState?.let {
            it.alpha = 0f
            it.isEnabled = false
        }

        state?.videoEnabled = false
        switchVisitorVideoViews()
        CommonUtils.updateVisitorVideoState(true, this@BaseSessionUIView);
    }

    private fun loadIbExpandContractVideoStateView() {
        ibExpandContractVideo?.setOnClickListener {
            cameraManager?.onCloseCamera() // force camera to run a fresh setup before opening again
            if (fullView) {
                onClickToContractWidget()
            } else {
                onLargeVideoMode()
            }
            setIsFullView(!fullView)
            configureIbExpandContractVideoA11y()
        }

        configureIbExpandContractVideoA11y()
    }

    private fun configureIbExpandContractVideoA11y() {
        setAccessibilityLabelAndAction(
            ibExpandContractVideo,
            if (fullView) R.string.glance_accessibility_contract_action_description else R.string.glance_accessibility_expand_action_description,
            if (fullView) R.string.glance_accessibility_contract_button_label else R.string.glance_accessibility_expand_button_label
        )
    }

    override fun moveWidgetToNearestCorner() {
        calculateNearestCorner()

        moveWidgetToCorner(currentCorner)
    }

    override fun calculateNearestCorner() {
        val l = IntArray(2) // 0 - left; 1 - top;
        widgetRootView?.getLocationInWindow(l)
        val leftX = l[0]
        val topY = l[1]

        val screenWidth = Util.getScreenWidth()
        val screenHeight = Util.getScreenHeight()

        currentCorner =
            if (topY <= screenHeight / HALF) { // push to top
                if (leftX >= screenWidth / HALF) {
                    WidgetCorner.TOP_RIGHT
                } else {
                    WidgetCorner.TOP_LEFT
                }
            } else {
                if (leftX >= screenWidth / HALF) {
                    WidgetCorner.BOTTOM_RIGHT
                } else {
                    WidgetCorner.BOTTOM_LEFT
                }
            }
    }

    override fun moveWidgetToCorner(corner: WidgetCorner) {
        currentCorner = corner

        val coordinates = generateCornerCoordinates(widgetRootView)
        val leftX = coordinates?.get(0) ?: 0f
        val rightX = coordinates?.get(1) ?: 0f
        val topY = coordinates?.get(2) ?: 0f
        val bottomY = coordinates?.get(3) ?: 0f

        when (corner) {
            WidgetCorner.TOP_LEFT -> {
                widgetRootView?.let {
                    it.x = leftX
                    it.y = topY
                }
            }

            WidgetCorner.TOP_RIGHT -> {
                widgetRootView?.let {
                    it.x = rightX
                    it.y = topY
                }
            }

            WidgetCorner.BOTTOM_LEFT -> {
                widgetRootView?.let {
                    it.x = leftX
                    it.y = bottomY
                }
            }

            WidgetCorner.BOTTOM_RIGHT -> {
                widgetRootView?.let {
                    it.x = rightX
                    it.y = bottomY
                }
            }

            else -> {}
        }
        onMovedTabToCorner(corner)

        sendNewLocation()
    }

    private fun onMovedTabToCorner(corner: WidgetCorner) {
        if (currentMode == WidgetMode.TAB_MODE) {
            val iconParams = ivSessionUiTabWidgetIcon?.layoutParams as FrameLayout.LayoutParams
            val cornerMargin =
                getDimen(R.dimen.glance_session_ui_tab_widget_corner_side_margin).toInt()
            val iconMargin = getDimen(R.dimen.glance_session_ui_tab_widget_icon_side_margin).toInt()

            val width = getDimen(R.dimen.glance_session_ui_tab_widget_width).toInt()

            when (corner) {
                WidgetCorner.TOP_LEFT, WidgetCorner.BOTTOM_LEFT -> {
                    widgetRootView?.x = cornerMargin.toFloat()
                    iconParams.leftMargin = iconMargin
                    iconParams.rightMargin = 0
                    ivSessionUiTabWidgetIcon?.setImageResource(R.drawable.glance_ic_session_tab_widget_right)
                }

                WidgetCorner.TOP_RIGHT, WidgetCorner.BOTTOM_RIGHT -> {
                    widgetRootView?.x = (Util.getScreenWidth() - width - cornerMargin).toFloat()
                    iconParams.rightMargin = iconMargin
                    iconParams.leftMargin = 0
                    ivSessionUiTabWidgetIcon?.setImageResource(R.drawable.glance_ic_session_tab_widget_left)
                }

                else -> {}
            }
            ivSessionUiTabWidgetIcon?.layoutParams = iconParams
        }
    }

    override fun generateCornerCoordinates(view: View?): FloatArray? {
        val coordinates = FloatArray(4)

        val screenWidth = Util.getScreenWidth()
        val screenHeight = Util.getScreenHeight()

        val widthByMode: HashMap<WidgetMode, Int> = object : HashMap<WidgetMode, Int>() {
            init {
                put(WidgetMode.TAB_MODE, R.dimen.glance_session_ui_tab_widget_width)
                put(
                    WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE,
                    R.dimen.glance_session_ui_app_share_widget_width
                )
                put(
                    WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE,
                    R.dimen.glance_session_ui_video_widget_width
                )
                put(
                    WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE,
                    R.dimen.glance_session_ui_video_widget_width
                )
            }
        }

        val heightByMode: HashMap<WidgetMode, Int> = object : HashMap<WidgetMode, Int>() {
            init {
                put(WidgetMode.TAB_MODE, R.dimen.glance_session_ui_tab_widget_height)
                put(
                    WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE,
                    R.dimen.glance_session_ui_app_share_widget_height
                )
                put(
                    WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE,
                    R.dimen.glance_session_ui_video_widget_container_height
                )
                put(
                    WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE,
                    R.dimen.glance_session_ui_two_way_video_widget_container_height
                )
            }
        }

        val width = getDimenAux(widthByMode)
        val height = getDimenAux(heightByMode)

        // - left X
        coordinates[0] = screenWidth * SESSION_VIDEO_WIDGET_TOP_RIGHT_CORNER_SPACE_RATE

        // need to subtract the view width because this method places the view's left side at
        // the given X, so the view might be cut
        // - right X
        coordinates[1] = screenWidth * SESSION_VIDEO_WIDGET_BOTTOM_LEFT_CORNER_SPACE_RATE - width

        // - top Y
        coordinates[2] = screenHeight * SESSION_VIDEO_WIDGET_TOP_RIGHT_CORNER_SPACE_RATE

        // - bottom Y
        coordinates[3] = screenHeight * SESSION_VIDEO_WIDGET_BOTTOM_LEFT_CORNER_SPACE_RATE - height

        return coordinates
    }

    private fun getDimenAux(measureByMode: Map<WidgetMode, Int>): Int {
        val dimenId = measureByMode[currentMode]
        return if (dimenId != null) getDimen(dimenId).toInt() else 0
    }

    protected fun dismissView(view: View?, remove: Boolean) {
        view?.let {
            val viewParent: ViewParent? = view.parent
            if (viewParent is ViewGroup) {
                view.clearAnimation()
                view.visibility = View.GONE
                if (remove) {
                    viewParent.removeView(view)
                }
            }
        }
    }

    private fun switchVisitorVideoViews() {
        flVisitorVideoContainer?.visibility =
            if (state?.videoEnabled == true) View.VISIBLE else View.GONE

        clVisitorVideoOff?.visibility = if (state?.videoEnabled == true) View.GONE else View.VISIBLE
    }

    protected fun changeVisitorVideoViewState(targetMode: WidgetMode?) {
        widgetRootView?.let {
            switchVisitorVideoViews()

            if (cameraManager?.isCameraOpened == true) {
                if (cameraManager?.isFrontCameraSelected == true) { // camera currently showing on squared visitor video
                    cameraManager?.onCloseCamera()
                    if (state?.videoEnabled == false) {
                        // prevent camera to open in some situations
                        cameraManager?.initCameraWhenAvailable(false)
                    }
                } else {
                    // currently showing back camera on the full view, so when flipping to the front
                    // camera, we should start with it disabled, so no need to open the camera right away
                    cameraManager?.flipCamera(R.id.aftvVisitorVideo, !isLargeMode(currentMode))

                    // show agent off view again
                    cvVisitorBackVideoContainer?.visibility = View.GONE

                    // show the squared visitor video back
                    rlVisitorVideoContainer?.visibility = View.VISIBLE
                    flVisitorVideoContainer?.visibility = View.VISIBLE
                    clVisitorVideoOff?.isVisible = state?.videoEnabled != true

                    ibVisitorCameraMode?.isActivated = true // front camera
                }
            } else if (state?.videoEnabled == true) {
                if (aftvVisitorVideo?.isAvailable == true) { // camera is closed
                    cameraManager?.onOpenCamera(
                        clVisitorVideoContainer?.width ?: 0,
                        clVisitorVideoContainer?.height ?: 0
                    )
                } else { // camera should open but the surface is not ready yet
                    cameraManager?.initCameraWhenAvailable(true)
                }
            }

            if (ibExpandContractVideo != null && isLargeMode(currentMode)) {
                if (Glance.isAgentVideoEnabled() && cameraManager?.isFrontCameraSelected == true) {
                    ibExpandContractVideo?.visibility = View.VISIBLE
                } else {
                    ibExpandContractVideo?.visibility = View.INVISIBLE
                }
            }

            clVisitorVideoContainer?.contentDescription =
                getString(if (state?.videoEnabled == true) R.string.glance_accessibility_visitor_video_on else R.string.glance_accessibility_visitor_video_off)

            ibVisitorCameraMode?.let {
                cvAgentVideoContainer?.importantForAccessibility =
                    if (ibVisitorCameraMode?.isActivated == true) View.IMPORTANT_FOR_ACCESSIBILITY_YES else View.IMPORTANT_FOR_ACCESSIBILITY_NO
            }

            sendNewVideosSize(targetMode)
        }
    }

    private fun sendNewVideosSize(widgetMode: WidgetMode?) {
        var videoMode: VisitorVideoSizeMode? = null
        val width = clVisitorVideoContainer?.width ?: 0
        val height = clVisitorVideoContainer?.height ?: 0

        if (!isLargeMode(widgetMode)) {
            videoMode = VisitorVideoSizeMode.VIDEO_SMALL_MODE
            Glance.updateVisitorVideoSize(width, height, VisitorVideoSizeMode.VIDEO_SMALL_MODE)
        } else if (cvVisitorBackVideoContainer != null) {
            videoMode = VisitorVideoSizeMode.VIDEO_LARGE_MODE
            cvVisitorBackVideoContainer?.post {
                Glance.updateVisitorVideoSize(height, height, VisitorVideoSizeMode.VIDEO_LARGE_MODE)
            }
        }

        if (videoMode != null) {
            CommonUtils.onSDKEventTriggered(
                this, "sendNewVideosSize",
                mapOf(
                    "videosize" to String.format(
                        "message:videosize;size:%s;previewwidth:%d;previewheight:%d",
                        videoMode.value,
                        width,
                        height
                    )
                )
            )
        }
    }

    companion object {
        private const val SESSION_VIDEO_WIDGET_BOTTOM_LEFT_CORNER_SPACE_RATE = 0.95f

        private const val SESSION_VIDEO_WIDGET_TOP_RIGHT_CORNER_SPACE_RATE = 0.05f

        private const val HALF = 2

        private const val LARGE_AGENT_VIDEO_SCREEN_SIZE = 0.7

        const val LARGE_WIDGET_VISITOR_VIDEO_SCREEN_SIZE: Float = 0.25f

        private const val MOVE_GESTURE_EDGE_GAP_LIMIT = 15

        private const val MIN_LONG_PRESS_DELAY = 100

        private var LAST_TOUCH_IN_MILLIS: Long = 0
    }
}
