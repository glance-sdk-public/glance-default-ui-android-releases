package net.glance.defaultui.kotlin

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.SurfaceTexture
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.Surface
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Keep
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ProcessLifecycleOwner
import net.glance.android.DialogSessionMode
import net.glance.android.Event
import net.glance.android.EventConstants
import net.glance.android.Glance
import net.glance.android.SessionDialogListener
import net.glance.android.SessionUICompletionListener
import net.glance.android.SessionUIListener
import net.glance.android.StartParams
import net.glance.android.VideoMode
import net.glance.android.VisitorInitParams
import net.glance.android.VisitorListener
import net.glance.android.VisitorVideoSizeMode
import net.glance.android.WidgetCorner
import net.glance.android.WidgetVisibilityMode
import net.glance.android.api.GlanceTimeout
import net.glance.defaultui.R
import net.glance.defaultui.kotlin.DefaultSessionUI.PendingLifecycleTask
import net.glance.defaultui.kotlin.SessionDialog.Companion.newInstance
import net.glance.defaultui.kotlin.blurdialogfragment.BlurDialogEngine
import net.glance.defaultui.kotlin.util.CommonUtils
import net.glance.defaultui.kotlin.util.SessionDialogListenerAdapter
import net.glance.glancevideo.CameraManager
import net.glance.glancevideo.GlanceSize
import net.glance.glancevideo.videosession.VideoSession
import java.lang.ref.WeakReference

class DefaultSessionUI @Keep constructor(context: Context, activity: Activity, startParams: StartParams?) : BaseSessionUIView() {
    
    internal fun interface PendingLifecycleTask {
        fun onLifecycleResume()
    }

    private var previousForegroundActivity: WeakReference<Activity?>? = null

    private var pendingLifecycleTask: PendingLifecycleTask? = null

    private var listener: SessionUIListener? = null

    private var dialogListener: SessionDialogListener? = null

    private var areTermsAccepted = false

    private var sessionUIContainer: ViewGroup? = null

    private var sessionUIContainerView: View? = null

    private var blurEngine: BlurDialogEngine? = null

    private var foundDimensions = false

    private var isVisitorVideoAdded = false

    private var sessionParams: StartParams? = null

    init {
        mContext = context
        mForegroundActivity = WeakReference(activity)
        handler = Handler(Looper.getMainLooper())

        sessionParams = startParams

        SessionState.reset(context) // make sure we'll have a fresh session
        state = SessionState.loadFromPreferences(context)

        initWidgetDragListener()

        runOnMainThread {
            ProcessLifecycleOwner.get().lifecycle.addObserver(object : DefaultLifecycleObserver {
                override fun onResume(owner: LifecycleOwner) {
                    pendingLifecycleTask?.onLifecycleResume()
                    pendingLifecycleTask = null

                    if ((isTwoWayVideo(currentMode) || isLargeMode(currentMode)) && state?.videoEnabled == true) {
                        videoSession?.resume()
                        cameraManager?.onOpenCamera()
                        CommonUtils.updateVisitorVideoState(
                            false,
                            this@DefaultSessionUI
                        );

                    }
                }

                override fun onPause(owner: LifecycleOwner) {
                    if ((isTwoWayVideo(currentMode) || isLargeMode(currentMode)) && state?.videoEnabled == true && videoSession?.isPaused == false) {
                        videoSession?.stopEncoding()
                        cameraManager?.onCloseCamera()
                        CommonUtils.updateVisitorVideoState(
                            true,
                            this@DefaultSessionUI
                        );
                    }
                }
            })
        }
    }

    fun setVisitorVideoAdded(connected: Boolean) {
        isVisitorVideoAdded = connected
    }

    override fun isVisitorVideoAdded(): Boolean {
        return isVisitorVideoAdded
    }

    override fun isVisitorVideoStreaming(): Boolean {
        return videoSession != null && videoSession?.isVisitorVideoStreaming == true
    }

    override fun setTermsAccepted(accepted: Boolean) {
        areTermsAccepted = accepted
    }

    override fun areTermsAccepted(): Boolean {
        return areTermsAccepted
    }

    override fun startVisitorVideo(sparams: StartParams, groupId: Int, invokeShowWidget: Boolean) {
        videoStartParams = sparams

        if (sparams.video != VideoMode.VideoOff) {
            videoSession = VideoSession.getInstance()

            videoSession?.let {
                val alreadyConnected = it.isConnected
                it.initWithGroupId(groupId)
                it.setListener(this)
                if (alreadyConnected) {
                    // If was connected already, start again with new parameters
                    // Since camera is already open, deviceConnected won't happen again
                    it.deviceConnected()
                    it.start(sparams)
                }
            }

            isVisitorVideoAdded = true

            if (invokeShowWidget) {
                showWidgetFromStartVideoMode()
            }
        }
    }

    override fun setSessionParams(params: StartParams) {
        sessionParams = params
    }

    override fun processUserMessage(event: Event) {
        val message = event.GetValue(EventConstants.ATTR_MESSAGE_KEY)
        if (TextUtils.isEmpty(message)) {
            return
        }
        when (message) {
            EventConstants.ATTR_MESSAGE_WIDGET_LOCATION -> if (!isLargeMode(currentMode)) {
                val location = event.GetValue(EventConstants.ATTR_VALUE_LOCATION)
                currentCorner = WidgetCorner.fromServer(location)

                runOnMainThread { moveWidgetToCorner(currentCorner) }
            }

            EventConstants.ATTR_MESSAGE_WIDGET_VISIBILITY -> {
                val visibility =
                    WidgetVisibilityMode.getEnum(event.GetValue(EventConstants.ATTR_VALUE_VISIBILITY))

                runOnMainThread {
                    if (visibility == WidgetVisibilityMode.TAB_MODE) {
                        calculateNearestCorner()
                        animateWidgetToTabTransition()
                    } else {
                        animateTabToWidgetTransition(sessionUIContainerView?.x ?: 0f)
                    }
                }
            }

            EventConstants.ATTR_MESSAGE_VISITOR_VIDEO_REQUESTED -> {
                if (videoSession?.isConnected == true) {
                    return
                }
                val videoMode =
                    if (videoStartParams != null) videoStartParams?.video else VideoMode.VideoOff
                val newMode = if (videoMode == VideoMode.VideoLargeVisitor) {
                    VideoMode.VideoLargeMultiway
                } else {
                    VideoMode.VideoSmallMultiway
                }
                if (videoMode == VideoMode.VideoOff || !areTermsAccepted) {
                    promptVisitorVideo(newMode)
                } else {
                    Glance.addVisitorVideo(newMode)
                }
            }

            EventConstants.ATTR_MESSAGE_PAUSE_VISITOR_VIDEO_REQUESTED -> runOnMainThread { ibVideoState?.performClick() }
            EventConstants.ATTR_MESSAGE_VISITOR_VIDEO_SIZE -> if (isTwoWayVideo(currentMode) || isLargeMode(
                    currentMode
                )
            ) {
                runOnMainThread { ibExpandContractVideo?.performClick() }
            }
        }
    }

    override fun promptVisitorVideo(videoMode: VideoMode) {
        dialogListener = object : SessionDialogListenerAdapter() {
            override fun onAcceptTerms() {
                Glance.addVisitorVideo(videoMode)
            }

            override fun onDeclinedTerms() {
                closeCamera()
            }
        }
        buildSessionDialog(DialogSessionMode.TWO_WAY_VIDEO_AND_APP_SHARING, dialogListener)
    }

    override fun onSessionKeyReceived(sessionKey: String) {
        if (dialogListener == null) {
            dialogListener = object : SessionDialogListenerAdapter() {
                override fun onEndSession(activity: Activity) {
                    Glance.endSession()
                }
            }
        }
        dialogListener?.let { onSessionKeyReceivedAux(sessionKey, it) }
    }

    override fun onSessionKeyReceived(sessionKey: String, dialogListener: SessionDialogListener?) {
        onSessionKeyReceivedAux(sessionKey, dialogListener)
    }

    private fun onSessionKeyReceivedAux(
        sessionKey: String,
        dialogListener: SessionDialogListener?
    ) {
        sessionParams?.setKey(sessionKey)
        buildSessionDialog(DialogSessionMode.SESSION_CODE, dialogListener)
    }

    private fun updateAgentVideoViewId(mode: WidgetMode?) {
        if (mode == null) {
            Log.e(TAG, "Attempted to load a widget with null mode")
            return
        }
        // we can't reuse the same sessionView id for the agent video
        val agentSessionVideoId =
            if (mode == WidgetMode.LARGE_VIDEO) {
                R.id.svSessionLargeAgentVideo
            } else {
                if (mode == WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE) {
                    R.id.svSessionAgentOnlyVideo
                } else {
                    R.id.svSessionAgentVideo
                }
            }
        Glance.setCustomAgentVideoSessionViewId(agentSessionVideoId)
    }

    override fun onAgentVideoConnected(listener: SessionUICompletionListener) {
        completionListener = listener
        val sessionKey = if (videoStartParams != null) {
            videoStartParams?.keyAsString
        } else {
            null
        }
        videoStartParams =
            sessionParams //TODO: remove when the group setting changes back to trigger EventMessageReceived
        sessionKey?.let { videoStartParams?.setKey(sessionKey) }
        if (videoStartParams?.mainCallId == null) {
            videoStartParams?.mainCallId = Glance.getCallId()
        }

        if (hasAgentVideo(currentMode)) {
            changeAgentVideoState(true)
            completionListener?.onWidgetViewsLoaded()
        } else {
            val isLargeVideo = when (videoStartParams?.video) {
                VideoMode.VideoLargeVisitor -> true
                VideoMode.VideoLargeMultiway -> true
                else -> false
            }
            setDragListener = true
            showWidget(if (isLargeVideo) WidgetMode.LARGE_VIDEO else WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE)
        }
    }

    override fun deviceConnected() {
        Log.d(TAG, "deviceConnected")
        videoSession?.let { videoSession ->
            videoSession.deviceConnected()
            if (foundDimensions) {
                Log.d(TAG, "Starting video session...")
                videoStartParams?.let {
                    videoSession.start(it)
                } ?: run {
                    Log.d(TAG, "Can't start video session: videoStartParams is null")
                }
            } else {
                Log.w(TAG, "ISSUE: deviceConnected but no dimensions found")
            }
        }
    }

    override fun deviceDidUpdateDimensions(dimensions: GlanceSize, deviceDimensions: GlanceSize) {
        Log.d(
            TAG, String.format(
                "deviceDidUpdateDimensions -- camera: %d x %d | device screen: %d x %d",
                dimensions.width, dimensions.height, deviceDimensions.width,
                deviceDimensions.height
            )
        )
        foundDimensions = true
        videoSession?.updateDimensions(dimensions, deviceDimensions)
    }

    override fun showSessionDialog(listener: SessionDialogListener) {
        val mode =
            if (sessionParams?.video != VideoMode.VideoOff) DialogSessionMode.TWO_WAY_VIDEO_AND_APP_SHARING else null
        showSessionDialogNoMsgAux(listener, mode)
    }

    override fun showSessionDialog(listener: SessionDialogListener, mode: DialogSessionMode) {
        showSessionDialogNoMsgAux(listener, mode)
    }

    override fun showSessionDialog(
        listener: SessionDialogListener?,
        mode: DialogSessionMode?,
        warningMessage: String?,
        dismiss: Boolean
    ) {
        dialogListener = listener
        buildSessionDialog(mode, dialogListener, warningMessage, dismiss)
    }

    private fun showSessionDialogNoMsgAux(
        listener: SessionDialogListener?,
        mode: DialogSessionMode?
    ) {
        dialogListener = listener
        buildSessionDialog(mode, dialogListener)
    }

    override fun hideSessionDialog() {
        sessionDialog?.dismiss()
    }

    override fun onConfigurationChanged() {
        showWidget(currentMode, true)
    }

    override fun onActivityChanged(activity: Activity, isChangingConfigurations: Boolean) {
        previousForegroundActivity = WeakReference(mForegroundActivity?.get())
        mForegroundActivity = WeakReference(activity)
        isActivityChangingConfigurations = isChangingConfigurations
        setDragListener = true

        showWidget(currentMode)
        restoreSessionDialog(activity)
    }

    private fun restoreSessionDialog(activity: Activity) {
        val dialog = (activity as AppCompatActivity).supportFragmentManager.findFragmentByTag(
            SessionDialog.TAG
        )
        if (dialog is SessionDialog) {
            sessionDialog = dialog
            sessionDialog?.setSessionDialogListener(dialogListener)
        }
    }

    fun onPermissionResult(permission: String?) {
        permissionListener?.let {
            val permissionCheck = mForegroundActivity?.get()?.let {
                ContextCompat.checkSelfPermission(it, Manifest.permission.CAMERA)
            }

            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                permissionListener?.onPermissionRequestGranted(permission)
            } else {
                permissionListener?.onPermissionRequestDenied(permission)
            }
        }
    }

    private fun buildSessionDialog(mode: DialogSessionMode?, listener: SessionDialogListener?) {
        buildSessionDialog(mode, listener, null, true)
    }

    @SuppressLint("CommitTransaction") //we can't call commit() otherwise it will make the dark blurred background to be hidden
    private fun buildSessionDialog(
        mode: DialogSessionMode?,
        listener: SessionDialogListener?,
        warningMessage: String?,
        dismiss: Boolean
    ) {
        runOnMainThread {
            val compatActivity = mForegroundActivity.get()
            if (compatActivity !is AppCompatActivity) {
                Log.e(TAG, "Activity must be a subclass of AppCompatActivity")
                return@runOnMainThread
            }

            val fragmentManager = compatActivity.supportFragmentManager
            val ft = fragmentManager.beginTransaction()
            val prev = fragmentManager.findFragmentByTag(SessionDialog.TAG)
            prev?.let { ft.remove(prev) }
            ft.addToBackStack(null)

            pendingLifecycleTask = PendingLifecycleTask {
                val activity = mForegroundActivity?.get() ?: return@PendingLifecycleTask
                addBlurredDialogBackground(activity)
                sessionDialog = newInstance(mode, warningMessage, dismiss).apply {
                    sessionParams?.let { setSessionParams(it) }
                    setSessionDialogListener(listener)
                    show(fragmentManager.beginTransaction(), SessionDialog.TAG)
                }
            }

            val currentState: Lifecycle.State = ProcessLifecycleOwner.get().lifecycle.currentState
            if (currentState == Lifecycle.State.RESUMED) {
                pendingLifecycleTask?.onLifecycleResume()
                pendingLifecycleTask = null
            } else {
                Log.d(
                    TAG, String.format(
                        "Could not show the dialog now because the current " +
                                "Lifecycle.State (%s) is different from the expected RESUMED value",
                        currentState
                    )
                )
            }
        }
    }

    private fun addBlurredDialogBackground(activity: Activity) {
        val inflatedView = View.inflate(activity, R.layout.dialog_underneath_view, null)
        val llp = ConstraintLayout.LayoutParams(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.MATCH_PARENT
        )
        activity.addContentView(inflatedView, llp)
    }

    private fun showWidget(mode: WidgetMode?, forceRelayout: Boolean = false) {
        if (mode == null) {
            return
        }

        runOnMainThread {
            val activity = mForegroundActivity?.get()
            state = SessionState.loadFromPreferences(activity)

            val rootView = activity?.findViewById<View>(android.R.id.content)?.rootView as ViewGroup

            updateAgentVideoViewId(mode)

            val previousActivity = previousForegroundActivity
            val currentActivity = mForegroundActivity

            val activityChanged =
                mode == currentMode && previousActivity != null && currentActivity != null && previousActivity.get() != currentActivity.get()

            val inflateView = forceRelayout || activityChanged || sessionUIContainer == null
            if (inflateView) {
                dismissView(sessionUIContainer, true)
                sessionUIContainer = activity.layoutInflater.inflate(
                    R.layout.session_ui_layout,
                    rootView,
                    false
                ) as ViewGroup
                sessionUIContainerView = null
                rootView.addView(sessionUIContainer) //README: not attaching the container to the activity root since we need to reuse it in all other activities

                if (activityChanged || cameraManager == null) {
                    cameraManager?.onCloseCamera()
                    cameraManager = CameraManager(
                        this,
                        activity,
                        sessionUIContainer,
                        null,
                        R.id.aftvVisitorVideo
                    )
                    listener = cameraManager
                }

                sessionUIContainer?.findViewById<ViewGroup>(R.id.flSessionUILayout)
                    ?.let { loadBorderView(it) }
                loadWidgetByMode(mode)
                interruptA11yReadout()
                sessionUIContainerView?.announceForAccessibility(getString(R.string.glance_accessibility_share_screen))

                completionListener?.onWidgetViewsLoaded()
            } else if (mode != currentMode) { // we are changing the widget mode
                loadWidgetByMode(mode)
            }
        }
    }

    override fun showWidget() {
        val mode = widgetModeFromStartParams
        setDragListener = true

        showWidget(mode)
    }

    override fun showWidgetFromStartVideoMode() {
        val videoMode = startVideoMode

        // if we don't have a video mode set up, just keep showing the current widget
        if (videoMode != VideoMode.VideoOff) {
            val mode = getWidgetModeFromVideoMode(videoMode)
            setDragListener = true
            showWidget(mode)
        }
    }

    override fun getStartVideoMode(): VideoMode {
        return if (Glance.isPresenceConnected()) {
            val presenceVideoMode = Glance.getPresenceVideoMode()
            if (presenceVideoMode != VideoMode.VideoOff) {
                presenceVideoMode
            } else {
                VideoMode.VideoSmallMultiway
            }
        } else {
            videoStartParams?.video ?: VideoMode.VideoOff
        }
    }

    override fun hide() {
        runOnMainThread {
            Log.d(TAG, "hide")
            closeCamera()

            hideSessionDialog()

            if (currentMode == WidgetMode.LARGE_VIDEO) {
                clLargeVideoWidgetBackground?.visibility = View.GONE
                blurEngine?.onDismiss()
            }

            sessionUIBorder?.let {
                it.visibility = View.GONE
            }

            sessionUIContainerView?.let {
                it.announceForAccessibility(getString(R.string.glance_accessibility_stop_share_screen))

                dismissView(it, true)
                sessionUIContainerView = null
            }

            sessionUIContainer?.let {
                it.removeAllViews()
                dismissView(sessionUIContainer, true)
                sessionUIContainer = null
            }

            SessionState.reset(mForegroundActivity?.get())

            currentMode = null
            modeBeforeExpanding = null
            modeBeforeMinimizing = null
            fullView = false

            mContext = null
            mForegroundActivity = null
            previousForegroundActivity = null
            isActivityChangingConfigurations = false
            handler = null
            sessionParams = null

            dialogListener = null
            sessionDialog = null
            cameraManager = null

            isVisitorVideoAdded = false
            areTermsAccepted = false
            videoSession?.let {
                it.end()
                videoSession = null
            }
        }
    }

    private fun loadWidgetByMode(mode: WidgetMode?) {
        if (mode == null) {
            Log.e(TAG, "Attempted to load a widget with null mode")
            return
        }

        when (mode) {
            WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE -> onAppShareWithPhoneAudioMode()
            WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE -> onAgentWithPhoneAudioMode()
            WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE -> onTwoWayVideoWithVisitorMode()
            WidgetMode.LARGE_VIDEO -> onLargeVideoMode()
            WidgetMode.TAB_MODE -> onTabMode()
        }

        if (clVisitorVideoContainer != null) {
            val dimen = getDimen(R.dimen.glance_session_ui_visitor_video_widget_height).toInt()
            Glance.updateVisitorVideoSize(dimen, dimen, VisitorVideoSizeMode.VIDEO_SMALL_MODE)
        } else {
            Glance.updateVisitorVideoSize(0, 0, VisitorVideoSizeMode.VIDEO_SMALL_MODE)
        }

        sessionUIBorder?.visibility = View.VISIBLE
    }

    private val widgetModeFromStartParams: WidgetMode
        get() {
            val videoMode = sessionParams?.video
            return getWidgetModeFromVideoMode(videoMode)
        }

    private fun getWidgetModeFromVideoMode(videoMode: VideoMode?): WidgetMode {
        var mode = WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE

        if (videoMode != null) {
            when (videoMode) {
                VideoMode.VideoSmallVisitor, VideoMode.VideoSmallMultiway -> mode =
                    WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE

                VideoMode.VideoLargeVisitor, VideoMode.VideoLargeMultiway -> mode =
                    WidgetMode.LARGE_VIDEO

                VideoMode.VideoOff -> {}
                else -> {}
            }
        } else {
            Log.w(TAG, "Video mode invalid, setting APP_SHARE_WITH_PHONE_AUDIO_MODE as the default")
        }

        return mode
    }

    override fun onClickToExpandWidget() {
        loadWidgetByMode(modeBeforeMinimizing)

        updateWidgetVisibility(WidgetVisibilityMode.FULL_MODE)
    }

    override fun onClickToContractWidget() {
        // then we are contracting the large widget that was loaded from the beginning, so we need
        // to contract it to one of the two-way-video small widget matching the session params
        if (!isTwoWayVideo(modeBeforeExpanding)) {
            modeBeforeExpanding = getWidgetModeFromVideoMode(VideoMode.VideoSmallVisitor)
        }

        updateAgentVideoViewId(modeBeforeExpanding)
        loadWidgetByMode(modeBeforeExpanding)

        clLargeVideoWidgetBackground?.visibility = View.GONE
        blurEngine?.onDismiss()

        Glance.restartAgentVideo()
    }

    override fun onClickEndSession() {
        Log.d(TAG, "------------END SESSION----------")
        buildSessionDialog(DialogSessionMode.END_SESSION, Glance.getVisitorSessionDialogListener())
    }

    override fun onVideoStateChanged() {
        changeVisitorVideoViewState(currentMode)
    }

    override fun onTabMode() {
        if (currentMode != WidgetMode.TAB_MODE) {
            modeBeforeMinimizing = currentMode
        }

        loadRootView(WidgetMode.TAB_MODE)
        loadTabWidget(sessionUIContainerView)

        currentMode = WidgetMode.TAB_MODE

        moveAndShowWidget()
        setUpSessionViewMoveGestureHandler()
        updateWidgetVisibility(WidgetVisibilityMode.TAB_MODE)
    }

    override fun onAppShareWithPhoneAudioMode() {
        onAppShareModeAux(WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE)

        currentMode = WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE
    }

    override fun onAgentWithPhoneAudioMode() {
        onAgentModeAux(WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE)

        currentMode = WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE
    }

    override fun onTwoWayVideoWithVisitorMode() {
        loadTwoWayVideoLayout(WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE)

        currentMode = WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE
    }

    override fun onLargeVideoMode() {
        updateAgentVideoViewId(WidgetMode.LARGE_VIDEO)

        loadRootView(WidgetMode.LARGE_VIDEO)

        // preparing the background view to be blurried
        clLargeVideoWidgetBackground =
            sessionUIContainer?.findViewById<ConstraintLayout>(R.id.session_ui_large_video_background)
                ?.apply {
                    setBackgroundColor(getColor(R.color.glance_session_ui_large_video_widget_background_color))
                    visibility = View.VISIBLE
                    isClickable = true
                }

        // blurring the background
        blurEngine = mForegroundActivity?.get()?.let { BlurDialogEngine(it) }?.apply {
            setBlurRadius(3)
            setDownScaleFactor(3f)
            setBlurActionBar(true)
            onResume()
        }

        loadLargeVideoWidget(sessionUIContainerView)

        sessionUIContainerView?.visibility = View.VISIBLE

        modeBeforeExpanding = currentMode
        currentMode = WidgetMode.LARGE_VIDEO

        Glance.restartAgentVideo()
    }

    @Suppress("SameParameterValue")
    private fun onAppShareModeAux(targetMode: WidgetMode) {
        loadRootView(targetMode)
        loadAppShareView(sessionUIContainerView)

        moveAndShowWidget(showWidgetOnLoadingLayout)
    }

    @Suppress("SameParameterValue")
    private fun onAgentModeAux(targetMode: WidgetMode) {
        loadRootView(targetMode)
        loadViews(sessionUIContainerView, targetMode)

        moveAndShowWidget()
    }

    /***
     * In order to make things easier, we split the video widget layouts into two main xml files,
     * one for agent mode only and another for the two-way-video scenario. So it's necessary to
     * re-assign the main container view component in order to load the required views.
     */
    @Suppress("SameParameterValue")
    private fun loadTwoWayVideoLayout(targetMode: WidgetMode) {
        loadRootView(targetMode)
        loadViews(sessionUIContainerView, targetMode)

        moveAndShowWidget()
    }

    private fun loadRootView(targetMode: WidgetMode) {
        val containerViewId = sessionUIContainerView?.id ?: -1

        val isTab = targetMode == WidgetMode.TAB_MODE
        val isAppShare = targetMode == WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE
        val isAgentVideo = targetMode == WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE
        val isLargeVideo = targetMode == WidgetMode.LARGE_VIDEO

        if (containerViewId == -1) {
            loadRootViewAux(targetMode)
        } else if ((isTab && containerViewId != R.id.session_ui_tab_widget)
            || (isAppShare && containerViewId != R.id.session_ui_app_share)
            || (isAgentVideo && containerViewId != R.id.session_ui_agent_video)
            || (!isAgentVideo && containerViewId != R.id.session_ui_agent_and_visitor_video)
            || (isLargeVideo && containerViewId != R.id.session_ui_large_video)
        ) {
            dismissView(sessionUIContainerView, false)

            loadRootViewAux(targetMode)
        } // else: we trying to reuse the same layout, so no need for changes
    }

    private fun loadRootViewAux(targetMode: WidgetMode) {
        var layoutId = 0
        var changeCameraManagerRoot = false

        when (targetMode) {
            WidgetMode.TAB_MODE -> layoutId = R.id.session_ui_tab_widget
            WidgetMode.APP_SHARE_WITH_PHONE_AUDIO_MODE -> layoutId = R.id.session_ui_app_share
            WidgetMode.AGENT_WITH_PHONE_AUDIO_MODE -> layoutId = R.id.session_ui_agent_video
            WidgetMode.TWO_WAY_VIDEO_WITH_VISITOR_MODE -> {
                layoutId = R.id.session_ui_agent_and_visitor_video
                changeCameraManagerRoot = true
            }

            WidgetMode.LARGE_VIDEO -> {
                layoutId = R.id.session_ui_large_video
                changeCameraManagerRoot = true
            }

        }
        sessionUIContainerView = sessionUIContainer?.findViewById(layoutId)

        if (changeCameraManagerRoot) {
            cameraManager?.onVideoSurfaceViewChanged(getVisitorVideoViewIdByMode(targetMode))
            cameraManager?.initCameraWhenAvailable(
                (shouldOpenCameraWhenAvailable ?: state?.videoEnabled) == true
            )
            shouldOpenCameraWhenAvailable = null

            cameraManager?.onRootViewChanged(sessionUIContainerView)
        }
    }

    private fun moveAndShowWidget(show: Boolean = true) {
        runOnMainThread {
            sessionUIContainerView?.post {
                // height and width will be ready to use
                moveWidgetToCorner(currentCorner)
                if (show) {
                    sessionUIContainerView?.visibility = View.VISIBLE
                }
            }
        }
    }

    // VideoSessionListener methods
    override fun videoSessionDidStartVideoCapture(videoSession: VideoSession) {
        Log.d(TAG, "videoSessionDidStartVideoCapture")
    }

    override fun videoSessionDidConnectStreamer(videoSession: VideoSession) {
        Log.d(TAG, "videoSessionDidConnectStreamer")
    }

    override fun videoSessionDidDisconnnectStreamer(videoSession: VideoSession) {
        Log.d(TAG, "videoSessionDidDisconnnectStreamer")
    }

    override fun videoSessionDidConnectVideoSource(videoSession: VideoSession) {
        Log.d(TAG, "videoSessionDidConnectVideoSource")
    }

    override fun videoSessionDidDisconnectVideoSource(videoSession: VideoSession) {
        Log.d(TAG, "videoSessionDidDisconnectVideoSource")
    }

    override fun videoSessionDidFailToConnectVideoSource(videoSession: VideoSession, error: Error) {
        Log.d(TAG, "videoSessionDidFailToConnectVideoSource")
    }

    override fun videoSessionDidFailConnectStreamer(videoSession: VideoSession, error: Error) {
        Log.d(TAG, "videoSessionDidFailConnectStreamer")
    }

    override fun videoSessionDidStart(videoSession: VideoSession) {
        Log.d(TAG, "videoSessionDidStart")
    }

    override fun videoSessionDidEnd(videoSession: VideoSession) {
        Log.d(TAG, "videoSessionDidEnd")
    }

    override fun videoSessionWillStartStreaming(videoSession: VideoSession) {
        Log.d(TAG, "videoSessionWillStartStreaming")
    }

    override fun videoSessionWillStopStreaming(videoSession: VideoSession) {
        Log.d(TAG, "videoSessionWillStopStreaming")
    }

    override fun videoSessionWillChangeQuality(videoSession: VideoSession) {
        Log.d(TAG, "videoSessionWillChangeQuality")
    }

    override fun videoSessionInvitation(
        videoSession: VideoSession,
        sessionType: String,
        username: String,
        sessionKey: String
    ) {
        Log.d(TAG, "videoSessionInvitation")
    }

    override fun videoSessionEncoderSurfaceAndTexture(
        surface: Surface,
        surfaceTexture: SurfaceTexture
    ) {
        this.listener?.let {
            handler?.post {
                it.sessionUIEncoderSurfaceAndTexture(surface, surfaceTexture)
                videoSession?.encoderSurfaceAttached(surface)
            }
        }
    }

    override fun videoSessionDidResizeDimensions(width: Int, height: Int) {
        Log.d(
            TAG, String.format(
                "videoSessionDidResizeDimensions: %s x %s", width,
                height
            )
        )
        this.listener?.sessionUIDidResizeDimensions(width, height)
    }

    companion object {

        private var sessionParams: StartParams? = null

        @JvmStatic
        fun init(
            activity: Activity,
            initParams: VisitorInitParams,
            maskKeyboard: Boolean,
            listener: VisitorListener?
        ) {
            Glance.init(activity, initParams)
            Glance.maskKeyboard(maskKeyboard)
            if (listener != null) {
                Glance.addVisitorListener(listener)
            }

            val startParams = StartParams()
            startParams.setKey(initParams.visitorId)
            Glance.setSessionUIImplementation(DefaultSessionUI::class.java, startParams)
        }

        @JvmStatic
        fun init(
            activity: Activity,
            startParams: StartParams?,
            maskKeyboard: Boolean,
            groupId: Int,
            visitorId: String?,
            eventsListener: VisitorListener?
        ) {
            sessionParams = startParams
            init(activity, startParams, maskKeyboard, groupId, visitorId, null, eventsListener)
        }

        @JvmStatic
        fun init(
            activity: Activity,
            startParams: StartParams?,
            maskKeyboard: Boolean,
            groupId: Int,
            visitorId: String?,
            glanceServer: String?,
            eventsListener: VisitorListener?
        ) {
            sessionParams = startParams
            Glance.init(activity, groupId, visitorId, glanceServer, eventsListener)
            Glance.maskKeyboard(maskKeyboard)
            Glance.setSessionUIImplementation(DefaultSessionUI::class.java, startParams)
        }

        @JvmStatic
        @JvmOverloads
        fun startSession(startSessionTimeout: GlanceTimeout? = null) {
            sessionParams?.let { Glance.startSession(it, false, startSessionTimeout) } ?: run {
                Log.w(
                    TAG,
                    "DefaultSessionUI not initialized. You need to call DefaultSessionUI.init(...) first"
                )
            }
        }
    }
}