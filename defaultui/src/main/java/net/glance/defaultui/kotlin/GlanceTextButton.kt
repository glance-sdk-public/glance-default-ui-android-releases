package net.glance.defaultui.kotlin

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatButton
import androidx.core.widget.TextViewCompat
import net.glance.defaultui.R
import kotlin.math.min

class GlanceTextButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = R.style.GlanceDialogButtonTextStyle
) : AppCompatButton(context, attrs, defStyleAttr) {

    private enum class ButtonType(val type: Int) {
        Positive(0),

        @Suppress("unused")  // might be used by the customers
        Negative(1)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(
            measureWidth(widthMeasureSpec),
            measureHeight(heightMeasureSpec)
        )
    }

    private fun measureWidth(measureSpec: Int): Int {
        var preferred = resources.getDimension(R.dimen.glance_dialog_bt_width).toInt()
        preferred = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            preferred.toFloat(),
            resources.displayMetrics
        ).toInt()
        return getMeasurement(measureSpec, preferred)
    }

    private fun measureHeight(measureSpec: Int): Int {
        var preferred = resources.getDimension(R.dimen.glance_dialog_bt_height).toInt()
        preferred = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            preferred.toFloat(),
            resources.displayMetrics
        ).toInt()
        return getMeasurement(measureSpec, preferred)
    }

    private fun getMeasurement(measureSpec: Int, preferred: Int): Int {
        val specSize = MeasureSpec.getSize(measureSpec)

        val measurement = when (MeasureSpec.getMode(measureSpec)) {
            MeasureSpec.EXACTLY ->                 // This means the width of this view has been given.
                specSize

            MeasureSpec.AT_MOST ->                 // Take the minimum of the preferred size and what
                // we were told to be.
                min(preferred.toDouble(), specSize.toDouble()).toInt()

            else -> preferred
        }
        return measurement
    }

    init {
        val scaledDensity = resources.configuration.fontScale * resources.displayMetrics.density
        val maxFontSize = Math.round(textSize / scaledDensity)
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(
            this,
            2,
            maxFontSize,
            1,
            TypedValue.COMPLEX_UNIT_SP
        )

        if (attrs != null) {
            val typedValue = TypedValue()
            val attribute = intArrayOf(R.styleable.GlanceTextButton_glance_text_bt_type)
            val array = context.obtainStyledAttributes(typedValue.resourceId, attribute)
            val type = array.getInt(0, ButtonType.Positive.type)
            array.recycle()

            if (type != ButtonType.Positive.type) {
                when (type) {
                    0 -> setPositive()
                    1 -> setNegative()
                    else -> {}
                }
            }
        } else {
            setPositive()
        }
    }

    private fun setPositive() {
        setBackgroundResource(R.drawable.glance_dialog_rounded_primary_button_shape)
        setText(R.string.glance_accept)
    }

    private fun setNegative() {
        setBackgroundResource(R.drawable.glance_dialog_rounded_secondary_button_shape)
        setText(R.string.glance_decline)
    }
}
