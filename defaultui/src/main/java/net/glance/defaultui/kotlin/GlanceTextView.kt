package net.glance.defaultui.kotlin

import android.content.Context
import android.graphics.Paint
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ScaleXSpan
import android.util.AttributeSet
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.TextViewCompat
import net.glance.defaultui.R

class GlanceTextView : AppCompatTextView {
    enum class TextType(val type: Int) {
        Default(-1),
        Terms(0),
        Brand(1);

        companion object {
            fun valueOfLabel(type: Int): TextType {
                for (e in entries) {
                    if (e.type == type) {
                        return e
                    }
                }
                return Default
            }
        }
    }

    var type: TextType? = null

    constructor(context: Context) : super(context) {
        init(context, null, -1)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs, -1)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(context, attrs, defStyle)
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyle: Int) {
        val scaledDensity = resources.configuration.fontScale * resources.displayMetrics.density
        val maxFontSize = Math.round(textSize / scaledDensity)
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(
            this,
            2,
            maxFontSize,
            1,
            TypedValue.COMPLEX_UNIT_SP
        )

        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.GlanceTextView, defStyle, 0)
            val type = a.getInt(
                R.styleable.GlanceTextView_glance_textview_special_type,
                TextType.Default.type
            )
            this.type = TextType.valueOfLabel(type)
            a.recycle()

            if (type != TextType.Default.type) {
                when (type) {
                    0 -> setupTermsText()
                    1 -> setupBrandText()
                    else -> {}
                }
            }
        }
    }

    private fun setupTermsText() {
        paintFlags = paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }

    private fun setupBrandText() {
        // setting letter spacing - backwards compatible solution
        val originalText = text ?: return

        val builder = StringBuilder()

        for (i in originalText.indices) {
            builder.append(originalText[i])
            if (i + 1 < originalText.length) {
                builder.append("\u00A0")
            }
        }

        val spacing = resources.getDimension(R.dimen.glance_dialog_brand_letter_spacing)
        val finalText = SpannableString(builder.toString())
        if (builder.toString().length > 1) {
            var i = 1
            while (i < builder.toString().length) {
                finalText.setSpan(
                    ScaleXSpan((spacing + 1) / 10),
                    i,
                    i + 1,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                i += 2
            }
        }
        super.setText(finalText, BufferType.SPANNABLE)
    }
}
