package net.glance.defaultui.kotlin

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import net.glance.android.DialogSessionMode
import net.glance.android.Glance
import net.glance.android.GlanceAndroidPreferences
import net.glance.android.Util
import net.glance.defaultui.R

class SessionDialog : BaseSessionDialog() {
    private var isRequestingCameraPermission = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            isRequestingCameraPermission = savedInstanceState.getBoolean(
                REQUEST_CAMERA_PERMISSION_STATE, false
            )
        }
        handler = Handler(Looper.getMainLooper())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = super.onCreateView(inflater, container, savedInstanceState)
        setUpEvents()
        setupTermsNConditionsView()

        return rootView
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(REQUEST_CAMERA_PERMISSION_STATE, isRequestingCameraPermission)
        super.onSaveInstanceState(outState)
    }

    private fun setUpEvents() {
        ibCloseDialog?.setOnClickListener { handler?.post { setMode(DialogSessionMode.END_SESSION) } }

        btAcceptTerms?.setOnClickListener(positiveClickListener())

        btDeclineTerms?.setOnClickListener(negativeClickListener())
    }

    private fun positiveClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val bundle = arguments
            val dismiss = bundle?.getBoolean(DIALOG_DISMISS_KEY, false) ?: false
            /* Sometimes we need the dialog instance to be attached on the activity until some
              other task is done. In order to use the Fragment instance to deal with Permissions
              there, we need it to be alive otherwise, it will crash. The default behavior was to
              dismiss the dialog right after accepting/declining the prompt, but when we request
              a permission, we need to keep the dialog alive until the user takes a decision,
              so we can proceed with the flow.
             */
            if (dismiss) {
                dismiss()
            }
            if (dialogListener != null) {
                if (currentMode == DialogSessionMode.END_SESSION) {
                    dialogListener?.onEndSession(activity)
                } else {
                    cameraManager?.onCloseCamera()
                    dialogListener?.onAcceptTerms()
                }
            } else {
                Log.w(TAG, "SessionDialogListener not provided, skipping...")
            }
        }
    }

    private fun negativeClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val sendTermsDeclined = currentMode != DialogSessionMode.END_SESSION
            cameraManager?.onCloseCamera()
            if (previousMode != null && (previousMode == DialogSessionMode.WAITING_FOR_AGENT || previousMode == DialogSessionMode.SESSION_CODE)) {
                handler?.post {
                    setMode(previousMode)
                    previousMode = null
                }
            } else {
                previousMode = null
                currentMode = null

                dismiss()
            }
            if (sendTermsDeclined) {
                dialogListener?.onDeclinedTerms()
            }
        }
    }

    override fun onPromptMode(promptMessage: String) {
        changeDialogWidth(SESSION_CODE_DIALOG_WIDTH)

        ibCloseDialog?.visibility = View.GONE

        clVisitorVideoContainer?.visibility = View.GONE
        clVisitorVideoControlButtonsContainer?.visibility = View.GONE
        clSessionCodeContainer?.visibility = View.GONE
        gtvTermsAndConditions?.visibility = View.GONE
        gtvPoweredByGlance?.visibility = View.GONE

        gtvHeader?.setText(R.string.glance_prompt_dialog_title)
        gtvDescription?.text = promptMessage

        btAcceptTerms?.let {
            it.setText(R.string.glance_no)
            it.setOnClickListener(negativeClickListener())
        }
        btDeclineTerms?.let {
            it.setText(R.string.glance_yes)
            it.setOnClickListener(positiveClickListener())
        }
    }

    override fun onWarningMode(message: String?) {
        changeDialogWidth(SESSION_CODE_DIALOG_WIDTH)

        ibCloseDialog?.visibility = View.GONE

        clVisitorVideoContainer?.visibility = View.GONE
        clVisitorVideoControlButtonsContainer?.visibility = View.GONE
        clSessionCodeContainer?.visibility = View.GONE
        gtvTermsAndConditions?.visibility = View.GONE
        gtvPoweredByGlance?.visibility = View.GONE

        gtvHeader?.setText(R.string.glance_warning_dialog_title)
        gtvDescription?.text = message ?: getString(R.string.glance_general_error_message)

        gtvTermsAndConditions?.visibility =
            if (Glance.arePresenceTermsDisplayed()) View.VISIBLE else View.GONE

        btDeclineTerms?.visibility = View.GONE

        btAcceptTerms?.let {
            it.setText(R.string.glance_accept)
            it.setOnClickListener(if (Glance.arePresenceTermsDisplayed()) positiveClickListener() else View.OnClickListener { dismiss() })
        }
    }

    override fun onTermsAndConditionsMode() {
        changeDialogTop(TERMS_DIALOG_TOP)
        changeDialogHeight(TERMS_DIALOG_HEIGHT)
        changeDialogWidth(TERMS_DIALOG_WIDTH)

        clDialogSessionContainer?.let {
            it.setPadding(
                it.paddingLeft,
                it.paddingTop,
                it.paddingRight, 0
            ) // avoid the buttons to be cut
        }


        clVisitorVideoContainer?.visibility = View.GONE
        clVisitorVideoControlButtonsContainer?.visibility = View.GONE

        gtvHeader?.visibility = View.GONE
        gtvDescription?.visibility = View.GONE
        gtvTermsAndConditions?.visibility = View.GONE
        gtvPoweredByGlance?.visibility = View.GONE

        clTermsAndConditionsContainer?.visibility = View.VISIBLE
        wvTermsAndConditions?.loadUrl(requireArguments().getString(DIALOG_TERMS_URL_KEY, ""))
    }

    override fun onSessionKeyMode(sessionKey: String) {
        changeDialogWidth(SESSION_CODE_DIALOG_WIDTH)

        showCloseButton()
        isCancelable = false

        gtvHeader?.let {
            anchorViewToCloseButton(it, R.dimen.glance_dialog_header_margin_top_close_button)
            it.setText(R.string.glance_agent_code)
        }

        gtvDescription?.visibility = View.GONE
        clAcceptDeclineBtsContainer?.visibility = View.GONE
        gtvTermsAndConditions?.visibility = View.GONE

        showSessionCode(sessionKey)
    }

    override fun onWaitingSessionMode() {
        changeDialogWidth(SESSION_CODE_DIALOG_WIDTH)

        showCloseButton()
        isCancelable = false
        anchorViewToCloseButton(clWaitingSessionAnimContainer, R.dimen.glance_dialog_header_margin_top_close_button)

        clWaitingSessionAnimContainer?.visibility = View.VISIBLE
        wvWaitingSessionAnim?.loadUrl(WAITING_FOR_AGENT_ANIMATION_PATH)

        gtvHeader?.setText(R.string.glance_waiting_for_the_agent)

        gtvDescription?.visibility = View.GONE
        clAcceptDeclineBtsContainer?.visibility = View.GONE
        gtvTermsAndConditions?.visibility = View.GONE
        gtvPoweredByGlance?.visibility = View.GONE

        anchorViews(
            clDialogSessionContainer, gtvHeader, ConstraintSet.TOP,
            clWaitingSessionAnimContainer,
            ConstraintSet.BOTTOM, R.dimen.glance_dialog_header_margin_top_close_button
        )
    }

    override fun onVisitorVideoWithControlsMode() {
        clVisitorVideoContainer?.visibility = View.VISIBLE

        clVisitorVideoControlButtonsContainer?.visibility = View.VISIBLE

        val permissionCheck =
            activity?.let { ContextCompat.checkSelfPermission(it, Manifest.permission.CAMERA) }
        if (!isRequestingCameraPermission) {
            if (GlanceAndroidPreferences.hasCameraPermissionBeenAsked(activity)) {
                if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    changeVisitorVideoViewState() // happy path
                } else {
                    handler?.post {
                        onWarningMode(
                            getString(
                                R.string.glance_denied_camera_permission_warning,
                                Util.getApplicationName(activity)
                            )
                        )
                    }
                }
            } else if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                doRequestCameraPermission()
            }
        } else {
            val sessionUI = defaultSessionUI
            sessionUI?.setOnPermissionResultListener(dialogCameraPermissionResultListener)
        }

        anchorViews(
            clDialogSessionContainer,
            gtvHeader,
            ConstraintSet.TOP,
            clVisitorVideoControlButtonsContainer,
            ConstraintSet.BOTTOM,
            R.dimen.glance_dialog_header_margin_top_close_button
        )

        val presenceEnabled = Glance.isPresenceConnected()

        if (presenceEnabled) {
            Glance.sendPresenceTermsDisplayed()
        }

        val headerStringId =
            if (presenceEnabled) R.string.glance_two_way_video_presence_sharing_dialog_title else R.string.glance_two_way_video_sharing_dialog_title
        val descriptionStringId =
            if (presenceEnabled) R.string.glance_two_way_video_presence_dialog_description else R.string.glance_two_way_video_sharing_dialog_description

        gtvHeader?.setText(headerStringId)
        gtvDescription?.setText(descriptionStringId)
    }

    private fun doRequestCameraPermission() {
        val sessionUI = defaultSessionUI
        sessionUI?.setOnPermissionResultListener(dialogCameraPermissionResultListener)
        requestPermission(Manifest.permission.CAMERA)
        GlanceAndroidPreferences.setCameraPermissionBeenAsked(activity, true)
        isRequestingCameraPermission = true
    }

    override fun onVisitorVideoWithControlsAndCodeMode() {
        changeDialogWidth(VISITOR_VIDEO_WITHSESSION_CODE_DIALOG_WIDTH)

        showCloseButton()
        isCancelable = false
        anchorViewToCloseButton(
            clVisitorVideoContainer,
            R.dimen.glance_dialog_video_margin_top_close_button
        )

        wvTermsAndConditions?.visibility = View.GONE
        gtvDescription?.visibility = View.GONE
        clAcceptDeclineBtsContainer?.visibility = View.GONE

        clVisitorVideoContainer?.visibility = View.VISIBLE
        clVisitorVideoControlButtonsContainer?.visibility = View.VISIBLE

        gtvHeader?.let {
            it.visibility = View.VISIBLE
            it.setText(R.string.glance_agent_code)
        }

        anchorViews(
            clDialogSessionContainer,
            gtvHeader,
            ConstraintSet.TOP,
            clVisitorVideoControlButtonsContainer,
            ConstraintSet.BOTTOM,
            R.dimen.glance_dialog_header_margin_top_close_button
        )

        showSessionCode("- -")

        gtvTermsAndConditions?.visibility = View.GONE
    }

    override fun onEndSessionMode() {
        changeDialogWidth(END_SESSION_DIALOG_WIDTH)

        clWaitingSessionAnimContainer?.visibility = View.GONE

        ibCloseDialog?.visibility = View.GONE
        clVisitorVideoContainer?.visibility = View.GONE
        clVisitorVideoControlButtonsContainer?.visibility = View.GONE
        clSessionCodeContainer?.visibility = View.GONE
        gtvDescription?.visibility = View.GONE
        gtvTermsAndConditions?.visibility = View.GONE
        gtvPoweredByGlance?.visibility = View.GONE

        clAcceptDeclineBtsContainer?.visibility = View.VISIBLE
        gtvHeader?.setText(R.string.glance_end_session_title)

        btAcceptTerms?.let {
            it.setText(R.string.glance_no)
            it.setOnClickListener(negativeClickListener())
        }
        btDeclineTerms?.let {
            it.setText(R.string.glance_yes)
            it.setOnClickListener(positiveClickListener())
        }
    }

    override fun onVideoStateChanged() {
        changeVisitorVideoViewState()
    }

    /**
     * Anchor the view top side to the close button bottom side
     *
     * @param view the view to be used as the link source
     */
    private fun anchorViewToCloseButton(view: View?, marginDimenId: Int) {
        if (view != null) {
            anchorViews(
                clDialogSessionContainer, view, ConstraintSet.TOP, ibCloseDialog,
                ConstraintSet.BOTTOM, marginDimenId
            )
        }
    }

    private fun showSessionCode(sessionKey: String) {
        clSessionCodeContainer?.visibility = View.VISIBLE
        gtvSessionCode?.text = sessionKey

        if (activity?.resources?.getBoolean(R.bool.GLANCE_DIALOG_SHOW_GLANCE_TEXT) == true) {
            gtvPoweredByGlance?.visibility = View.VISIBLE
        } else {
            gtvPoweredByGlance?.visibility = View.INVISIBLE
        }

        anchorViews(
            clDialogSessionContainer,
            gtvPoweredByGlance,
            ConstraintSet.TOP,
            clSessionCodeContainer,
            ConstraintSet.BOTTOM,
            R.dimen.glance_dialog_header_margin_top_close_button
        )
    }

    companion object {
        @JvmField
        var TAG: String = "Session Dialog"

        private const val REQUEST_CAMERA_PERMISSION_STATE =
            "com.glance.session.camera_permission_state"

        @JvmStatic
        fun newInstance(
            mode: DialogSessionMode?,
            warningMessage: String?,
            dismiss: Boolean?
        ): SessionDialog {
            val sessionDialog = SessionDialog()

            val args = Bundle()

            args.putSerializable(DIALOG_MODE, mode)
            args.putBoolean(DIALOG_DISMISS_KEY, dismiss ?: false)
            args.putString(DIALOG_MESSAGE_RES_ID_KEY, warningMessage)

            sessionDialog.arguments = args
            return sessionDialog
        }
    }
}
