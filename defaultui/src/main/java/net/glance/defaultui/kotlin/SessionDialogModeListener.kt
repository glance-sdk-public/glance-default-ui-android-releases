package net.glance.defaultui.kotlin

interface SessionDialogModeListener {
    fun onPromptMode(promptMessage: String)

    fun onWarningMode(message: String?)

    fun onTermsAndConditionsMode()

    fun onSessionKeyMode(sessionKey: String)

    fun onWaitingSessionMode()

    fun onVisitorVideoWithControlsMode()

    fun onVisitorVideoWithControlsAndCodeMode()

    fun onEndSessionMode()
}
