package net.glance.defaultui.kotlin

import android.content.Context
import android.content.SharedPreferences
import android.util.Log

// Class to hold any UI state we might want to persist
class SessionState {
    // Visitor video controls
    internal var videoEnabled: Boolean = false

    companion object {
        private const val TAG = "GlanceSessionState"

        private const val PREFS_NAME = "PREF_GLANCE_VISITOR"

        private const val PREF_VIDEO_ENABLED: String = "PREF_VIDEO_ENABLED"

        private var visitorPreferences: SharedPreferences? = null

        private var visitorPreferencesEditor: SharedPreferences.Editor? = null

        fun loadFromPreferences(context: Context?): SessionState {
            val instance = SessionState()

            instance.videoEnabled = isVideoEnabled(context)

            return instance
        }

        private fun initiatePreferencesIfNull(context: Context) {
            if (visitorPreferences == null) {
                visitorPreferences = context.getSharedPreferences(PREFS_NAME, 0)
                visitorPreferencesEditor = visitorPreferences?.edit()
            }
        }

        private fun saveChanges() {
            visitorPreferencesEditor?.apply()
        }

        fun reset(context: Context?) {
            if (context != null) {
                initiatePreferencesIfNull(context)
                visitorPreferences?.edit()?.clear()?.apply()
            } else {
                Log.w(TAG, "Could not reset since context is null")
            }
        }

        private fun isVideoEnabled(context: Context?): Boolean {
            if (context != null) {
                initiatePreferencesIfNull(context)
                return visitorPreferences?.getBoolean(PREF_VIDEO_ENABLED, true) == true
            } else {
                Log.w(TAG, "Could not check isVideoEnabled since context is null")
                return false
            }
        }

        internal fun setVideoEnabled(context: Context?, enabled: Boolean) {
            if (context != null) {
                initiatePreferencesIfNull(context)
                visitorPreferencesEditor?.putBoolean(PREF_VIDEO_ENABLED, enabled)
                saveChanges()
            } else {
                Log.w(TAG, "Could not setVideoEnabled since context is null")
            }
        }
    }
}
