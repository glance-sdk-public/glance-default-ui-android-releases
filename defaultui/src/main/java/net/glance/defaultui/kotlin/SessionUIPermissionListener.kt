package net.glance.defaultui.kotlin

interface SessionUIPermissionListener {
    fun onPermissionRequestGranted(permission: String?)

    fun onPermissionRequestDenied(permission: String?)
}
