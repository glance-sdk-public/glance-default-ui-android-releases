package net.glance.defaultui.kotlin

interface SessionUIWidgetCommonControlListener {
    fun onClickToExpandWidget() // from tab mode

    fun onClickToContractWidget() // from large to the previous mode

    fun onClickEndSession()
}
