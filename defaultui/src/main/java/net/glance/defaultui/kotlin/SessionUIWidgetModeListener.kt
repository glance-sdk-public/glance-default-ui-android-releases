package net.glance.defaultui.kotlin

/***
 * Interface that will be used to define how the widget should look like depending on the options
 * passed into the start session. Each method consider that all checkings were already made
 * (ex.: video available?) so we just need to set the views up accordingly.
 */
interface SessionUIWidgetModeListener {
    fun onTabMode()

    fun onAppShareWithPhoneAudioMode() // only the blue headphone square with the end session

    fun onAgentWithPhoneAudioMode() // only the end session button

    fun onTwoWayVideoWithVisitorMode() // both agent and visitor videos + visitor video and end session button

    fun onLargeVideoMode()
}
