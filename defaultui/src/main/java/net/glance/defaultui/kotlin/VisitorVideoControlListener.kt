package net.glance.defaultui.kotlin

interface VisitorVideoControlListener {
    fun onVideoStateChanged()
}
