package net.glance.defaultui.kotlin

/***
 * The main goal of this enumaration is to have a better control of the views visibility,
 * behaviors and buttons actions depending on the current widget mode. It will be also used to
 * clearly define the transitions (escalations) between modes.
 */
enum class WidgetMode {
    TAB_MODE,
    APP_SHARE_WITH_PHONE_AUDIO_MODE,  // only the blue headphone square with the end session
    AGENT_WITH_PHONE_AUDIO_MODE,  // only the end session button
    TWO_WAY_VIDEO_WITH_VISITOR_MODE,  // both agent and visitor videos + visitor video and end session button
    LARGE_VIDEO
}
