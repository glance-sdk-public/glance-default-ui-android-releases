package net.glance.defaultui.kotlin.blurdialogfragment

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.DisplayMetrics
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.WindowInsets
import android.view.WindowManager
import android.view.animation.AccelerateInterpolator
import android.view.animation.LinearInterpolator
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import kotlinx.coroutines.runBlocking
import net.glance.defaultui.R
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.math.ceil
import kotlin.math.max

/**
 * Encapsulate the whole behaviour to provide a blur effect on a DialogFragment.
 *
 *
 * All the screen behind the dialog will be blurred except the action bar.
 *
 *
 * Simply linked all methods to the matching lifecycle ones.
 */
class BlurDialogEngine(holdingActivity: Activity) {
    /**
     * Image view used to display blurred background.
     */
    private var blurredBackgroundView: ImageView? = null

    /**
     * Layout params used to add blurred background.
     */
    private var blurredBackgroundLayoutParams: FrameLayout.LayoutParams? = null

    private var executorBlurringService: ExecutorService? = null

    private var handler: Handler? = null

    private var background: Bitmap? = null

    private var backgroundView: View? = null

    /**
     * Factor used to down scale background. High quality isn't necessary
     * since the background will be blurred.
     */
    private var downScaleFactor = DEFAULT_BLUR_DOWN_SCALE_FACTOR

    /**
     * Radius used for fast blur algorithm.
     */
    private var blurRadius = DEFAULT_BLUR_RADIUS

    /**
     * Holding activity.
     */
    private var holdingActivity: Activity?

    /**
     * Allow to use a toolbar without set it as action bar.
     */
    private var toolbar: Toolbar? = null

    /**
     * Duration used to animate in and out the blurred image.
     *
     *
     * In milli.
     */
    private val animationDuration: Int

    /**
     * Boolean used to know if the actionBar should be blurred.
     */
    private var blurredActionBar = false

    init {
        this.holdingActivity = holdingActivity
        animationDuration =
            holdingActivity.resources.getInteger(R.integer.blur_dialog_animation_duration)
    }

    /**
     * Must be linked to the original lifecycle.
     *
     * @param context holding activity.
     */
    fun onAttach(context: Context?) {
        holdingActivity = context as Activity?
    }

    /**
     * Resume the engine.
     */
    fun onResume() {
        if (blurredBackgroundView == null) {
            if (holdingActivity?.window?.decorView?.isShown == true) {
                executorBlurringService = Executors.newSingleThreadExecutor()
                handler = Handler(Looper.getMainLooper())
                executeBlurringTask()
            } else {
                holdingActivity?.window?.decorView?.viewTreeObserver?.addOnPreDrawListener(
                    object : ViewTreeObserver.OnPreDrawListener {
                        override fun onPreDraw(): Boolean {
                            // dialog can have been closed before being drawn

                            holdingActivity?.let {
                                it.window.decorView
                                    .viewTreeObserver.removeOnPreDrawListener(this)
                                executorBlurringService = Executors.newSingleThreadExecutor()
                                handler = Handler(Looper.getMainLooper())
                                executeBlurringTask()
                            }
                            return true
                        }
                    }
                )
            }
        }
    }

    @SuppressWarnings("deprecation")
    private fun executeBlurringTask() {
        onPreBlurringTaskExecute()

        executorBlurringService?.let { it ->
            it.execute {
                //process to the blur
                val bitmap = background
                val view = backgroundView
                if (bitmap != null && view != null && (!it.isShutdown || !it.isTerminated)) {
                    blur(bitmap, view)
                }

                //clear memory
                background?.recycle()

                // Update UI on the main thread
                handler?.post {
                    @Suppress("DEPRECATION")
                    backgroundView?.let {
                        it.destroyDrawingCache()
                        it.isDrawingCacheEnabled = false
                    }

                    blurredBackgroundView?.let {
                        val parent = it.parent
                        if (parent is ViewGroup) {
                            parent.removeView(it)
                        }
                        holdingActivity?.window?.addContentView(it, blurredBackgroundLayoutParams)

                        it.alpha = 0f
                        it.animate()
                            .alpha(1f)
                            .setDuration(animationDuration.toLong())
                            .setInterpolator(LinearInterpolator())
                            .start()
                    }

                    backgroundView = null
                    background = null
                }
            }
        }
    }

    private fun onPreBlurringTaskExecute() {
        backgroundView = holdingActivity?.window?.decorView

        //retrieve background view, must be achieved on ui thread since
        //only the original thread that created a view hierarchy can touch its views.
        val rect = Rect()
        backgroundView?.getWindowVisibleDisplayFrame(rect)
        resetDrawingCache()

        // After rotation, the DecorView has no height and no width. Therefore, .getDrawingCache()
        // returns null. That's why we  have to force measure and layout.
        if (background == null) {
            backgroundView?.let {
                it.measure(
                    View.MeasureSpec.makeMeasureSpec(rect.width(), View.MeasureSpec.EXACTLY),
                    View.MeasureSpec.makeMeasureSpec(rect.height(), View.MeasureSpec.EXACTLY)
                )
                it.layout(0, 0, it.measuredWidth, it.measuredHeight)
            }
            resetDrawingCache()
        }
    }

    @SuppressWarnings("deprecation")
    private fun resetDrawingCache() {
        @Suppress("DEPRECATION")
        backgroundView?.let {
            it.destroyDrawingCache()
            it.isDrawingCacheEnabled = true
            it.buildDrawingCache(true)
            background = it.getDrawingCache(true)
        }
    }

    /**
     * Must be linked to the original lifecycle.
     */
    @SuppressLint("NewApi")
    fun onDismiss() {
        //remove blurred background and clear memory, could be null if dismissed before blur effect
        //processing ends
        //cancel async task
        executorBlurringService?.shutdown() // TODO: check if it will really work for our needs, regarding the non-guarantee of stopping

        blurredBackgroundView
            ?.animate()
            ?.alpha(0f)
            ?.setDuration(animationDuration.toLong())
            ?.setInterpolator(AccelerateInterpolator())
            ?.setListener(
                object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        removeBlurredView()
                    }

                    override fun onAnimationCancel(animation: Animator) {
                        super.onAnimationCancel(animation)
                        removeBlurredView()
                    }
                }
            )
            ?.start()
    }

    /**
     * Must be linked to the original lifecycle.
     */
    fun onDetach() {
        executorBlurringService?.shutdown() // TODO: check if it will really work for our needs, regarding the non-guarantee of stopping
        executorBlurringService = null
        holdingActivity = null
    }

    /**
     * Apply custom down scale factor.
     *
     *
     * By default down scale factor is set to
     * [BlurDialogEngine.DEFAULT_BLUR_DOWN_SCALE_FACTOR]
     *
     *
     * Higher down scale factor will increase blurring speed but reduce final rendering quality.
     *
     * @param factor customized down scale factor, must be at least 1.0 ( no down scale applied )
     */
    fun setDownScaleFactor(factor: Float) {
        downScaleFactor = max(factor.toDouble(), 1.0).toFloat()
    }

    /**
     * Apply custom blur radius.
     *
     *
     * By default blur radius is set to
     * [BlurDialogEngine.DEFAULT_BLUR_RADIUS]
     *
     * @param radius custom radius used to blur.
     */
    fun setBlurRadius(radius: Int) {
        blurRadius = max(radius.toDouble(), 0.0).toInt()
    }

    /**
     * Enable / disable blurred action bar.
     *
     *
     * When enabled, the action bar is blurred in addition of the content.
     *
     * @param enable true to blur the action bar.
     */
    fun setBlurActionBar(enable: Boolean) {
        blurredActionBar = enable
    }

    /**
     * Set a toolbar which isn't set as action bar.
     *
     * @param toolbar toolbar.
     */
    fun setToolbar(toolbar: Toolbar?) {
        this.toolbar = toolbar
    }

    /**
     * Blur the given bitmap and add it to the activity.
     *
     * @param bkg  should be a bitmap of the background.
     * @param view background view.
     */
    private fun blur(bkg: Bitmap, view: View) {
        //define layout params to the previous imageView in order to match its parent
        blurredBackgroundLayoutParams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.MATCH_PARENT
        )

        //overlay used to build scaled preview and blur background
        var overlay: Bitmap?

        //evaluate top offset due to action bar, 0 if the actionBar should be blurred.
        val actionBarHeight = if (blurredActionBar) {
            0
        } else {
            actionBarHeight
        }

        //evaluate top offset due to status bar
        val topOffset = getTopOffset(actionBarHeight)
        // evaluate bottom or right offset due to navigation bar.
        var bottomOffset = 0
        var rightOffset = 0
        val navBarSize = navigationBarOffset

        if (holdingActivity?.resources?.getBoolean(R.bool.blur_dialog_has_bottom_navigation_bar) == true) {
            bottomOffset = navBarSize
        } else {
            rightOffset = navBarSize
        }

        //add offset to the source boundaries since we don't want to blur actionBar pixels
        val srcRect = Rect(
            0,
            topOffset,
            bkg.width - rightOffset,
            bkg.height - bottomOffset
        )

        //in order to keep the same ratio as the one which will be used for rendering, also
        //add the offset to the overlay.
        val height =
            ceil(((view.height - topOffset - bottomOffset) / downScaleFactor).toDouble())
        val width =
            ceil((view.width - rightOffset) * height / (view.height - topOffset - bottomOffset))

        overlay = Bitmap.createBitmap(width.toInt(), height.toInt(), Bitmap.Config.RGB_565)

        try {
            if (holdingActivity is AppCompatActivity) {
                //add offset as top margin since actionBar height must also considered when we display
                // the blurred background. Don't want to draw on the actionBar.
                blurredBackgroundLayoutParams?.let {
                    it.setMargins(0, actionBarHeight, 0, 0)
                    it.gravity = Gravity.TOP
                }
            }
        } catch (e: NoClassDefFoundError) {
            // no dependency to appcompat, that means no additional top offset due to actionBar.
            blurredBackgroundLayoutParams?.setMargins(0, 0, 0, 0)
        }
        //scale and draw background view on the canvas overlay
        val canvas = Canvas(overlay)
        val paint = Paint()
        paint.flags = Paint.FILTER_BITMAP_FLAG

        //build drawing destination boundaries
        val destRect = RectF(0f, 0f, overlay.width.toFloat(), overlay.height.toFloat())

        try {
            //draw background from source area in source background to the destination area on the overlay
            canvas.drawBitmap(bkg, srcRect, destRect, paint)

            runBlocking {
                try {
                    overlay?.let { outerOverlay ->
                        overlay = GlideBlurHelper.doBlur(outerOverlay, 5, holdingActivity)
                        overlay?.let { innerOverlay ->
                            //set bitmap in an image view for final rendering
                            blurredBackgroundView = ImageView(holdingActivity)
                            blurredBackgroundView?.let {
                                it.scaleType = ImageView.ScaleType.CENTER_CROP
                                it.setImageDrawable(
                                    BitmapDrawable(
                                        holdingActivity?.resources,
                                        innerOverlay
                                    )
                                )
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } catch (e: Exception) {
            Log.w(TAG, "Could not process blur due to the following exception: ", e)
        }
    }

    private fun getTopOffset(actionBarHeight: Int): Int {
        var statusBarHeight = 0
        @SuppressWarnings("deprecation")
        @Suppress("DEPRECATION")
        if ((holdingActivity?.window?.attributes?.flags?.and(WindowManager.LayoutParams.FLAG_FULLSCREEN)) == 0) {
            //not in fullscreen mode
            statusBarHeight = this.statusBarHeight
        }

        // check if status bar is translucent to remove status bar offset in order to provide blur
        // on content bellow the status.
        if (isStatusBarTranslucent) {
            statusBarHeight = 0
        }

        return actionBarHeight + statusBarHeight
    }

    /**
     * Retrieve action bar height.
     *
     * @return action bar height in px.
     */
    private val actionBarHeight: Int
        get() {
            val safeActivity = holdingActivity
            val actionBarHeight = try {
                toolbar?.height ?: if (safeActivity is AppCompatActivity) {
                        safeActivity.supportActionBar?.height
                    } else {
                        safeActivity?.actionBar?.height
                    }
            } catch (e: NoClassDefFoundError) {
                safeActivity?.actionBar?.height
            }
            return actionBarHeight ?: 0
        }

    /**
     * retrieve status bar height in px
     *
     * @return status bar height in px
     */
    private val statusBarHeight: Int
        get() {
            val rectangle = Rect()
            val rootView = holdingActivity?.window?.decorView
            rootView?.getWindowVisibleDisplayFrame(rectangle)
            return if (rectangle.top < rectangle.height() * 0.2f) {
                rectangle.top
            } else {
                0
            }
        }

    /**
     * Retrieve offset introduce by the navigation bar.
     *
     * @return bottom offset due to navigation bar.
     */
    private val navigationBarOffset: Int
        @SuppressWarnings("deprecation")
        @Suppress("DEPRECATION")
        get() {
            val windowManager = holdingActivity?.windowManager
            val navigationBarHeight: Int? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                windowManager?.currentWindowMetrics?.windowInsets?.getInsets(WindowInsets.Type.navigationBars())?.bottom
            } else {
                val metrics = DisplayMetrics()
                windowManager?.defaultDisplay?.getMetrics(metrics)
                val usableHeight = metrics.heightPixels
                windowManager?.defaultDisplay?.getRealMetrics(metrics)
                val realHeight = metrics.heightPixels
                if (realHeight > usableHeight) {
                    realHeight - usableHeight
                } else {
                    null
                }
            }
            return navigationBarHeight ?: 0
        }

    /**
     * Used to check if the status bar is translucent.
     *
     * @return true if the status bar is translucent.
     */
    private val isStatusBarTranslucent: Boolean
        get() {
            val typedValue = TypedValue()
            val attribute = intArrayOf(android.R.attr.windowTranslucentStatus)
            val array = holdingActivity?.obtainStyledAttributes(typedValue.resourceId, attribute)
            val isStatusBarTranslucent = array?.getBoolean(0, false) ?: false
            array?.recycle()
            return isStatusBarTranslucent
        }

    /**
     * Removed the blurred view from the view hierarchy.
     */
    private fun removeBlurredView() {
        blurredBackgroundView?.let {
            val parent = it.parent
            parent?.let {
                (parent as ViewGroup).removeView(blurredBackgroundView)
            }
            blurredBackgroundView = null
        }
    }

    companion object {
        /**
         * Since image is going to be blurred, we don't care about resolution.
         * Down scale factor to reduce blurring time and memory allocation.
         */
        const val DEFAULT_BLUR_DOWN_SCALE_FACTOR: Float = 4.0f

        /**
         * Radius used to blur the background
         */
        const val DEFAULT_BLUR_RADIUS: Int = 8

        /**
         * Default dimming policy.
         */
        const val DEFAULT_DIMMING_POLICY: Boolean = false

        /**
         * Default action bar blurred policy.
         */
        const val DEFAULT_ACTION_BAR_BLUR: Boolean = false

        /**
         * Default use of RenderScript.
         */
        const val DEFAULT_USE_RENDERSCRIPT: Boolean = false

        /**
         * Log cat
         */
        private val TAG: String = BlurDialogEngine::class.java.simpleName
    }
}
