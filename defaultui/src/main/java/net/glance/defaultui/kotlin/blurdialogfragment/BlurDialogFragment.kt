package net.glance.defaultui.kotlin.blurdialogfragment

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.DialogFragment
import net.glance.defaultui.R

/**
 * Encapsulate dialog behavior with blur effect for app using [DialogFragment].
 *
 *
 * All the screen behind the dialog will be blurred except the action bar.
 */
abstract class BlurDialogFragment : DialogFragment() {
    /**
     * Engine used to blur.
     */
    private var blurEngine: BlurDialogEngine? = null

    /**
     * Allow to set a Toolbar which isn't set as actionbar.
     */
    private var toolbar: Toolbar? = null

    /**
     * Dimming policy.
     */
    private var dimmingEffect = false

    override fun onAttach(context: Context) {
        super.onAttach(context)
        blurEngine?.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (activity != null) {
            blurEngine = BlurDialogEngine(activity as Activity)

            toolbar?.let {
                blurEngine?.setToolbar(toolbar)
            }

            val radius = blurRadius
            require(radius > 0) { "Blur radius must be strictly positive. Found : $radius" }
            blurEngine?.setBlurRadius(radius)

            val factor = downScaleFactor
            require(!(factor <= 1.0)) { "Down scale must be strictly greater than 1.0. Found : $factor" }

            blurEngine?.let {
                it.setDownScaleFactor(factor)
                it.setBlurActionBar(isActionBarBlurred)
            }

            dimmingEffect = isDimmingEnable
        } else {
            Log.w(TAG, "Couldn't render the blur effect: Activity is null")
        }
    }

    override fun onStart() {
        val dialog = dialog
        dialog?.let {
            val window = dialog.window
            if (window != null) {
                // enable or disable dimming effect.
                if (!dimmingEffect) {
                    window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
                }

                // add default fade to the dialog if no window animation has been set.
                val currentAnimation = window.attributes.windowAnimations
                if (currentAnimation == 0) {
                    window.attributes.windowAnimations = R.style.BlurDialogFragment_Default_Animation
                } else {
                    //no op
                }
            } else {
                Log.w(TAG, "Couldn't render the blur effect: Window is null")
            }
        }
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        blurEngine?.onResume()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        blurEngine?.onDismiss()
    }

    override fun onDetach() {
        super.onDetach()
        blurEngine?.onDetach()
    }

    override fun onDestroyView() {
        dialog?.setDismissMessage(null)
        super.onDestroyView()
    }

    /**
     * Allow to set a Toolbar which isn't set as ActionBar.
     *
     *
     * Must be called before onCreate.
     *
     * @param toolBar toolBar
     */
    fun setToolbar(toolBar: Toolbar?) {
        toolbar = toolBar
        blurEngine?.setToolbar(toolBar)
    }

    protected open val downScaleFactor: Float
        /**
         * For inheritance purpose.
         *
         *
         * Allow to customize the down scale factor.
         *
         *
         * The factor down scaled factor used to reduce the size of the source image.
         * Range :  ]1.0,infinity)
         *
         * @return customized down scaled factor.
         */
        get() = BlurDialogEngine.DEFAULT_BLUR_DOWN_SCALE_FACTOR

    protected open val blurRadius: Int
        /**
         * For inheritance purpose.
         *
         *
         * Allow to customize the blur radius factor.
         *
         *
         * radius down scaled factor used to reduce the size of the source image.
         * Range :  [1,infinity)
         *
         * @return customized blur radius.
         */
        get() = BlurDialogEngine.DEFAULT_BLUR_RADIUS

    protected open val isDimmingEnable: Boolean
        /**
         * For inheritance purpose.
         *
         *
         * Enable or disable the dimming effect.
         *
         *
         * Disabled by default.
         *
         * @return true to enable the dimming effect.
         */
        get() = BlurDialogEngine.DEFAULT_DIMMING_POLICY

    protected open val isActionBarBlurred: Boolean
        /**
         * For inheritance purpose.
         *
         *
         * Enable or disable the blur effect on the action bar.
         *
         *
         * Disable by default.
         *
         * @return true to enable the blur effect on the action bar.
         */
        get() = BlurDialogEngine.DEFAULT_ACTION_BAR_BLUR

    protected open val isRenderScriptEnable: Boolean
        /**
         * For inheritance purpose.
         *
         *
         * Enable or disable RenderScript.
         *
         *
         * Disable by default.
         *
         *
         * Don't forget to add those lines to your build.gradle if your are using Renderscript
         * <pre>
         * defaultConfig {
         * ...
         * renderscriptTargetApi 22
         * renderscriptSupportModeEnabled true
         * ...
         * }
        </pre> *
         *
         * @return true to enable RenderScript.
         */
        get() = BlurDialogEngine.DEFAULT_USE_RENDERSCRIPT

    companion object {
        private val TAG: String = BlurDialogFragment::class.java.name
    }
}
