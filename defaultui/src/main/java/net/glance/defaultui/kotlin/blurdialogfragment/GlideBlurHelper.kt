package net.glance.defaultui.kotlin.blurdialogfragment

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

/**
 * Simple helper used to blur a bitmap thanks to render script.
 */
internal object GlideBlurHelper {
    /**
     * Log cat
     */
    private val TAG: String = GlideBlurHelper::class.java.simpleName

    /**
     * blur a given bitmap
     *
     * @param sentBitmap       bitmap to blur
     * @param radius           blur radius
     * @param canReuseInBitmap true if bitmap must be reused without blur
     * @param context          used by RenderScript, can be null if RenderScript disabled
     * @return blurred bitmap
     */
    suspend fun doBlur(
        sentBitmap: Bitmap,
        radius: Int,
        context: Context?
    ): Bitmap? {
        return suspendCancellableCoroutine { continuation ->
            if (context != null) {
                Glide.with(context)
                    .asBitmap()
                    .load(sentBitmap)
                    .transform(BlurTransformation(radius))
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            continuation.resume(resource)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                            // Handle cleanup if needed
                        }

                        override fun onLoadFailed(errorDrawable: Drawable?) {
                            continuation.resumeWithException(Exception("Blurring failed"))
                        }
                    })
            }
        }
    }
}
