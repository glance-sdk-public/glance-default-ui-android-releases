package net.glance.defaultui.kotlin.util

import net.glance.android.Glance
import net.glance.defaultui.kotlin.BaseSessionUI

object CommonUtils {
    fun updateVisitorVideoState(paused: Boolean, baseSessionUIInstance: BaseSessionUI?) {
        Glance.updateVisitorVideoStatus(paused)

        onSDKEventTriggered(
            baseSessionUIInstance, "updateVisitorVideoState",
            mapOf("visitorvideopaused" to String.format("paused:%b", paused))
        )
    }

    fun onSDKEventTriggered(
        baseSessionUIInstance: BaseSessionUI?,
        method: String,
        params: Map<String, String>
    ) {
        if (baseSessionUIInstance != null) {
            val glanceSDKEventSenderListener: GlanceSDKEventSenderListener? =
                baseSessionUIInstance.getGlanceSDKEventSenderListener();

            glanceSDKEventSenderListener?.onEventSent(buildSDKEventDataObj(method, params))
        }
    }

    fun buildSDKEventDataObj(method: String, params: Map<String, String>): String {
        val result = StringBuilder("{\n")

        result.append("        type: SDK").append(",\n")
        result.append("        method: ").append(method).append(",\n")
        result.append("        params: {")

        if (params != null) {
            for ((key, value) in params) {
                result.append("\n            ").append(key).append(": ").append(value).append(",")
            }
        }

        val currentLength = result.length
        if (',' == result[currentLength - 1]) {
            result.delete(currentLength - 1, currentLength)
        }
        result.append("\n        }")
        result.append("\n    }")

        return result.toString()
    }
}
