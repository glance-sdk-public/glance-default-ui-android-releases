package net.glance.defaultui.kotlin.util

interface GlanceSDKEventSenderListener {
    fun onEventSent(eventData: String?)
}
