package net.glance.defaultui.kotlin.util

import android.app.Activity
import net.glance.android.SessionDialogListener

/**
 * Auxiliary class that provides a base implementation for [SessionDialogListener]
 *
 * This class can be helpful for those cases where you are interested in override some but not all
 * methods from [SessionDialogListener]
 */
open class SessionDialogListenerAdapter : SessionDialogListener {
    override fun onAcceptTerms() {
    }

    override fun onDeclinedTerms() {
    }

    override fun onEndSession(activity: Activity) {
    }
}
