package net.glance.defaultuidemo_java.common.constants;

public class Constants {

    public static String GLANCE_TAG = "DefaultUIDemo_Java";

    public final static String EXTRA_WEBVIEW_URL = "url";

    public final static String EXTRA_WEBVIEW_SELECTORS = "querySelectors";

    public final static String EXTRA_WEBVIEW_LABELS = "labels";
}
