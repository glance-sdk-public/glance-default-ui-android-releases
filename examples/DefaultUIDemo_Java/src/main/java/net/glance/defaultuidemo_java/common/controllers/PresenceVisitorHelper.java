package net.glance.defaultuidemo_java.common.controllers;

import android.app.Activity;

import net.glance.android.Glance;
import net.glance.android.StartParams;
import net.glance.android.VisitorListener;
import net.glance.defaultui.java.DefaultSessionUI;

import java.util.Map;

public class PresenceVisitorHelper {

    private Activity activity;

    /***
     * If you are not using Presence you do not need to implement any code within if (VISITOR_ID != null)
     ***/
//    private static final String VISITOR_ID = "defaultUI123";
        private static final String VISITOR_ID = null;

    private final int glanceGroupId;

    public PresenceVisitorHelper(Activity activity, int groupId) {
        this.activity = activity;
        this.glanceGroupId = groupId;
    }

    public String getVisitorId() {
        return VISITOR_ID;
    }

    public boolean init(VisitorListener visitorListener, StartParams startParams) {
        if (VISITOR_ID != null) {
            DefaultSessionUI.init(activity, startParams, true, glanceGroupId, VISITOR_ID, visitorListener);
            return true;
        }
        return false;
    }

    public void connect() {
        Glance.connectToPresence();
        Glance.setPresenceTermsUrl("https://github.com/");
    }

    public void signalPresenceAgent(Map paramsMap) {
        if (VISITOR_ID != null) {
            Glance.sendToPresenceSession("presence", paramsMap);
        }
    }

    public void disconnectVisitor() {
        if (VISITOR_ID != null) {
            Glance.disconnectPresence();
        }
    }
}
