package net.glance.defaultuidemo_java.common.controllers;

import android.app.Activity;

import androidx.appcompat.app.AlertDialog;

import net.glance.android.Event;
import net.glance.android.EventCode;
import net.glance.android.EventType;
import net.glance.android.Glance;
import net.glance.android.StartParams;
import net.glance.android.VisitorListener;
import net.glance.defaultui.java.DefaultSessionUI;
import net.glance.defaultuidemo_java.R;
import net.glance.defaultuidemo_java.views.common.UIStateChangeListener;

import java.util.Map;

public class VisitorHelper {

    private final Activity activity;

    private final VisitorListener visitorListener;

    private final UIStateChangeListener uiStateChangeListener;

    private final PresenceVisitorHelper presenceVisitorHelper;

    private static final int GLANCE_GROUP_ID = 0; // put your own group id here

    private final String TERMS_STATUS_KEY = "sessionkey";

    public VisitorHelper(Activity activity) {
        this.activity = activity;
        this.visitorListener = (VisitorListener) activity;
        this.uiStateChangeListener = (UIStateChangeListener) activity;
        this.presenceVisitorHelper = new PresenceVisitorHelper(activity, GLANCE_GROUP_ID);
    }

    public void handleSessionEvent(Event event) {
        if (event.getCode() == EventCode.EventConnectedToSession) {
            String sessionKey = extractSessionKey(event);
            uiStateChangeListener.onConnectedToSession(sessionKey);
        } else if (event.getCode() == EventCode.EventSessionEnded) {
            uiStateChangeListener.onSessionEnded();
        } else if (event.getType() == EventType.EventWarning ||
                event.getType() == EventType.EventError ||
                event.getType() == EventType.EventAssertFail) {
            uiStateChangeListener.onWarning(event);
        }

        // PRESENCE CODES
        else if (event.getCode() == EventCode.EventVisitorInitialized) {
            uiStateChangeListener.onPresenceInitialized();
        } else if (event.getCode() == EventCode.EventPresenceConnected) {
            uiStateChangeListener.onPresenceConnected();
        } else if (event.getCode() == EventCode.EventPresenceConnectFail) {
            uiStateChangeListener.onPresenceConnectFailed(event);
        } else if (event.getCode() == EventCode.EventPresenceShowTerms) {
            uiStateChangeListener.onPresenceShowTerms();
        } else if (event.getCode() == EventCode.EventPresenceSignal) {
            uiStateChangeListener.onPresenceSignal();
        } else if (event.getCode() == EventCode.EventPresenceBlur) {
            uiStateChangeListener.onPresenceBlur();
        }
    }

    public void init() {
        StartParams startParams = new StartParams();
        startParams.setKey("GLANCE_KEYTYPE_RANDOM");
        startParams.setTermsUrl("https://ww2.glance.net/");

        if (!presenceVisitorHelper.init(visitorListener, startParams)) {
            DefaultSessionUI.init(activity, startParams, true, GLANCE_GROUP_ID, "", visitorListener);
        }
    }

    public String extractSessionKey(Event event) {
        return event.GetValue(TERMS_STATUS_KEY);
    }

    public String getVisitorId() {
        return presenceVisitorHelper.getVisitorId();
    }

    public void startSession() {
        if (GLANCE_GROUP_ID == 0) {
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
            dialogBuilder.setMessage("Please set a valid GROUP_ID value in the VisitorHelper class");
            dialogBuilder.setNegativeButton("Dismiss", (dialog, which) -> {
                        dialog.dismiss();
                        uiStateChangeListener.onSessionEnded();
                    }
            );
            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.show();
        } else {
            DefaultSessionUI.startSession();
        }
    }

    public void connectPresence() {
        presenceVisitorHelper.connect();
    }

    public void signalPresenceAgent(Map paramsMap) {
        presenceVisitorHelper.signalPresenceAgent(paramsMap);
    }

    public void endSession() {
        Glance.endSession();
    }

    public void reset() {
        presenceVisitorHelper.disconnectVisitor();

        Glance.removeMaskedViewId(R.id.tvMaskingTest);
        Glance.removeVisitorListener(visitorListener);
    }
}
