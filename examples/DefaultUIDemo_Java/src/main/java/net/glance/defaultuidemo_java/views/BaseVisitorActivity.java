package net.glance.defaultuidemo_java.views;

import androidx.appcompat.app.AppCompatActivity;

import net.glance.android.Event;
import net.glance.android.VisitorListener;
import net.glance.defaultuidemo_java.common.controllers.VisitorHelper;
import net.glance.defaultuidemo_java.views.common.UIStateChangeListener;

public abstract class BaseVisitorActivity extends AppCompatActivity implements UIStateChangeListener,
        VisitorListener {

    protected VisitorHelper visitorHelper;

    @Override
    protected void onResume() {
        super.onResume();

        visitorHelper = new VisitorHelper(this);
        visitorHelper.init();
    }

    // ----------------------- VISITOR LISTENER ------------------------
    @Override
    public void onGlanceVisitorEvent(Event event) {
        visitorHelper.handleSessionEvent(event);
    }
}
    // -------------------------------- END ------------------------------

    // --------------------------- DefaultUI Listener --------------------
