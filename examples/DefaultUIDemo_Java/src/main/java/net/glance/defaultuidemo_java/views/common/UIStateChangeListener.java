package net.glance.defaultuidemo_java.views.common;

import net.glance.android.Event;

public interface UIStateChangeListener {

    void onConnectedToSession(String sessionKey);

    void onSessionEnded();

    void onWarning(Event event);

    void onPresenceInitialized();

    void onPresenceConnected();

    void onPresenceConnectFailed(Event event);

    void onPresenceShowTerms();

    void onPresenceSignal();

    void onPresenceBlur();

}
