package net.glance.defaultuidemo_java.views.main;

import static net.glance.defaultuidemo_java.common.constants.Constants.GLANCE_TAG;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import net.glance.android.Event;
import net.glance.defaultuidemo_java.R;
import net.glance.defaultuidemo_java.common.constants.Constants;
import net.glance.defaultuidemo_java.views.BaseVisitorActivity;
import net.glance.defaultuidemo_java.views.web.WebViewActivity;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends BaseVisitorActivity {

    private static String GLANCE_SESSION_KEY = "-";

    protected ConstraintLayout pbLoading;

    protected TextView tvSessionKey;

    protected Button btStartSession;

    protected Button btEndSession;

    protected Button btOpenBrowser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        visitorHelper.reset();
    }

    private void setUpUI() {
        pbLoading = findViewById(R.id.pb_loading);

        tvSessionKey = findViewById(R.id.tv_session_key);
        btStartSession = findViewById(R.id.bt_start_session);
        btEndSession = findViewById(R.id.bt_end_session);
        btOpenBrowser = findViewById(R.id.bt_open_browser);

        tvSessionKey.setText(getString(R.string.session_key, "-"));

        btStartSession.setOnClickListener(view -> {
            pbLoading.setVisibility(View.VISIBLE);

            visitorHelper.startSession();
        });

        btEndSession.setOnClickListener(view -> {
            pbLoading.setVisibility(View.VISIBLE);

            visitorHelper.endSession();
        });

        btOpenBrowser.setOnClickListener(view -> {
            String url = "https://d2e93a2oavc15x.cloudfront.net";

            Intent i = new Intent(getApplicationContext(), WebViewActivity.class);
            i.putExtra(Constants.EXTRA_WEBVIEW_URL, url);
            i.putExtra(Constants.EXTRA_WEBVIEW_SELECTORS, ".mask_1, .mask_2, #mask_3, .mask_4, span, #hplogo");
            i.putExtra(Constants.EXTRA_WEBVIEW_LABELS, "mask 1, mask 2, mask 3, mask 4, span, LOGO");

            Map<String, String> paramsMap = new HashMap<>();
            paramsMap.put("url", "webview " + url);
            visitorHelper.signalPresenceAgent(paramsMap);

            startActivity(i);
        });
    }

    @Override
    public void onConnectedToSession(String sessionKey) {
        GLANCE_SESSION_KEY = sessionKey;
        Log.d(GLANCE_TAG, "Session Key (default): " + GLANCE_SESSION_KEY);

        runOnUiThread(() -> {
            updateViews(true);
            pbLoading.setVisibility(View.GONE);
        });
    }

    @Override
    public void onSessionEnded() {
        runOnUiThread(() -> {
            updateViews(false);
            pbLoading.setVisibility(View.GONE);
        });
    }

    @Override
    public void onWarning(Event event) {
        Log.d(GLANCE_TAG, "event: " + event.getType() + ", reason: " + event.getMessageString());
    }

    @Override
    public void onPresenceInitialized() {
        Log.d(GLANCE_TAG, "Presence event visitor initialized");
        visitorHelper.connectPresence();
    }

    @Override
    public void onPresenceConnected() {
        Log.d(GLANCE_TAG, "Presence connected");

        GLANCE_SESSION_KEY = visitorHelper.getVisitorId();
        runOnUiThread(() -> {
            tvSessionKey.setText(getString(R.string.session_key, GLANCE_SESSION_KEY));
            findViewById(R.id.tv_presence_status).setVisibility(View.VISIBLE);
        });
    }

    @Override
    public void onPresenceConnectFailed(Event event) {
        Log.d(GLANCE_TAG, "Presence connection failed (will retry): " + event.getMessageString());
    }

    @Override
    public void onPresenceShowTerms() {
        // You only need to handle this event if you are providing a custom UI for terms and/or confirmation
        Log.d(GLANCE_TAG, "Agent signalled ShowTerms");
    }

    @Override
    public void onPresenceSignal() {
        Log.d(GLANCE_TAG, "Agent sent a signal");
    }

    @Override
    public void onPresenceBlur() {
        Log.d(GLANCE_TAG, "Agent is now using another app or website");
    }

    private void updateViews(boolean sessionStarted) {
        GLANCE_SESSION_KEY = sessionStarted ? GLANCE_SESSION_KEY : "-";

        tvSessionKey.setText(getString(R.string.session_key, GLANCE_SESSION_KEY));
        btStartSession.setEnabled(!sessionStarted);
        btEndSession.setVisibility(sessionStarted ? View.VISIBLE : View.INVISIBLE);
    }
}