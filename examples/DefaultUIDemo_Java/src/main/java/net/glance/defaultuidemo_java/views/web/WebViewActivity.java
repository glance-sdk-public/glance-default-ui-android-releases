package net.glance.defaultuidemo_java.views.web;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import net.glance.android.GlanceWebViewClient;
import net.glance.android.GlanceWebViewJavascriptInterface;
import net.glance.defaultuidemo_java.R;
import net.glance.defaultuidemo_java.common.constants.Constants;

public class WebViewActivity extends AppCompatActivity {

    WebView webView;

    GlanceWebViewClient webViewClient;

    GlanceWebViewJavascriptInterface jsInterface;

    private static final String JS_INTERFACE_NAME = "GLANCE_Mask";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        Bundle extras = getIntent().getExtras();
        String url = (String) extras.get(Constants.EXTRA_WEBVIEW_URL);
        String querySelectors = (String) extras.get(Constants.EXTRA_WEBVIEW_SELECTORS);
        String labels = (String) extras.get(Constants.EXTRA_WEBVIEW_LABELS);

        webView = findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(2);
        }
        jsInterface = new GlanceWebViewJavascriptInterface(webView);
        webView.addJavascriptInterface(jsInterface, JS_INTERFACE_NAME);

        webViewClient = new GlanceWebViewClient(querySelectors, labels);
        webView.setWebViewClient(webViewClient);

        webView.loadUrl(url);
    }

    @Override
    protected void onDestroy() {
        webView.removeJavascriptInterface(JS_INTERFACE_NAME);
        jsInterface.onDestroy();
        super.onDestroy();
    }
}