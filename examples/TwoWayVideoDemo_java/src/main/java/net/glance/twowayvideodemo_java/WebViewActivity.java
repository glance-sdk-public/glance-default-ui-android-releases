package net.glance.twowayvideodemo_java;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import net.glance.android.GlanceWebViewClient;
import net.glance.android.GlanceWebViewJavascriptInterface;

public class WebViewActivity extends AppCompatActivity {
    WebView webView;
    GlanceWebViewClient webViewClient;
    GlanceWebViewJavascriptInterface jsInterface;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        String url = (String) getIntent().getExtras().get("url");
        String querySelectors = (String) getIntent().getExtras().get("querySelectors");
        String labels = (String) getIntent().getExtras().get("labels");

        webView = findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setMixedContentMode(2);
        jsInterface = new GlanceWebViewJavascriptInterface(webView);
        webView.addJavascriptInterface(jsInterface, "GLANCE_Mask");

        webViewClient = new GlanceWebViewClient(querySelectors, labels);
        webView.setWebViewClient(webViewClient);

        webView.loadUrl(url);
    }

    @Override
    protected void onDestroy() {
        webView.removeJavascriptInterface("GLANCE_Mask");

        // you must call onDestroy on the javascript interface in order for the Glance SDK to properly clean up webView masks
        jsInterface.onDestroy();
        jsInterface = null;

        webViewClient = null;

        super.onDestroy();
    }
}
