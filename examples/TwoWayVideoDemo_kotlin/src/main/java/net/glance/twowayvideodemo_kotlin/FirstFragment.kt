package net.glance.twowayvideodemo_kotlin

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.Fragment
import net.glance.android.*
import net.glance.defaultui.java.DefaultSessionUI
import net.glance.twowayvideodemo_kotlin.Constants.GLANCE_GROUP_ID
import net.glance.twowayvideodemo_kotlin.Constants.VIDEO_MODE
import net.glance.twowayvideodemo_kotlin.Constants.VISITOR_ID
import net.glance.twowayvideodemo_kotlin.databinding.FragmentFirstBinding

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment(), VisitorListener {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFirstBinding.inflate(inflater, container, false).also {
            Glance.addMaskedView(it.textviewSecond, "")
        }
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val self = this

        binding?.buttonStart?.setOnClickListener {
            if (GLANCE_GROUP_ID == 0) {
                context?.let {
                    val dialogBuilder = AlertDialog.Builder(it)
                    dialogBuilder.setMessage("Please set a valid GROUP_ID value in the VisitorHelper class")
                    dialogBuilder.setNegativeButton(
                        "Dismiss"
                    ) { dialog: DialogInterface, which: Int -> dialog.dismiss() }
                    val alertDialog = dialogBuilder.create()
                    alertDialog.show()
                }
            } else {
                val startParams = StartParams()
                startParams.setKey("GLANCE_KEYTYPE_RANDOM")
                startParams.video = VIDEO_MODE

                DefaultSessionUI.init(activity, startParams, true, GLANCE_GROUP_ID, "", this)
                DefaultSessionUI.startSession()
            }
        }

        binding?.buttonPresence?.setOnClickListener { v ->
            if (Glance.isPresenceConnected()) {
                Glance.disconnectPresence()
            } else {
                val initParams = VisitorInitParams(GLANCE_GROUP_ID)
                initParams.visitorId = VISITOR_ID

                DefaultSessionUI.init(activity, initParams, true, this)
                Glance.connectToPresence()
            }
        }

        binding?.buttonCustomTabs?.setOnClickListener { v ->
            val url = "https://google.com/"
            val builder: CustomTabsIntent.Builder = CustomTabsIntent.Builder()
                .setInitialActivityHeightPx(40)
                .setCloseButtonPosition(CustomTabsIntent.CLOSE_BUTTON_POSITION_END)

            val defaultColors: CustomTabColorSchemeParams = CustomTabColorSchemeParams.Builder()
                .setToolbarColor(self.resources.getColor(R.color.teal_700))
                .build()
            builder.setDefaultColorSchemeParams(defaultColors)

            val customTabsIntent: CustomTabsIntent = builder.build()
            customTabsIntent.launchUrl(self.requireContext(), Uri.parse(url))
        }

        binding?.buttonWebview?.setOnClickListener { _ ->
            val i = Intent(context, WebViewActivity::class.java)
            i.putExtra("url", "https://d2e93a2oavc15x.cloudfront.net/")
            i.putExtra("querySelectors", ".mask_1, .mask_2, #mask_3, .mask_4, span, #hplogo")
            i.putExtra("labels", "mask 1, mask 2, mask 3, mask 4, span, LOGO")
            startActivity(i)
        }
    }

    override fun onDestroyView() {
        Glance.removeVisitorListener(this)

        super.onDestroyView()
        _binding = null
    }

    override fun onGlanceVisitorEvent(event: Event?) {
        // TODO: move this event handler to MainActivity and pass variable into fragment to change button view states
        event?.let {
            when (event.code) {
                EventCode.EventVisitorInitialized -> {
                    Log.d("GLANCE", "EventVisitorInitialized")
                }
                EventCode.EventConnectedToSession -> {
                    activity?.runOnUiThread {
                        binding?.let {
                            it.buttonStart.visibility = View.GONE
                            it.buttonPresence.visibility = View.GONE
                            it.buttonWebview.visibility = View.VISIBLE
                            it.buttonCustomTabs.visibility = View.VISIBLE
                            if (Glance.isPresenceConnected()) {
                                anchorViews(
                                    it.clFirstFragmentContainer,
                                    it.buttonPresence,
                                    ConstraintSet.TOP,
                                    it.buttonCustomTabs,
                                    ConstraintSet.BOTTOM,
                                    R.dimen.enabled_presence_bt_margin_top
                                )
                            }
                        }
                    }
                }
                EventCode.EventSessionEnded -> {
                    activity?.runOnUiThread {
                        binding?.let {
                            it.buttonStart.visibility = View.VISIBLE
                            it.buttonPresence.visibility = View.VISIBLE
                            it.buttonWebview.visibility = View.GONE
                            it.buttonCustomTabs.visibility = View.GONE
                            if (Glance.isPresenceConnected()) {
                                anchorViews(
                                    it.clFirstFragmentContainer,
                                    it.buttonPresence,
                                    ConstraintSet.TOP,
                                    it.buttonStart,
                                    ConstraintSet.BOTTOM,
                                    R.dimen.default_presence_bt_margin_top
                                )
                            }
                        }
                    }
                }
                EventCode.EventPresenceConnected -> {
                    activity?.runOnUiThread {
                        binding?.let{
                            it.textviewFirst.text = String.format("Visitor id: %s", VISITOR_ID)
                            it.buttonPresence.setText(R.string.disable_presence)
                        }
                    }
                }
                EventCode.EventPresenceDisconnected -> {
                    activity?.runOnUiThread {
                        binding?.let {
                            it.textviewFirst.setText(R.string.hello_first_fragment)
                            it.buttonPresence.setText(R.string.enable_presence)
                            it.buttonStart.visibility = View.VISIBLE
                            it.buttonWebview.visibility = View.GONE
                            it.buttonCustomTabs.visibility = View.GONE
                            anchorViews(
                                it.clFirstFragmentContainer,
                                it.buttonPresence,
                                ConstraintSet.TOP,
                                it.buttonStart,
                                ConstraintSet.BOTTOM,
                                R.dimen.default_presence_bt_margin_top
                            )
                        }
                    }
                }
                else -> {}
            }
        }
    }

    private fun anchorViews(
        parentLayout: ConstraintLayout?, view1: View, direction1: Int,
        view2: View, direction2: Int, marginDimenId: Int
    ) {
        val anchorSet = ConstraintSet()
        anchorSet.clone(parentLayout)
        anchorSet.connect(
            view1.id, direction1, view2.id, direction2,
            resources.getDimensionPixelSize(marginDimenId)
        )
        anchorSet.applyTo(parentLayout)
    }
}