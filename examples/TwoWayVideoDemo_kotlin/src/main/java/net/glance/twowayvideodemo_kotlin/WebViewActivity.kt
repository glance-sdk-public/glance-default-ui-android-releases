package net.glance.twowayvideodemo_kotlin

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import net.glance.android.GlanceWebViewClient
import net.glance.android.GlanceWebViewJavascriptInterface
import net.glance.twowayvideodemo_kotlin.databinding.ActivityWebViewBinding

class WebViewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWebViewBinding
    private var webView: WebView? = null
    private var webViewClient: GlanceWebViewClient? = null
    private var jsInterface: GlanceWebViewJavascriptInterface? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityWebViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val url = intent?.extras?.getString("url")
        val querySelectors = intent?.extras?.getString("querySelectors")
        val labels = intent?.extras?.getString("labels")

        binding.webView.settings.javaScriptEnabled = true
        binding.webView.settings.domStorageEnabled = true
        binding.webView.settings.mixedContentMode = 2
        jsInterface = GlanceWebViewJavascriptInterface(binding.webView).also {
            binding.webView.addJavascriptInterface(it, "GLANCE_Mask")
        }

        webViewClient = GlanceWebViewClient(querySelectors, labels).also {
            binding.webView.webViewClient = it
        }

        binding.webView.loadUrl(url ?: "")
    }

    override fun onDestroy() {
        webView?.removeJavascriptInterface("GLANCE_Mask")

        // you must call onDestroy on the javascript interface in order for the Glance SDK to properly clean up webView masks
        jsInterface?.onDestroy()
        jsInterface = null

        webViewClient = null

        super.onDestroy()
    }
}