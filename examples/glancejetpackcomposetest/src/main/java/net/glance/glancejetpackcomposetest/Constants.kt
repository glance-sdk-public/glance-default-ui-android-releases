package net.glance.glancejetpackcomposetest

class Constants {

    companion object {
        const val GLANCE_TAG = "glancejetpackcomposetest"
        const val GROUP_ID = 0 // YOUR GROUP ID HERE
    }
}