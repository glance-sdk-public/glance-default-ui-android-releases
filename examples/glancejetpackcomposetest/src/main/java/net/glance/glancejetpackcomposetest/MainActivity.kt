package net.glance.glancejetpackcomposetest

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import net.glance.android.Event
import net.glance.android.EventCode
import net.glance.android.Glance
import net.glance.android.StartParams
import net.glance.android.VisitorListener
import net.glance.defaultui.java.DefaultSessionUI
import net.glance.glancejetpackcomposetest.Constants.Companion.GLANCE_TAG
import net.glance.glancejetpackcomposetest.Constants.Companion.GROUP_ID
import net.glance.glancejetpackcomposetest.ui.theme.GlanceAndroidTheme


class MainActivity : AppCompatActivity(), VisitorListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val startParams = StartParams()
        startParams.setKey("GLANCE_KEYTYPE_RANDOM")

        Glance.addMaskedViewId(R.id.compose_view_text_view, "MASKED TEXT")
        DefaultSessionUI.init(
            this,
            startParams,
            true,
            GROUP_ID,
            "",
            this
        )

        setContent {
            GlanceAndroidTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Column() {
                        Greeting("world")
                        Button(onClick = {
                            val intent = Intent(applicationContext, SecondActivity::class.java)
                            startActivity(intent)
                        }) {
                            Text("Second activity")
                        }

                        Button(onClick = {
                            val intent = Intent(applicationContext, WebViewActivity::class.java)
                            startActivity(intent)
                        }) {
                            Text("WebView activity")
                        }
                    }
                }
            }
        }
    }

    override fun onGlanceVisitorEvent(event: Event?) {
        event?.let {
            when (event.code) {
                EventCode.EventVisitorInitialized -> {
                    Log.d(GLANCE_TAG, "EventVisitorInitialized")
                }

                EventCode.EventConnectedToSession -> {
                    Log.d(GLANCE_TAG, "EventConnectedToSession")
                }

                EventCode.EventSessionEnded -> {
                    Log.i(GLANCE_TAG, "EventSessionEnded")
                }

                EventCode.EventPresenceConnected -> {
                    Log.i(GLANCE_TAG, "EventPresenceConnected")
                }

                EventCode.EventPresenceDisconnected -> {
                    Log.i(GLANCE_TAG, "EventPresenceDisconnected")
                }

                else -> {}
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Column(modifier = Modifier.fillMaxWidth()) {
        val showDialog = remember { mutableStateOf(false) }

        AndroidView(factory = {
            TextView(it).apply {
                id = R.id.compose_view_text_view
                text = "Hello world - mask"
                textSize = 20.sp.value
            }
        })

        LazyColumn() {
            item {
                Text(text = "Hello $name!")
                Text(text = "Hello $name 2!")
                Text(text = "Hello $name 3!")
            }
        }
        LazyColumn() {
            item {
                Text(text = "Hello $name!")
                Text(text = "Hello $name 2!")
                Text(text = "Hello $name 3!")
            }
        }

        GlideImage(
            imageModel = { "https://images.unsplash.com/photo-1628373383885-4be0bc0172fa?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=101&q=80" }, // loading a network image using an URL.
            imageOptions = ImageOptions(
                contentScale = ContentScale.Crop,
                alignment = Alignment.Center
            ),
            modifier = Modifier.size(100.dp)
        )

        Button(onClick = {
            if (GROUP_ID == 0) {
                showDialog.value = true
            } else {
                DefaultSessionUI.startSession()
            }
        }) {
            Text("Start Session")
        }

        if (showDialog.value) {
            MyAlertDialog {
                showDialog.value = it
            }
        }
    }
}

@Composable
fun MyAlertDialog(setShowDialog: (show: Boolean) -> Unit) {
    AlertDialog(
        onDismissRequest = { setShowDialog(false) },
        confirmButton = {},
        dismissButton = {
            TextButton(onClick = {
                setShowDialog(false)
            }) {
                Text("Dismiss")
            }
        },
        title = { Text(text = "Please set a valid GROUP_ID value in the Constants class", fontSize = 20.sp) },
        modifier = Modifier.fillMaxWidth()
    )
}