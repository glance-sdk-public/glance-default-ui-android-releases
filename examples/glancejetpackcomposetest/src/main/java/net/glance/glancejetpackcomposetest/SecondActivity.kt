package net.glance.glancejetpackcomposetest

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.foundation.layout.paddingFromBaseline
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import net.glance.glancejetpackcomposetest.ui.theme.GlanceAndroidTheme

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            GlanceAndroidTheme {
                Column(Modifier.absolutePadding(30.dp, 30.dp, 0.dp, 0.dp)) {
                    LazyColumn {
                        items(1) {
                            Text("Testing subactivity with jetpack")
                            Text("another text field with jetpack")
                            NetworkImage()
                        }
                    }
                    LazyColumn (Modifier.paddingFromBaseline(100.dp, 0.dp)) {
                        items(1) {
                            LazyRow {
                                items(1) {
                                    Text("row 1 text 1")
                                    Text("row 1 text 2")
                                }
                            }
                            LazyRow {
                                items(1) {
                                    Text("row 2 text 1")
                                    Text("row 2 text 2")
                                }
                            }
                        }
                    }
                    val self = this
                    LazyRow(Modifier.absolutePadding(0.dp,20.dp,0.dp,0.dp)) {
                        items(1) {
                            Button(onClick = {
                                displayAlert()
                            }, modifier = Modifier.absolutePadding(0.dp, 40.dp, 20.dp, 0.dp)) {
                                Text(text = "Button test")
                            }
                            Button(onClick = {}) {
                                Text(text = "Button test 2")
                            }
                        }
                    }
                    LazyColumn (Modifier.paddingFromBaseline(100.dp, 0.dp)) {
                        items(1) {
                            LazyRow {
                                items(1) {
                                    Text("row 1 text 1")
                                    Text("row 1 text 2")
                                }
                            }
                            LazyRow {
                                items(1) {
                                    Text("row 2 text 1")
                                    Text("row 2 text 2")
                                }
                            }
                        }
                    }
                    LazyColumn {
                        items(1) {
                            BoxItem(mainText = "hello world", subText = "box sub text")
                        }
                    }
                }
            }
        }
    }

    fun displayAlert(){
        val dialogBuilder = AlertDialog.Builder(this)

        // set message of alert dialog
        dialogBuilder.setMessage("Do you want to close this application ?")
            // if the dialog is cancelable
            .setCancelable(false)
            // positive button text and action
            .setPositiveButton("Proceed", DialogInterface.OnClickListener {
                    dialog, id -> finish()
            })
            // negative button text and action
            .setNegativeButton("Cancel", DialogInterface.OnClickListener {
                    dialog, id -> dialog.cancel()
            })

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle("AlertDialogExample")
        // show alert dialog
        alert.show()
    }
}

@Composable
fun BoxItem(mainText: String, subText: String) {
    LazyRow {
        item {
            Text(mainText)
            Text(subText)
        }
    }
}

@Composable
fun NetworkImage() {
    Box {
        GlideImage(
            imageModel = { "https://images.unsplash.com/photo-1617704548623-340376564e68?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" }, // loading a network image using an URL.
            imageOptions = ImageOptions(
                contentScale = ContentScale.Crop,
                alignment = Alignment.Center
            ),
            modifier = Modifier.size(100.dp)
        )
    }
}