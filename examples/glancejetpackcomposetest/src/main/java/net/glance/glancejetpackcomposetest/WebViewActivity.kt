package net.glance.glancejetpackcomposetest

import android.content.Intent
import android.os.Bundle
import android.webkit.WebView
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.viewinterop.AndroidView
import net.glance.android.Glance
import net.glance.android.GlanceWebViewClient
import net.glance.android.GlanceWebViewJavascriptInterface
import net.glance.android.HostSession
import net.glance.glancejetpackcomposetest.ui.theme.GlanceAndroidTheme

class WebViewActivity : AppCompatActivity() {

    private var jsInterface : GlanceWebViewJavascriptInterface? = null
    private var webView : WebView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val context = this;

        val webviewUrl = "https://d2e93a2oavc15x.cloudfront.net"
        val queryMaskSelectors = ".mask_1, .mask_2, #mask_3, .mask_4, span, #hplogo"
        val labelsToMask = "mask 1, mask 2, mask 3, mask 4, span, LOGO"

        setContent {
            GlanceAndroidTheme {

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = Color.White
                ) {
                    AndroidView(factory = {
                        WebView(context).apply {
                            settings.setSupportZoom(true)
                            settings.builtInZoomControls = true
                            settings.displayZoomControls = false
                            settings.javaScriptEnabled = true
                            settings.domStorageEnabled = true

                            jsInterface = GlanceWebViewJavascriptInterface(this).also {
                                addJavascriptInterface(it, "GLANCE_Mask")
                            }

                            webViewClient = GlanceWebViewClient(queryMaskSelectors, labelsToMask)

                            loadUrl(webviewUrl)

                            id = R.id.compose_view_webview

                            webView = this
                        }
                    })
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (Glance.isInSession()) {
            if (requestCode == HostSession.REQUEST_SCREEN_SHARE && resultCode == -1) {
                Glance.onCaptureScreenPermissionSuccess(data)
            } else if (requestCode == HostSession.REQUEST_SCREEN_SHARE && resultCode == 0) {
                Glance.onCaptureScreenPermissionFailure()
            }
        }
    }

    override fun onDestroy() {
        webView?.removeJavascriptInterface("GLANCE_Mask")

        // you must call onDestroy on the javascript interface in order for the Glance SDK to properly clean up webView masks
        jsInterface?.onDestroy()
        super.onDestroy()
    }
}