# Glance SDK DefaultUI

The Glance Mobile SDK comes with an implementation of SessionUI. You can build your own custom UI on top of the same events it uses.

## How Glance Works

The Glance SDK enables your mobile app to be shareable to one or more viewing agents at your organization so they can offer your users a guided customer experience.

Glance is designed to run in parallel with your organization's existing voice communications systems. After a customer contacts your organization, Glance can be introduced at an agent's discretion.

Sessions start in the SDK within your organization's Glance Group. Your organization's licensed Glance users are then able to join these sessions to guide your customers in the app.

Through Glance's Presence service, your Glance users can proactively offer sessions to identified customers through CRM systems like Salesforce for a more seamless experience.

One-way or two-way video can be added to sessions within certain configurations. This is also intended to be used in parallel with your existing voice communication. Glance does not transmit audio.

## Default UI

The Glance SDK for Android comes with UI out-of-the-box to fully enable you to offer your app users a guided experience. This includes permission and coordination prompts to get people connected and a floating panel for video communication and session controls.

### Features

**Scalable Widget**: a floating widget that adapts to the session type (screen sharing only, multiway video, large video, etc.)

Default UI Agent Widget  |  Default UI Small Multiway-video Widget | Default UI Large Multiway-video Widget
:-------------------------:|:-------------------------:|:-------------------------:
![Default UI Agent Widget](./defaultui/docs_imgs/default_ui_agent_widget1.png)  |  ![Default UI Small Multiway-video Widget](./defaultui/docs_imgs/default_ui_small_multiway_widget_1.png) |  ![Default UI Large Multiway-video Widget](./defaultui/docs_imgs/default_ui_large_widget1.png)

**Configurable dialogs**: User prompts to explain, configure and control the guided experience before it starts. These generally include a description, terms links, camera permissions and previews, session keys, and confirmations.

Default UI Start Session Dialog  |  Default UI Session Key Dialog | Default UI Video Session Dialog
:-------------------------:|:-------------------------:|:-------------------------:
![Default UI Start Session Dialog](./defaultui/docs_imgs/default_ui_dialog1.png)  |  ![Default UI Session Key Dialog](./defaultui/docs_imgs/default_ui_session_key_dialog1.png) |  ![Default UI Video Session Dialog](./defaultui/docs_imgs/default_ui_video_dialog1.png)

<br>

Default UI Terms Dialog |  Default UI Confirmation Dialog |
:-------------------------:|:-------------------------:
![Default UI Terms Dialog](./defaultui/docs_imgs/default_ui_terms_dialog1.png)  |  ![Default UI Session Key Dialog](./defaultui/docs_imgs/default_ui_end_session_dialog1.png) |

**Tab Mode**: When your user wants to focus their attention on the app during a session, they can drag the UI off screen to collapse into "tab mode." They can bring it back by tapping on it, or drag it to a corner. The agent can also control the tab's position from their side.

**Fixed-corner widget**: The floating in-session panel always sticks to one of the four corners of the device. The agent can also send the panel to another corner for your user to draw attention to content it might have been in front of. 

**Agent/user state synchronization**: When your user takes action like moving the UI or toggling video, we notify the Glance service so the agent UI reflects the current state.

| Tab Mode | Fixed-corner widget | Agent/user state synchronization |
| --- | --- | --- |
<img src="./defaultui/docs_imgs/default_ui_tab_widget_1.png" height="540px" /> | <img src="./defaultui/docs_imgs/default_ui_tab_widget_2.png" height="540px" /> | <img src="./defaultui/docs_imgs/default_ui_small_multiway_widget_4.png" height="540px" />

**Localizable strings**: you can modify all strings in this UI and support multiple languages by using our string keys (see appendix).<br>

**Customizable colors, fonts and dimensions**: like strings, we also support custom colors, fonts and dimensions. They all work following the native Android resources customization system using resource keys.

Default UI Customized Start Session Dialog | Default UI Customized Session Key Dialog
:-------------------------:|:-------------------------:
![ Default UI Customized Start Session Dialog](./defaultui/docs_imgs/default_ui_dialog2.png) |  ![Default UI Customized Session Key Dialog](./defaultui/docs_imgs/default_ui_dialog3.png)

**Custom logging interface to integrate with libraries or custom UIs**: You can implement and pass a `GlanceSDKEventSenderListener` interface instance to your `BaseSessionUI` implementation after initializing the SDK.
Then, for every SDK Event (expected or error-related) or user actions (widget position, video size and status changes, etc), you will be notified. Ex.:
```JSON
{
   type: SDK,
   method: sendNewLocation,
   params: {
      widgetlocation: message:widgetlocation;location:topleft
   }
}
```

## How to integrate custom UI

1. Add our latest module to your build.gradle file.
2. Set the ```SessionUI``` implementation class before starting the session using the following method:             
```java
Visitor.setSessionUIImplementation(<your SessionUI implementation>.class, startParams);
```
In our case, the class is `DefaultSessionUI`.

That's it, now the core SDK will invoke your UI whenever it's appropriate.

> **Note**: If you are using the standard SessionUI implementation, please do not modify the public methods. We have wrappers for some `Glance` class calls to keep things easier to read and use. For example, use `DefaultSessionUI.startsession()` instead of calling `Glance.startsession(…)`.

### Public methods

| Method | Description|
| ----------- | ------------- |
| `public static void init(Activity activity, VisitorInitParams initParams, boolean maskKeyboard, VisitorListener listener)` | Inits the SDK with the given Presence parameters. |
|`public static void init(Activity activity, StartParams startParams, boolean maskKeyboard, int groupId, String visitorId, VisitorListener eventsListener)`| Inits the SDK with the given StartParams parameters, group ID and visitor ID.|
| `public static void init(Activity activity, StartParams startParams, boolean maskKeyboard, int groupId, String visitorId, String glanceServer, VisitorListener eventsListener)` | Same as above, but allows you to provide a different Glance server address. |
| `public static void startSession()` | Starts a session with a previous set configuration. |
| `public void setVideoAdded(boolean connected)` | Defines whether the visitor video is connected or not. |
| `public void onPermissionResult(String permission)` | Handles a given permission request result, by checking the provided permission key status. |
| `public void getWidgetModeFromVideoMode(VideoMode videoMode)` | Gets the corresponding `WidgetMode` enum for the given `VideoMode` value. |


## Implementing your own UI

### Kotlin and Java Implementations

The DefaultUI is implemented in both Java and Kotlin. These two implementations are provided for cases where you want to customize the DefaultUI by integrating the code directly into your own projects. In this case, you can choose between integrating the code into your own project using either Java or Kotlin according to your preferences.

The Java implementation of the classes can be found in the directory: src/main/java/net/glance/defaultui/java

The Kotlin implementation of the classes can be found in the directory: src/main/java/net/glance/defaultui/kotlin

In both cases, the same classes are implemented. While there may be some syntactical differences between the Java and Kotlin versions, the following documentation applies to both.

### Extend the SessionUI abstract class

The first step is to make your UI implementation class to extend our abstract `SessionUI` class. It contains all the protocol definitions you'll need to implement. Since we instantiate it in our core SDK, we need the class to have this specific constructor signature:

```
public YourSessionUI(Context context, Activity activity, StartParams startParams) {
}
```

Here are the methods and their descriptions:
 | Method | Description |
 | ----------------- | ------------- |
|`void onActivityChanged(final Activity activity, boolean isChangingConfigurations);` | Called when an activity change is detected. In this case, you can use it to update the activity reference and anything else UI-related. |
| `void setSessionParams(StartParams params);` | Called whenever a session params update comes (like adding Visitor video), so you can update the internal `sessionParams` object. |
| `void setTermsAccepted(boolean accepted);` | Invoked when the user accepts terms. You can use it to save this status internally. |
| `void onSessionKeyReceived(String sessionKey);` | Triggered as soon as we receive the session key from the `EventConnectedToSession` event and the session didn't start via Presence (either Presence isn't connected or the session was started with `"GLANCE_KEYTYPE_RANDOM"`). |
| `void onSessionKeyReceived(String sessionKey, SessionDialogListener dialogListener);` | Same as above, but setting a custom ```SessionDialogListener``` instance. |
| `void promptVisitorVideo(VideoMode videoMode);` | Triggered when received the event `EventVisitorVideoRequested` and the terms are not accepted yet and video mode is not set to `VideoMode.VideoOff`. |
| `void showWidget();` | Invoked when the SDK detects that an agent has connected to the session. You can use it to show whatever UI you want to represent the agent. |
| `void showSessionDialog(SessionDialogListener listener);` | Called after invoking `Visitor.startSession()` method to prompt the visitor. |
| `void showSessionDialog(SessionDialogListener listener, DialogSessionMode mode);` | Called when we need to pass the specific session mode. In our core SDK, we curently use it when Presence is enabled and we may want to show the "Waiting for the agent" dialog instead of a key prompt. |
| `void showSessionDialog(SessionDialogListener listener, DialogSessionMode mode, String warningMessage, boolean dismiss);` | Same as above, but allowing to change the dialog message and also telling it whether to dismiss the window after confirming. |
| `void hideSessionDialog();` | Invoked when the SDK detects that the agent connected to the session, meaning any open dialogs should be hidden and session UI should be displayed instead. |
| `void startVisitorVideo(StartParams sparams, int groupId, boolean invokeShowWidget);` | Called when the visitor video is ready to be added. Here you can set up the VideoSession and then show the visitor video preview in the UI. |
| `void onAgentVideoConnected(SessionUICompletionListener listener);` | Called when the agent video is ready to be shown to the visitor. Here you need to call `GlanceManager.getInstance().setCustomSessionViewId(<your net.glance.android.SessionView id>);` to link the SessionView from your layout with the video stream. |
| `void changeAgentVideoState(boolean isAgentVideoEnabled);` | Called when the agent video state changes. You can use it to decide whether to show or hide some UI that handles the agent video on the visitor side. |
| `void hide();` | Called when the session ends, so you can hide whatever UI you are showing. |
| `boolean isVisitorVideoStreaming();` | Called when the SDK is ready to show the visitor video streaming. We need to check this status in order to decide whether to add the video or not (in case it is already added). You can use your `VideoSession` instance to verify it. |
| `boolean areTermsAccepted();` | Called when the SDK is ready to show the visitor video streaming. We check this status to protect the user's privacy. The video will be just streamed to the agent if the user has already accepted terms. |
| `VideoMode getStartVideoMode();` | Called when the SDK is ready to show the visitor video streaming. The UI should adapt to the mode returned. |
| `boolean isVisitorVideoAdded();` | Checks if the visitor video was already added to indicate that video should be sent to the server. |
| `void deviceConnected();` | Triggered when your user's device camera connects and is ready to send the feed to the video session. Here you can invoke your `VideoSession` instance. |
| `void deviceDidUpdateDimensions(GlanceSize dimensions, GlanceSize deviceDimensions);` | Triggered when the device camera updates the image dimension, like if user has flipped to the back camera or vice-versa. This should propogate to the `VideoSession` instance in order to keep the right image aspect ratio. |
| `void processUserMessage(Event event);` |Triggered when `EventMessageReceived` is received. | 

### Parsing Messages
Example code to parse messages from the server: 

```java
String message = event.GetValue("message");
```

Getting the message value (if applicable): 

```java 
String value = event.GetValue(<value property name>);
```

Messages from the server via a connected agent can assume one of the following values:

| Label | Description | Possible Values |
| --- | --- | --- |
|**widgetlocation**| the agent requested the widget to change corners. The corner is retrieved by getting the value using the property ```location```. | *topleft*, *topright*, *bottomleft*, and *bottomright*; |
| **widgetvisibility**| the agent requested the widget to go to either the tab or full (current) mode. The mode is retrieved by getting the value using the property ```visibility```. | *tab*, *full*; |
| **visitorvideorequested** | the agent asked the visitor to **add** his video. You can show some UI to prompt the visitor for confirmation about it. | |
| **pausevisitorvideo** | the agent asked the visitor to **pause** his video. You can hide/disable the UI currently showing the visitor video. *Only the visitor can resume his video back*. | |
| **videosize** | the agent requested the widget to go to either the small or large mode. You can understand the small mode as the most used floating widget size, and the large mode as a full screen mode of some sort. | *small*, *large*; |
___

### Integrating Video with your SessionUI

#### 1. Make sure you have a ```net.glance.android.SessionView``` view for the agent video and a ```net.glance.glancevideo.AutoFitTextureView``` view for the visitor video in your layout

For example:
```xml
<FrameLayout
      android:id="@+id/flVisitorVideoContainer"
      android:layout_width="match_parent"
      android:layout_height="match_parent"
      app:layout_constraintEnd_toEndOf="parent"
      app:layout_constraintStart_toStartOf="parent"
      app:layout_constraintTop_toTopOf="parent" >

      <net.glance.glancevideo.AutoFitTextureView
         android:id="@+id/aftvVisitorVideo"
         android:layout_width="wrap_content"
         android:layout_height="wrap_content"/>
</FrameLayout>
```

```xml
<androidx.cardview.widget.CardView
    android:id="@+id/cvAgentVideoContainer"
    app:cardCornerRadius="@dimen/glance_dialog_corner_radius"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content">

    <net.glance.android.SessionView
        android:id="@+id/svSessionAgentVideo"
        android:layout_width="@dimen/glance_session_ui_video_widget_width"
        android:layout_height="@dimen/glance_session_ui_video_widget_height"
        android:adjustViewBounds="true"
        android:background="@drawable/glance_dialog_session_video_rounded_corner_shape"/>
</androidx.cardview.widget.CardView>
```

##### 1.1 Define a class variable for the visitor video view

```java
AutoFitTextureView aftvVisitorVideo;
```

We don't need the same for the agent because in this case, we only use the view's ID. We'll also use the `AutoFitTextureView` view ID to instantiate the `CameraManager`.

##### 1.2 Pass the agent video view ID to the Glance singleton

Whenever you change (or use for the first time) the `SessionView` view (different screens, different containers, etc.), you need to update it on the SDK so we can redirect the video stream to it.

```xml
Glance.setCustomAgentVideoSessionViewId(R.id.svSessionAgentVideo);
```

#### 2. Create the CameraManager, VideoSession, SessionUIListener, and Handler variables

For camera access, our SDK provides the ```CameraManager``` class to manage permission checking/request and camera selection. We also need a ```VideoSession``` instance to control the camera video stream flow.

```java
CameraManager cameraManager = new CameraManager(this, activity, mSessionUIContainer, null, R.id.aftvVisitorVideo);
VideoSession videoSession = VideoSession.getInstance();
SessionUIListener listener = (SessionUIListener) cameraManager;
```

Lastly, we use a Handler to run some video operations in the main thread:

```java
Handler mHandler = new Handler(Looper.getMainLooper());
```

#### 3. VideoSession API

Below you can find a description of each ```VideoSession``` API class we may use in our integration. For more details, please refer to our ```DefaultSessionUI``` implementation.

| API Class | Description |
| --- | ---| 
|```.initWithGroupId(int groupId)``` | Sets the customer's group ID and initializes the internal video streamer. |
| ```.setListener(VideoSession.VideoSessionListener listener)``` | Sets the listener we use to communicate with the ```SessionUI``` implementation. |
| ```.deviceConnected()``` | Can be invoked once the camera is ready to stream the video. |
| ```.start(StartParams params)``` | Starts the camera video streaming to the server. |
| ```.stopEncoding()``` | Pauses the video streaming. It can be used when the visitor doesn't want to show their video temporarily.   |
| ```.resume()``` | Resumes the previously stopped video streaming. It can be used when the visitor decides to show their video again during the session. | 
| ```.isVisitorVideoStreaming()``` | Checks whether the streaming is paused or not. |
| ```.isConnected()``` | Checks whether the internal video streamer is connected. |
| ```.encoderSurfaceAttached(Surface surface)``` | Pass the surface instance to the internal encoder. Needs to be called when the surface instance is ready to be used. |
| ```.updateDimensions(GlanceSize dimensions, GlanceSize deviceDimensions)``` | Updates the internal video dimensions variables to be used when the surface is ready to receive the new settings. Called whenever the video dimensions needs to be changed. |
| ```.end()``` | Ends the video enconding and streaming, and resets all internal variables state.

#### 4. Using the VideoSession API

Following our code samples, here are a brief list of things you might need to do in order to get your video integration done:

##### 4.1 Start the visitor video

When the SDK creates the child session needed for the video feature (when the session is started with **videoMode != videooff**), it will invoke the ```SessionUI.startVisitorVideo()``` method so you can manage your UI accordingly. So here's our implementation example:

```java
@Override
   public void startVisitorVideo(StartParams sparams, int groupId, boolean invokeShowWidget) {
      videoStartParams = sparams;

      if (videoStartParams.getVideo() != VideoMode.VideoOff) {
         videoSession = VideoSession.getInstance();

         boolean alreadyConnected = videoSession.isConnected();
         videoSession.initWithGroupId(groupId);
         videoSession.setListener(this);
         if (alreadyConnected) {
               // If was connected already, start again with new parameters
               // Since camera is already open, deviceConnected won't happen again
               videoSession.deviceConnected();
               videoSession.start(videoStartParams);
         }

         isVisitorVideoAdded = true;

         if (invokeShowWidget) {
               showWidgetFromStartVideoMode();
         }
      }
   }
```

##### 4.2 Escalate to a Two-way-video

When the session is started **without video** and then the visitor accepts the agent's invite to share their video, the SDK will trigger, at some point, the ```SessionUI.deviceConnected()``` method after the camera signal that it's ready to stream. So here's how we've implemented it:

```java
@Override
public void deviceConnected() {
   Log.d(TAG, "deviceConnected");
   if (videoSession != null) {
      videoSession.deviceConnected();
      if (foundDimensions) {
            Log.d(TAG, "Starting video session...");
            videoSession.start(videoStartParams);
      } else {
            Log.w(TAG, "ISSUE: deviceConnected but no dimensions found");
      }
   }
}
```

##### 4.3 End the streaming

In general, you'll only stop the video streaming when the session ends. So in this case, just call ```videoSession.end()``` when you dismiss all the session-related views.


#### 5. CameraManager API

Below you can find a description of each ```CameraManager``` API class we may use in our integration. Its role is to basically control everything related to the camera resource and how it communicates with our SDK. For more details, please refer to our ```DefaultSessionUI``` implementation.

| API Class | Description|
| --- | --- |
| ```.onRootViewChanged(View newRoot)``` | Call whenever you change the root view of the camera ```SurfaceView``` view. It will update the internal states. |
| ```.onVideoSurfaceViewChanged(int newSurfaceViewId)``` | Call whenever you change the ```SurfaceView``` view used by the camera resource. It will update the internal states. |
| ```.onOpenCamera()``` | Configures and opens the camera resource based on the selected (front/back) camera ID, screen orientation, and permission state. |
| ```.onOpenCamera(int width, int height)``` | Same as above, but specifying the camera preview frames measures.
| ```.onCloseCamera()``` | Stops the camera feed and resets all internal variables state. |
| ```.initCameraWhenAvailable(boolean openCamera)``` | Set a flag to control whether the camera should open right away when it's available. |
| ```.flipCamera(int surfaceViewId)``` | Switches the current camera to either front/back, by giving the new SurfaceView id target.
| ```.flipCamera(int surfaceViewId, boolean shouldOpenCamera)``` | Same as above, but specifying whether the camera should open right away when it's available (default is `true`, and it's the same as calling the `initCameraWhenAvailable(boolean openCamera)` internally).
| ```.isFrontCameraSelected()``` | Checks whether the front camera is currently selected.
| ```.isCameraOpened()``` | Checks whether the camera is already opened.

#### 6. Implement the ```VideoSession.VideoSessionListener``` on your extended SessionUI class

We need to implement this interface in order to build the bridge between the UI, the camera resource and the server.

Here we'll describe the main ones, for more details regarding the other methods (currently not used in our sample), please refer to our ```DefaultSessionUI``` class implementation.

##### 6.1 ```public void videoSessionEncoderSurfaceAndTexture(Surface surface, SurfaceTexture surfaceTexture)```

This method will be invoked when the video encoder is attached to the given Surface so it can be streamed to the server. At this point we need to let the ```VideoSession``` know it, so it can update its internal ```Surface``` instance.

```java
@Override
public void videoSessionEncoderSurfaceAndTexture(Surface surface, SurfaceTexture surfaceTexture) {
   if (this.sessionUIListener != null) {
      mHandler.post(() -> {
            sessionUIListener.sessionUIEncoderSurfaceAndTexture(surface, surfaceTexture);
            if (videoSession != null) {
               videoSession.encoderSurfaceAttached(surface);
            }
      });
   }
}
```

##### 6.2 ```public void videoSessionDidResizeDimensions(int width, int height)```

This one will be invoked whenever we have a video dimension change, so we need to share this update with the ```CameraManager``` instance via ```SessionUIListener```.

```java
@Override
public void videoSessionDidResizeDimensions(int width, int height) {
   if (this.sessionUIListener != null) {
      this.sessionUIListener.sessionUIDidResizeDimensions(width, height);
   }
}
```

### Reference: additional methods from the BaseSessionUIView class

| Method | Description |
| --- | --- |
| ```void showWidgetFromStartVideoMode();``` | Shows the widget considering the selected ```VideoMode``` for the session start. |
| ```void showSessionDialog(SessionDialogListener listener, DialogSessionMode mode, String warningMessage, boolean dismiss);``` | Same as ```void showSessionDialog(SessionDialogListener listener, DialogSessionMode mode)```, but allowing to change the dialog message and also telling it whether to dismiss the window after confirming the action. |


### Notify the Agent Side About UI Updates

The agent window will contain a control panel where the agent can receive and send commands to change the visitor side UI behavior. It's important that the UI implementation notifies the agent about these changes as soon as they happen so the state remains synchronized. Our ```Visitor``` class provides some methods to achieve it. The values map for each one is available in the ```void processUserMessage(Event event);``` description session above. Please find the list below:

| Method | Description |
| --- | --- |
| `void updateWidgetVisibility(String mode)`: | Send to the agent the new widget mode, either full (small) or tab, when the user drags it towards the edges (collapses to tab) or the center (expands to full). |
| `void updateVisitorVideoStatus(boolean paused)`: | Sends the new visitor video paused state to the agent so the agent side view window can render it accordingly. This state will change when the visitor enables/disables its video while in the session. |
| `void updateVisitorVideoSize(int width, int height, String mode)`: | Sends the new visitor video view size to the agent so the agent side view window can render it accordingly. This change will basically happen when flipping the visitor device cameras. |
| `void updateWidgetLocation(String cornerString)`: | Sends the new widget corner location so the agent side view window can update the control panel accordingly. |

### Jetpack Compose

If you need to integrate our SDK into a Jetpack Compose app, please see our [example project](https://gitlab.com/glance/sdkteam/android/glance-android-default-ui/-/tree/main/examples/glancejetpackcomposetest).

There you'll find some examples of how to init, start a session, mask views, etc.

### Performance metrics

Below you can find are our current measurements for the latest 6.X SDK version. Notes:

- We used the DefaultUI app for both builds, which is a simple app with only default settings for everything (dependencies, Proguard rules, etc);
- For the UI-less mode, we used another app that is even simpler;
- We executed the measurement considering both .apk and .aar formats for the app;
- This is a simple app with only default settings for everything (dependencies, Proguard rules, etc).


|    Version    |            .apk Compilation time          |              .aar Compilation time        |   SDK size  |
|---------------|-------------------------------------------|-------------------------------------------|-------------|
| 5.7.15.2      | 40s (first time), 5s (second time and on) | 12s (first time), 2s (second time and on) |     5.4M    |
| 6.15          | 30s (first time), 2s (second time and on) | 5s (first time), 2s (second time and on)  |    5.01MB   |
| 6.15 (with UI)| 42s (first time), 2s (second time and on) | 13s (first time), 2s (second time and on) |      7MB    |
